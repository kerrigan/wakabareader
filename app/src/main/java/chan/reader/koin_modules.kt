package chan.reader

import chan.reader.features.boardlist.BoardListActivity
import chan.reader.features.boardlist.BoardListViewModel
import chan.reader.features.boardlist.Navigator
import chan.reader.features.boardlist.data.BoardsRepository
import chan.reader.features.boardlist.data.BoardsRepositoryImpl
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val appModule = module {
    scope<BoardListActivity> {
        scoped { Navigator(get()) }
        scoped<BoardsRepository> { BoardsRepositoryImpl(get()) }
        viewModel { BoardListViewModel(get(), get()) }
    }
}
