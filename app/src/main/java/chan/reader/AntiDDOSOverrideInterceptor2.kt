package chan.reader
import android.annotation.SuppressLint
import android.content.Context
import android.net.http.SslError
import android.os.Handler
import android.os.Looper
import android.webkit.CookieManager
import android.webkit.SslErrorHandler
import android.webkit.WebResourceRequest
import android.webkit.WebSettings
import android.webkit.WebView
import android.webkit.WebViewClient
import okhttp3.Interceptor
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.Response
import okhttp3.ResponseBody
import java.util.concurrent.CountDownLatch

interface AntiDDOSCookiesStorage {
    fun saveCookie(cookie: String)
    fun loadCookie(): String?
}

class AntiDdosInterceptor2(private val context: Context, private val cookiesStorage: AntiDDOSCookiesStorage) : Interceptor {
    companion object {
        const val TARGET_STRING = "<option value=\\\"Futaba\\\">Futaba</option>"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request()
        var savedCookies = cookiesStorage.loadCookie()

        if (savedCookies == null) {
            val bodyFromWebView = loadPageWithWebView(originalRequest.url.toString())
            savedCookies = cookiesStorage.loadCookie()

            if (bodyFromWebView != null && bodyFromWebView.contains(TARGET_STRING)) {
                return chain.proceed(originalRequest)
                    .newBuilder()
                    .body(ResponseBody.create("text/html".toMediaTypeOrNull(), bodyFromWebView))
                    .build()
            }
        }

        val requestWithCookies = originalRequest.newBuilder()
            .header("Cookie", savedCookies ?: "")
            .build()

        var response = chain.proceed(requestWithCookies)
        val responseBodyString = response.body?.string() ?: ""

        if (!responseBodyString.contains(TARGET_STRING)) {
            // If TARGET_STRING is not found, repeat the process
            response.close()
            val bodyFromWebView = loadPageWithWebView(originalRequest.url.toString())
            savedCookies = cookiesStorage.loadCookie()
            val newRequestWithCookies = originalRequest.newBuilder()
                .header("Cookie", savedCookies ?: "")
                .build()
            response = chain.proceed(newRequestWithCookies)

            if (bodyFromWebView != null && bodyFromWebView.contains(TARGET_STRING)) {
                return response.newBuilder()
                    .body(ResponseBody.create("text/html".toMediaTypeOrNull(), bodyFromWebView))
                    .build()
            }
        }

        return response.newBuilder()
            .body(ResponseBody.create("text/html".toMediaTypeOrNull(), responseBodyString))
            .build()
    }

    private fun loadPageWithWebView(url: String): String? {
        val latch = CountDownLatch(1)
        val result = StringBuilder()

        Handler(Looper.getMainLooper()).post {
            val webView = WebView(context)
            webView.layout(0, 0, 380, 720)
            webView.settings.javaScriptEnabled = true
            webView.settings.cacheMode = WebSettings.LOAD_NO_CACHE

            webView.webViewClient = object : WebViewClient() {
                override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                    //TODO: check cookies
                    view?.loadUrl(request?.url.toString())
                    return false
                }

                override fun onPageFinished(view: WebView?, url: String?) {

                    view?.evaluateJavascript("document.documentElement.outerHTML") {
                        val html = it.replace("\\u003C", "<");

                        if (html.contains(AntiDdosInterceptor2.TARGET_STRING, true)) {
                            val cookieManager = CookieManager.getInstance()
                            val cookies = cookieManager.getCookie(url)
                            cookiesStorage.saveCookie(cookies)
                            view.clearCache(true)
                            latch.countDown()
                        }
                    }
                }

                @SuppressLint("WebViewClientOnReceivedSslError")
                override fun onReceivedSslError(
                    view: WebView?,
                    handler: SslErrorHandler?,
                    error: SslError?
                ) {
                    handler?.proceed()
                }
            }

            webView.loadUrl(url)
        }

        try {
            latch.await()
        } catch (e: InterruptedException) {
            e.printStackTrace()
        }

        return result.toString()
    }
}