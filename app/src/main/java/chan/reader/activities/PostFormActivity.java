package chan.reader.activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.io.IOException;

import androidx.appcompat.app.AppCompatActivity;
import chan.reader.CirnoidSettings;
import chan.reader.KUtils;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.objects.ReplyData;
import chan.reader.services.PostCreateService;
import coil.ImageLoader;
import coil.request.ImageRequest;

public class PostFormActivity extends AppCompatActivity {
    private static final int SELECT_PICTURE = 1;
    private static final int SELECT_PICTURE_KITKAT = 2;

    public static final String NAME_ID = "name";
    public static final String PASSWORD_ID = "password";
    public static final String BOARD_PREFIX_ID = "prefix";
    public static final String THREAD_NUMBER_ID = "thread";
    public static final String INIT_TEXT_ID = "text";
    public static final String IMAGE_URI_ID = "imageuri";
    public static final String TOPIC_ID = "topic";

    private String mBoardPrefix;
    private String mInitText;
    private long mThreadNumber;
    private String mCurrentImageUri = "";
    private String mCurrentPassword = "";
    private String mCurrentName = "";
    private String mCurrentTopic = "";

    //private SharedPreferences preferences;
    private Button mCancel;
    private ImageButton mChooseFile;
    private Button mSend;
    private ImageView mCaptchaButton;
    private TextView mName;
    private TextView mTopic;
    private TextView mText;
    private TextView mCaptcha;

    private TextView mPassword;

    private boolean showFormFields;
    private CirnoidSettings mSettings = null;
    private ImageLoader mImageLoader = null;


    @Override
    protected void onStart() {
        mSettings = new CirnoidSettings(this);
        super.onStart();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mSettings.getSaveLastMessage()) {
            String lastMessage = mText.getText().toString();
            if (!lastMessage.equals("")) {
                mSettings.setLastSavedMessage(lastMessage);
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //setTheme(R.style.Theme_Sherlock);
        super.onCreate(savedInstanceState);

        mImageLoader = KUtils.initUntrustImageLoader(this);

        if (mSettings == null) {
            mSettings = new CirnoidSettings(this);
        }

        initFromIntent();

        showFormFields = mSettings.getShowFormFields();

        overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);
        setContentView(R.layout.thread_form);

        if (mThreadNumber == -1) {
            setTitle("Создать тред в /" + mBoardPrefix + "/");
        } else {
            setTitle("Отправить ответ в /" + mBoardPrefix + "/" + mThreadNumber);
        }

        mCancel = (Button) findViewById(R.id.form_cancel);
        mChooseFile = (ImageButton) findViewById(R.id.file_choose);
        mSend = (Button) findViewById(R.id.form_send);
        mCaptchaButton = (ImageView) findViewById(R.id.board_captcha);

        mName = (TextView) findViewById(R.id.form_name);
        mTopic = (TextView) findViewById(R.id.form_topiс);
        mText = (TextView) findViewById(R.id.form_text);
        mCaptcha = (TextView) findViewById(R.id.form_captcha);
        mPassword = (TextView) findViewById(R.id.form_password);


        LinearLayout formHiddenFields = (LinearLayout) findViewById(R.id.form_hide_wrapper);
        if (!showFormFields && mThreadNumber != -1) {
            formHiddenFields.setVisibility(View.GONE);
        }


        mPassword.setText(mCurrentPassword);
        mName.setText(mCurrentName);
        mText.setText(mInitText);
        mTopic.setText(mCurrentTopic);

        if (!mCurrentImageUri.equals("")) {
            mChooseFile.setSelected(true);
            mChooseFile.setMaxHeight(600);
            mChooseFile.setAdjustViewBounds(true);
            mChooseFile.setImageURI(Uri.parse(mCurrentImageUri));
        }

        new CaptchaLoadTask().execute(mCaptchaButton);

        mCaptchaButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                new CaptchaLoadTask().execute(mCaptchaButton);
            }
        });


        mChooseFile.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (Build.VERSION.SDK_INT < 19) {
                    Intent intent = new Intent();
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(
                            Intent.createChooser(intent, "Выбрать изображение"),
                            SELECT_PICTURE);
                } else {
                    Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("image/*");
                    startActivityForResult(intent, SELECT_PICTURE_KITKAT);
                }
            }
        });

        mCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mSend.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                if (validateForm()) {
                    // Послать запрос на создание комментария

                    ReplyData data = new ReplyData();
                    data.prefix = mBoardPrefix;
                    data.baseUrl = Registry.SITE_URL;
                    if (mThreadNumber == -1)
                        data.threadId = "0";
                    else
                        data.threadId = Long.toString(mThreadNumber);
                    data.name = mName.getText().toString();
                    data.topic = mTopic.getText().toString();
                    data.text = mText.getText().toString();
                    data.imageUri = mCurrentImageUri;
                    data.captcha = mCaptcha.getText().toString();
                    data.password = mPassword.getText().toString();

                    Intent replyIntent = new Intent(PostFormActivity.this,
                            PostCreateService.class);
                    replyIntent.putExtra("data", data);
                    startService(replyIntent);

                    //Скрываем форму, она нам больше не нужна
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),
                                    "Ошибки в заполнении формы", Toast.LENGTH_SHORT)
                            .show();
                }
            }
        });
    }

    private void initFromIntent() {
        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            finish();
        }

        if (extras.containsKey(BOARD_PREFIX_ID)) {
            mBoardPrefix = extras.getString(BOARD_PREFIX_ID);
        } else {
            finish();
        }

        mThreadNumber = extras.getInt(THREAD_NUMBER_ID, -1);

        if (extras.containsKey(INIT_TEXT_ID)) {
            mInitText = extras.getString(INIT_TEXT_ID);
        } else {
            if (mSettings.getSaveLastMessage()) {
                mInitText = mSettings.getLastSavedMessage();
            } else {
                mInitText = "";
            }
        }

        if (extras.containsKey(IMAGE_URI_ID)) {
            mCurrentImageUri = extras.getString(IMAGE_URI_ID);
        } else {
            mCurrentImageUri = "";
        }

        if (extras.containsKey(PASSWORD_ID)) {
            mCurrentPassword = extras.getString(PASSWORD_ID);
        } else {
            mCurrentPassword = mSettings.getPassword();
        }

        if (extras.containsKey(NAME_ID)) {
            mCurrentName = extras.getString(NAME_ID);
        } else {
            mCurrentName = mSettings.getNickname();
        }

        if (extras.containsKey(TOPIC_ID)) {
            mCurrentTopic = extras.getString(TOPIC_ID);
        } else {
            mCurrentTopic = "";
        }
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE || requestCode == SELECT_PICTURE_KITKAT) {

                Uri selectedImageUri = data.getData();
                if (requestCode == SELECT_PICTURE_KITKAT) {
                    final int takeFlags = Intent.FLAG_GRANT_READ_URI_PERMISSION;
                    // Check for the freshest data.
                    getContentResolver().takePersistableUriPermission(selectedImageUri, takeFlags);
                }

                String uriString = selectedImageUri.toString();

                mCurrentImageUri = uriString;
                mChooseFile.setSelected(true);
                mChooseFile.setMaxHeight(600);
                mChooseFile.setAdjustViewBounds(true);

                ImageRequest request = new ImageRequest.Builder(this)
                        .data(mCurrentImageUri)
                        .crossfade(true)
                        .allowRgb565(true)
                        .target(mChooseFile)
                        .build();

                mImageLoader.enqueue(request);
            }


        } else {
            if (requestCode == SELECT_PICTURE) {
                mCurrentImageUri = "";
                mChooseFile.setSelected(false);
                mChooseFile.setAdjustViewBounds(false);
                mChooseFile.setImageResource(R.drawable.ic_button_attachment);
            }
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
    }

    private boolean validateForm() {
        if (mThreadNumber == -1) {

            if (mBoardPrefix.equals("d")) {
                if (mText.getText().toString().equals("")) {
                    return false;
                }
            } else if (mCurrentImageUri.equals("") || mText.getText().toString().equals("")) {
                return false;
            }
        } else {
            if (mCurrentImageUri.equals("") && mText.getText().toString().equals("")) {
                return false;
            }
        }

        return !mCaptcha.getText().toString().equals("");

    }

    private class CaptchaLoadTask extends AsyncTask<ImageView, Integer, Bitmap> {
        // Задача загрузки капчи
        private ImageView captchaView;
        private ProgressDialog captchaDialog;
        private Bitmap mCaptchaImage;

        @Override
        protected void onPreExecute() {
            captchaDialog = ProgressDialog.show(PostFormActivity.this,
                    "Загрузка капчи", "Ждите...");
        }

        @Override
        protected Bitmap doInBackground(ImageView... params) {
            captchaView = params[0];
            try {
                Context ctx = captchaDialog.getContext();

                String altCaptcha = "";
                if (mBoardPrefix.equals("b") || mBoardPrefix.equals("a"))
                    altCaptcha = "1";

                if (mThreadNumber == -1) {
                    mCaptchaImage = Utils.urlToBitmap(ctx, Registry.SITE_URL
                            + "/cgi-bin/captcha" + altCaptcha + ".pl/"
                            + mBoardPrefix + "/?key=mainpage");
                } else {
                    mCaptchaImage = Utils.urlToBitmap(ctx, Registry.SITE_URL
                            + "/cgi-bin/captcha" + altCaptcha + ".pl/"
                            + mBoardPrefix + "/?key=res" + mThreadNumber);
                }
                return mCaptchaImage;
            } catch (IOException e) {
                return null;
            }
        }

        @Override
        protected void onPostExecute(Bitmap result) {
            if (result == null) {
                Toast.makeText(PostFormActivity.this,
                                "Невозможно загрузить капчу", Toast.LENGTH_SHORT)
                        .show();
                captchaView.setImageResource(R.drawable.empty);
            } else {
                captchaView.setImageBitmap(result);
            }
            captchaDialog.dismiss();
        }

    }
}
