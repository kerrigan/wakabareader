package chan.reader.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Process;
import android.util.Log;
import android.util.Pair;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import chan.reader.CirnoidSettings;
import chan.reader.KUtils;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.adapters.ThreadAdapter;
import chan.reader.db.DbHandler;
import chan.reader.objects.Post;
import chan.reader.parsers.ThreadParser;
import chan.reader.services.PostDeleteService;
import coil.ImageLoader;
import okhttp3.Response;

public class ThreadActivity extends AppCompatActivity {
    ProgressDialog dialog;
    public static final String THREAD_URL_ID = "THREAD_URL";
    public static final String BOARD_PREFIX_ID = "BOARD_PREFIX";
    public static final String UPDATE_THREAD = "update_thread";
    private static final String TAG = "ThreadActivity";

    private String THREAD_URL;

    private int mOpPostId = -1;
    private List<Post> threadReplies = new ArrayList<Post>();
    private Post opPost;
    private ThreadParser parser = null;
    private ThreadAdapter mAdapter;
    private ListView replies;
    private String BOARD_PREFIX;
    //private SharedPreferences preferences;
    private CirnoidSettings mSettings;
    private DbHandler dbHandler;
    private View wrpLoadStatus;
    private TextView txtLoadStatus;
    private LoadThreadTask loadTask = null;
    private AtomicBoolean isThreadLoading;
    private ImageLoader imageLoader;


    private Integer postsCount = 0;
    private Integer imagesCount = 0;


    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter(UPDATE_THREAD));
    }


    @Override
    protected void onPause() {
        synchronized (Registry.dbMonitor) {
            if (loadTask != null && loadTask.getStatus() == Status.RUNNING) {
                loadTask.cancel(true);
            }
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
        }
        super.onPause();
    }

    @Override
    protected void onStart() {
        mSettings = new CirnoidSettings(this);
        int theme = mSettings.getTheme();// preferences.getInt(PreferencesActivity.THEME_OPTION, 0);
        setTheme(Utils.chooseTheme(theme));
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);
        setContentView(R.layout.thread);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        imageLoader = KUtils.initUntrustImageLoader(this);


        Bundle extras = getIntent().getExtras();

        Uri data = getIntent().getData();
        if (data != null) {
            String path = data.getPath();
            Pattern threadPattern = Pattern.compile("/([a-z]{1,})/res/([0-9]+).html");
            Matcher matcher = threadPattern.matcher(path);
            if (matcher.find()) {
                BOARD_PREFIX = matcher.group(1);
                THREAD_URL = Registry.SITE_URL + path;
            } else {
                loadFromExtras(extras);
            }
        } else {
            loadFromExtras(extras);
        }
        Pattern threadPattern = Pattern.compile("/([a-z]{1,})/res/([0-9]+).html");
        Matcher matcher = threadPattern.matcher(THREAD_URL);
        matcher.find();
        mOpPostId = Integer.parseInt(matcher.group(2));


        wrpLoadStatus = findViewById(R.id.wrpLoadStatus);
        txtLoadStatus = (TextView) findViewById(R.id.txtLoadStatus);
        replies = (ListView) findViewById(R.id.replies);
        mAdapter = new ThreadAdapter(this);
        dbHandler = new DbHandler(this);

        loadTask = new LoadThreadTask();
        loadTask.execute(THREAD_URL);
        replies.setAdapter(mAdapter);
        //TODO: replace with Coil analog or remove
//		replies.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
        registerForContextMenu(replies);
    }


    private void loadFromExtras(Bundle extras) {
        try {
            THREAD_URL = extras.getString(ThreadActivity.THREAD_URL_ID);
        } catch (Exception e) {
            // default thread if not found
            THREAD_URL = Registry.SITE_URL + "/b/res/2542732.html";
        }


        try {
            BOARD_PREFIX = extras.getString(ThreadActivity.BOARD_PREFIX_ID);
        } catch (Exception e) {
            // Default board if not found
            BOARD_PREFIX = "b";
        }
    }


    class LoadThreadTask extends AsyncTask<String, Integer, List<Post>> {
        private static final int SHOW_PAGE_LOADING = 0;
        private static final int SHOW_PAGE_PARSING = 1;
        private static final int FINISH_LOADING = 2;


        @Override
        protected List<Post> doInBackground(String... params) {
            String threadUrl = params[0];

            List<Post> replies = new ArrayList<Post>();
            Pattern threadPattern = Pattern.compile("/" + BOARD_PREFIX + "/res/([0-9]+).html");
            Matcher m = threadPattern.matcher(threadUrl);
            m.find();

            Integer threadId = Integer.parseInt(m.group(1));
            if (parser == null) {
                parser = new ThreadParser();
            }

            try {
                long loadStart = System.currentTimeMillis();
                publishProgress(SHOW_PAGE_LOADING);
                Response response = Utils.urlToOkHTTPConnection(ThreadActivity.this, threadUrl);

                final String etagHeader = response.header("ETag");
                String hash = "";
                if (etagHeader != null) {
                    hash = etagHeader;
                }

                if (!hash.equals("") && !dbHandler.threadChanged(BOARD_PREFIX, threadId, hash)) {
                    //Log.d("Board", "Page not changed, loading from cache");
                    replies = dbHandler.getThread(BOARD_PREFIX, threadId);
                } else {
                    //Парсинг происходит в 2 этапа
                    //Загрузка
                    String threadHtml = response.body().string();
                    long loadStop = System.currentTimeMillis() - loadStart;
                    Log.i(TAG, "BOARD LOAD STOP " + loadStop);

                    publishProgress(SHOW_PAGE_PARSING);

                    //Разбор
                    parser.parseUrl(BOARD_PREFIX, threadHtml, hash);
                    replies = parser.getComments();
                    publishProgress(FINISH_LOADING);
                    final List<Post> repliesToThread = new ArrayList<Post>(replies);
                    if (Registry.CACHE_ENABLED) {

                        final String hashFinal = hash;
                        Thread dbThread = new Thread(new Runnable() {

                            public void run() {
                                synchronized (Registry.dbMonitor) {
                                    dbHandler.upsertThread(BOARD_PREFIX,
                                            repliesToThread, hashFinal);
                                }
                            }
                        });

                        dbThread.setPriority(Process.THREAD_PRIORITY_BACKGROUND);
                        dbThread.start();
                    }
                }
            } catch (Exception e) {
                replies = dbHandler.getThread(BOARD_PREFIX, threadId);
            } finally {
                publishProgress(FINISH_LOADING);
            }
            Post.buildOutLinks(replies, ThreadActivity.this);

            Pair<Integer, Integer> info = Post.countPostsData(replies);

            postsCount = info.first;
            imagesCount = info.second;

            threadReplies = replies;
            return threadReplies;
        }

        @Override
        protected void onPreExecute() {
            isThreadLoading = new AtomicBoolean(true);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (wrpLoadStatus == null) {
                return;
            }
            int code = values[0];
            switch (code) {
                case SHOW_PAGE_LOADING:
                    replies.setVisibility(View.GONE);
                    wrpLoadStatus.setVisibility(View.VISIBLE);
                    txtLoadStatus.setText(R.string.status_page_load);
                    break;

                case SHOW_PAGE_PARSING:
                    txtLoadStatus.setText(R.string.status_page_parsing);
                    break;
                case FINISH_LOADING:
                    isThreadLoading.set(false);
                    supportInvalidateOptionsMenu();
                    break;
            }
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(List<Post> result) {
            loadTask = null;
            replies.setVisibility(View.VISIBLE);
            replies.setAnimation(new AlphaAnimation(0, 1));
            mAdapter.setPosts(result);
            wrpLoadStatus.setVisibility(View.GONE);
            // TODO: Перенести строчку в ресурсы
            if (result.size() > 0) {
                opPost = result.get(0);
                setTitle("Тред #" + opPost.id + " - " + imagesCount + "/" + postsCount);
            }
        }

    }

    private void showReplyForm(String initialText) {
        Intent intent = new Intent(this, PostFormActivity.class);
        intent.putExtra(PostFormActivity.BOARD_PREFIX_ID, BOARD_PREFIX);
        intent.putExtra(PostFormActivity.THREAD_NUMBER_ID, mOpPostId);
        intent.putExtra(PostFormActivity.INIT_TEXT_ID, initialText);
        startActivity(intent);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.thread_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem gallery = (MenuItem) menu.findItem(R.id.show_gallery);
        if (loadTask != null && isThreadLoading.get()) {
            gallery.setEnabled(false);
        } else {
            gallery.setEnabled(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Выбор пункта меню в треде
        switch (item.getItemId()) {
            case R.id.new_reply:
                //Новый ответ
                showReplyForm("");
                break;

            case R.id.refresh:
                //Обновление треда
                if (loadTask != null && loadTask.getStatus() == Status.RUNNING) {
                    loadTask.cancel(true);
                }
                loadTask = new LoadThreadTask();
                loadTask.execute(THREAD_URL);
                supportInvalidateOptionsMenu();
                break;
            case R.id.show_gallery:
                Intent intent = new Intent();
                intent.putExtra(ThreadGalleryActivity.BOARD_PREFIX_ID, BOARD_PREFIX);
                intent.putExtra(ThreadGalleryActivity.THREAD_ID, mOpPostId);
                intent.setClass(this, ThreadGalleryActivity.class);
                this.startActivity(intent);
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        // Всплывающее меню треда на доске
        super.onCreateContextMenu(menu, v, menuInfo);
        android.view.MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.reply_context_menu, menu);
    }

    @Override
    public boolean onContextItemSelected(android.view.MenuItem item) {

        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        ThreadAdapter adapter = (ThreadAdapter) replies.getAdapter();
        Post post = (Post) adapter.getItem(info.position);

        switch (item.getItemId()) {
            case R.id.reply_to_reply:
                showReplyForm(">>" + post.id + " ");
                break;
            case R.id.citation_from_post:
                Utils.showCopyFromPostDialog(this, post);
                break;
            case R.id.delete_post:
                //TODO: call service to delete thread
                PostDeleteService.deletePost(this, BOARD_PREFIX, post.id, false);
                break;
            default:
                break;
        }
        return super.onContextItemSelected(item);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(UPDATE_THREAD) && intent.hasExtra(UPDATE_THREAD)) {
                if (intent.getStringExtra(UPDATE_THREAD).equals(BOARD_PREFIX + opPost.id)) {
                    loadTask = new LoadThreadTask();
                    loadTask.execute(THREAD_URL);
                }
            }
        }
    };
}
