package chan.reader.activities;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.ShareActionProvider;
import androidx.core.view.MenuItemCompat;

import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.Window;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import java.util.ArrayList;
import java.util.Collections;

import chan.reader.CirnoidSettings;
import chan.reader.PlatformUtils;
import chan.reader.R;
import chan.reader.Utils;
import chan.reader.services.ImageDownloadService;

public class PictureActivity extends AppCompatActivity {
    public static final String IMAGE_URL = "image_url";
    private String mImageUrl;
    private String mImageName;

    private ShareActionProvider mShareActionProvider;
    private WebView viewer;
    private CirnoidSettings mSettings;

    int DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE = 300;


    private class PictureClient extends WebChromeClient {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            int progress = (Window.PROGRESS_END - Window.PROGRESS_START) / 100 * newProgress;
            setSupportProgress(progress);
        }

    }


    private Intent createShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/*");
        shareIntent.putExtra(Intent.EXTRA_TEXT, mImageUrl);
        return shareIntent;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.picture_menu, menu);

        MenuItem item = menu.findItem(R.id.image_share);

        mShareActionProvider = (ShareActionProvider) MenuItemCompat.getActionProvider(item);
        mShareActionProvider.setShareIntent(createShareIntent());

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // TODO Auto-generated method stub
        switch (item.getItemId()) {
            case R.id.image_save:
                //startDownload(new ArrayList<String>(Collections.singletonList(mImageUrl)));
                checkPermissionAndDownload();
                break;
            case android.R.id.home:
                onBackPressed();
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStart() {
        mSettings = new CirnoidSettings(this);
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mSettings = new CirnoidSettings(this);
        setTheme(Utils.chooseTheme(mSettings.getTheme()));
        super.onCreate(savedInstanceState);


        Bundle extras = getIntent().getExtras();
        requestWindowFeature(Window.FEATURE_PROGRESS);
        try {
            mImageUrl = extras.getString(IMAGE_URL);
            mImageName = mImageUrl.split("/src/")[1];
        } catch (Exception e) {
            mImageUrl = "http://iichan.hk/gf/src/1352537322475.gif";
            mImageName = mImageUrl.split("/src/")[1];
        }
        setSupportProgressBarIndeterminateVisibility(true);
        setTitle(mImageName);

        overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);
        setContentView(R.layout.picture_viewer);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        viewer = (WebView) findViewById(R.id.image_viewer);

        viewer.setBackgroundColor(0);
        viewer.setBackgroundResource(R.drawable.picture_bg);
        viewer.setWebViewClient(
                new SSLTolerantWebViewClient()
        );
        WebSettings settings = viewer.getSettings();
        settings.setBuiltInZoomControls(true);
        settings.setUseWideViewPort(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUserAgentString(Utils.getUserAgent(this));
        viewer.setWebChromeClient(new PictureClient());
        viewer.loadUrl(mImageUrl);
    }


    //TODO: fix this
    private class SSLTolerantWebViewClient extends WebViewClient {
        @SuppressLint("WebViewClientOnReceivedSslError")
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            handler.proceed(); // Ignore SSL certificate errors
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
    }

    private void checkPermissionAndDownload() {
        Uri savedImagesPathUri = Uri.parse(mSettings.getSavedImagesPath());
        if (PlatformUtils.hasPermissionToUri(this, savedImagesPathUri)) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            startActivityForResult(intent, DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE);
        } else {
            startDownload(new ArrayList<>(Collections.singletonList(mImageUrl)));
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent resultData) {
        super.onActivityResult(requestCode, resultCode, resultData);
        if (requestCode == DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                Uri uri = null;
                if (resultData != null) {
                    uri = resultData.getData();
                    // Perform operations on the document using its URI.
                    final int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION
                            | Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

                    // Save permission to uri
                    getContentResolver().takePersistableUriPermission(uri, takeFlags);

                    mSettings.setSavedImagesPath(uri.toString());
                    startDownload(new ArrayList<>(Collections.singletonList(mImageUrl)));
                }
            }
        }
    }

    private void startDownload(ArrayList<String> urls) {
        Intent downloadIntent = new Intent(this, ImageDownloadService.class);
        downloadIntent.putExtra("images", urls);
        startService(downloadIntent);
    }
}
