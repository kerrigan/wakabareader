package chan.reader.activities;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Process;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import chan.reader.Boards;
import chan.reader.CirnoidSettings;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.adapters.BoardAdapter;
import chan.reader.db.DbHandler;
import chan.reader.objects.BoardUrl;
import chan.reader.objects.Post;
import chan.reader.parsers.BoardParser;
import chan.reader.services.PostDeleteService;
import okhttp3.Response;

public class BoardActivityDeprecated extends AppCompatActivity
        //implements OnItemClickListener
{
    private static final String TAG = BoardActivityDeprecated.class.getSimpleName();
    public static final String BOARD_URL_ID = "boardurl";
    public static final String BOARD_TITLE_ID = "boardtitle";
    public static final String BOARD_PREFIX_ID = "boardprefix";
    public static final String UPDATE_BOARD = "update_board";

    private BoardParser parser;
    private BoardAdapter adapter;
    private List<Post> lastPosts;
    private String BASE_BOARD_URL;
    private String BOARD_PREFIX;
    private int currentPage = 0;
    private String BOARD_TITLE;
    private DbHandler dbHandler;
    private ListView opPosts;
    private CirnoidSettings settings;
    //private Handler mHandler;
    private View wrpLoadStatus;
    private TextView txtLoadStatus;
    private LoadThreadsTask loadTask = null;

    private String getCurrentBoardUrl(int page) {
        if (page == 0) {
            return BASE_BOARD_URL;
        }
        return BASE_BOARD_URL + "/" + page + ".html";
    }

    @Override
    protected void onStart() {
        super.onStart();
        settings = new CirnoidSettings(this);
        setTheme(Utils.chooseTheme(settings.getTheme()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        LocalBroadcastManager.getInstance(this).registerReceiver(mReceiver, new IntentFilter("BOARDACTIVITYUPDATES"));
    }

    @Override
    protected void onPause() {
        synchronized (Registry.dbMonitor) {
            LocalBroadcastManager.getInstance(this).unregisterReceiver(mReceiver);
            if (loadTask != null && loadTask.getStatus() == Status.RUNNING) {
                loadTask.cancel(true);
            }
        }

        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);

        lastPosts = new LinkedList<Post>();

        setContentView(R.layout.board);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        Bundle extras = getIntent().getExtras();

        Uri data = getIntent().getData();
        if (data != null) {
            String path = data.getPath();
            Pattern threadPattern = Pattern.compile("/([a-z]{1,})/");
            Matcher matcher = threadPattern.matcher(path);
            if (matcher.find()) {
                BOARD_PREFIX = matcher.group(1);
                BASE_BOARD_URL = Registry.SITE_URL + "/" + BOARD_PREFIX;
                BoardUrl boardUrl = Boards.getInstance(getApplicationContext()).findByPrefix(BOARD_PREFIX);
                if (boardUrl != null) {
                    BOARD_TITLE = boardUrl.title + " - " + boardUrl.shortTitle;
                }
            } else {
                loadFromExtras(extras);
            }
        } else {
            loadFromExtras(extras);
        }

        setTitle(BOARD_TITLE);


        wrpLoadStatus = findViewById(R.id.wrpLoadStatus);
        txtLoadStatus = (TextView) findViewById(R.id.txtLoadStatus);
        opPosts = (ListView) findViewById(R.id.opposts);
        adapter = new BoardAdapter(this);
        dbHandler = new DbHandler(this);
        loadTask = new LoadThreadsTask();
        loadTask.executeOnExecutor(Executors.newSingleThreadExecutor(), getCurrentBoardUrl(currentPage));
        opPosts.setAdapter(adapter);

        // TODO: check in coil or simply remove
        //opPosts.setOnScrollListener(new PauseOnScrollListener(ImageLoader.getInstance(), true, true));
        registerForContextMenu(opPosts);
    }

    private void loadFromExtras(Bundle extras) {
        try {
            BASE_BOARD_URL = extras.getString(BoardActivityDeprecated.BOARD_URL_ID);
        } catch (Exception e) {
            BASE_BOARD_URL = Registry.SITE_URL + "/b";
        }
        try {
            BOARD_TITLE = extras.getString(BoardActivityDeprecated.BOARD_TITLE_ID);
        } catch (Exception e) {
            BOARD_TITLE = "/b/ - Бред";
        }
        try {
            BOARD_PREFIX = extras.getString(BoardActivityDeprecated.BOARD_PREFIX_ID);
        } catch (Exception e) {
            BOARD_PREFIX = "b";
        }
    }

    private void showPostForm(String initialText, Integer threadId) {
        Intent intent = new Intent(this, PostFormActivity.class);
        intent.putExtra(PostFormActivity.BOARD_PREFIX_ID, BOARD_PREFIX);
        if (threadId != -1) {
            intent.putExtra(PostFormActivity.THREAD_NUMBER_ID, threadId);
        }
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.board_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        MenuItem prev = (MenuItem) menu.findItem(R.id.prev_page);
        if (currentPage == 0) {
            prev.setVisible(false);
        }
        if (!prev.isVisible() && currentPage > 0) {
            prev.setVisible(true);
        }
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.new_thread:
                showPostForm();
                break;
            case R.id.refresh:
                if (loadTask != null && loadTask.getStatus() == Status.RUNNING) {
                    loadTask.cancel(true);
                }
                loadTask = new LoadThreadsTask();
                loadTask.execute(getCurrentBoardUrl(currentPage));
                opPosts.setSelection(0);
                break;
            case R.id.prev_page:
                if (currentPage > 0)
                    currentPage--;
                new LoadThreadsTask().execute(getCurrentBoardUrl(currentPage));
                opPosts.setSelection(0);
                supportInvalidateOptionsMenu();
                break;
            case R.id.next_page:
                currentPage++;
                new LoadThreadsTask().execute(getCurrentBoardUrl(currentPage));
                opPosts.setSelection(0);
                supportInvalidateOptionsMenu();
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showPostForm() {
        showPostForm("", -1);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenuInfo menuInfo) {
        // Всплывающее меню треда на доске
        super.onCreateContextMenu(menu, v, menuInfo);
        android.view.MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.thread_context_menu, menu);
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) menuInfo;
        Post post = (Post) opPosts.getAdapter().getItem(info.position);
        if (dbHandler.isBanned(post.board, post.id)) {
            menu.findItem(R.id.ban_thread).setVisible(false);
        } else {
            menu.findItem(R.id.unban_thread).setVisible(false);
        }
        if (dbHandler.isSubscribed(post.board, post.id)) {
            menu.findItem(R.id.subscribe_thread).setVisible(false);
        } else {
            menu.findItem(R.id.unsubscribe_thread).setVisible(false);
        }
    }

    public boolean onContextItemSelected(android.view.MenuItem item) {
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo) item
                .getMenuInfo();
        BoardAdapter adapter = (BoardAdapter) opPosts.getAdapter();
        Post post = (Post) adapter.getItem(info.position);
        switch (item.getItemId()) {
            case R.id.unsubscribe_thread:
                dbHandler.unsubscribeThread(post.board, post.id);
                Toast.makeText(this, "Подписка на тред #" + post.id + " удалена",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.subscribe_thread:
                // Подписаться на тред
                dbHandler.subscribeThread(post.board, post.id);
                Toast.makeText(this, "Добавлена подписка на тред #" + post.id,
                        Toast.LENGTH_SHORT).show();
                break;

            case R.id.ban_thread:
                // Забанить тред
                adapter.banPost(info.position);
                Toast.makeText(this, "Тред #" + post.id + " скрыт",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.unban_thread:
                // Разбанить тред
                adapter.unbanPost(info.position);
                Toast.makeText(this, "Тред #" + post.id + " разбанен",
                        Toast.LENGTH_SHORT).show();
                break;
            case R.id.reply_to_thread:
                showPostForm(">>" + post.id + " ", post.id);
                break;
            case R.id.citation_from_post:
                Utils.showCopyFromPostDialog(this, post);
                break;
            case R.id.delete_thread:
                //call service to delete thread
                PostDeleteService.deletePost(this, post.board, post.id, true);
                break;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
    }

    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {

        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(UPDATE_BOARD) && intent.hasExtra(UPDATE_BOARD)) {
                if (intent.getStringExtra(UPDATE_BOARD).equals(BOARD_PREFIX)) {
                    loadTask = new LoadThreadsTask();
                    loadTask.execute(getCurrentBoardUrl(currentPage));
                }
            }
        }
    };

    private class LoadThreadsTask extends
            AsyncTask<String, Integer, List<Post>> {

        private static final int SHOW_PAGE_LOADING = 0;
        private static final int SHOW_PAGE_PARSING = 1;
        private static final int FINISH_LOADING = 2;

        @Override
        protected List<Post> doInBackground(String... params) {
            String boardUrl = params[0];


            List<Post> opPosts = new ArrayList<Post>();
            parser = new BoardParser();
            try {
                long loadStart = System.currentTimeMillis();
                publishProgress(SHOW_PAGE_LOADING);
                Response response = Utils.urlToOkHTTPConnection(BoardActivityDeprecated.this, boardUrl);
                String etagHeader = response.header("ETag");

                String hash = "";
                if (etagHeader != null) {
                    hash = etagHeader;
                }

                if (!hash.equals("") && !dbHandler.pageChanged(BOARD_PREFIX, currentPage, hash)) {
                    Log.d("Board", "Page not changed, loading from cache");
                    opPosts = dbHandler.getThreads(BOARD_PREFIX, currentPage);
                    publishProgress(FINISH_LOADING);
                } else {
                    //Парсинг происходит в 2 этапа
                    //Диалог загрузка
                    String boardHtml = response.body().string();

                    long loadStop = System.currentTimeMillis() - loadStart;
                    Log.i(TAG, "BOARD LOAD STOP " + loadStop);

                    publishProgress(SHOW_PAGE_PARSING);
                    //END

                    //Разбор
                    parser.parseUrl(BOARD_PREFIX, boardHtml, hash);
                    opPosts = parser.getOpPosts();
                    publishProgress(FINISH_LOADING);
                    //END

                    final List<Post> postsToThread = new ArrayList<Post>(opPosts);
                    if (Registry.CACHE_ENABLED) {
                        final String finalHash = hash;
                        Thread dbThread = new Thread(new Runnable() {

                            public void run() {
                                synchronized (Registry.dbMonitor) {
                                    dbHandler.upsertBoardPage(BOARD_PREFIX, currentPage,
                                            postsToThread, finalHash);
                                    //Зачистка кеша после каждой загрузки
                                    dbHandler.cleanBoardCache(BOARD_PREFIX, settings.getThreadCacheSize());
                                }
                            }
                        });
                        dbThread.setPriority(Process.THREAD_PRIORITY_BACKGROUND);
                        dbThread.start();
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                opPosts = dbHandler.getThreads(BOARD_PREFIX, currentPage);
                publishProgress(FINISH_LOADING);
            }

            lastPosts = opPosts;
            return lastPosts;
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            if (wrpLoadStatus == null) {
                return;
            }
            int code = values[0];
            switch (code) {
                case SHOW_PAGE_LOADING:
                    opPosts.setVisibility(View.GONE);
                    wrpLoadStatus.setVisibility(View.VISIBLE);
                    txtLoadStatus.setText(R.string.status_page_load);
                    break;

                case SHOW_PAGE_PARSING:
                    txtLoadStatus.setText(R.string.status_page_parsing);
                    break;
                case FINISH_LOADING:
                    wrpLoadStatus.setVisibility(View.GONE);
                    break;
            }
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected void onPostExecute(List<Post> result) {
            loadTask = null;
            opPosts.setVisibility(View.VISIBLE);
            adapter.setPosts(result);
        }
    }
}
