package chan.reader.activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.appcompat.app.AppCompatActivity;
import chan.reader.CirnoidSettings;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.adapters.SubscriptionsAdapter;
import chan.reader.db.DbHandler;
import chan.reader.objects.Post;

public class SubscriptionsActivity extends AppCompatActivity {
	private SubscriptionsAdapter adapter;

	@Override
	protected void onStart() {
        CirnoidSettings settings = new CirnoidSettings(this);
		setTheme(Utils.chooseTheme(settings.getTheme()));
		super.onStart();
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);
		setContentView(R.layout.subscriptions_list);
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
		
		setTitle("Подписки на треды");
		ListView subscriptions = (ListView)findViewById(R.id.subscription_list);
		adapter = new SubscriptionsAdapter(this);
		subscriptions.setAdapter(adapter);
		adapter.setPosts(new DbHandler(this).getSubscribed());
		
		subscriptions.setOnItemClickListener(new ListView.OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view, int position,
					long id) {
				// TODO Auto-generated method stub
				Post post = adapter.getItem(position);
				String THREAD_URL = Registry.SITE_URL + post.board + "/res/" + post.id + ".html";
				Intent intent = new Intent();
				intent.putExtra(ThreadActivity.BOARD_PREFIX_ID, post.board);
				intent.putExtra(ThreadActivity.THREAD_URL_ID, THREAD_URL);
				intent.setClass(SubscriptionsActivity.this, ThreadActivity.class);
				startActivity(intent);
			}
		});
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
	}
}
