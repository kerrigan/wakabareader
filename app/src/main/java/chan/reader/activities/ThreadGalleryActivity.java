package chan.reader.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import java.util.ArrayList;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import chan.reader.CirnoidSettings;
import chan.reader.PlatformUtils;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.adapters.ThreadImagesAdapter;
import chan.reader.objects.Post;
import chan.reader.services.ImageDownloadService;

public class ThreadGalleryActivity extends AppCompatActivity implements
        OnItemClickListener {
    public static final String THREAD_ID = "THREAD_ID";
    public static final String BOARD_PREFIX_ID = "BOARD_PREFIX";

    private int THREAD_NUMBER;
    private String BOARD_PREFIX;
    private ThreadImagesAdapter mAdapter;
    private GridView gallery;
    private int mSelectedCount = 0;
    private boolean SELECT_MODE = false;
    private CirnoidSettings mSettings = null;

    private final static int DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE = 300;
	private ArrayList<String> mImageUrls;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        //setTheme(R.style.Theme_Sherlock);
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);
        setContentView(R.layout.thread_gallery);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        Bundle extras = getIntent().getExtras();
        try {
            THREAD_NUMBER = extras.getInt(ThreadGalleryActivity.THREAD_ID);
        } catch (Exception e) {
            // default thread if not found
            THREAD_NUMBER = 2408307;
        }
        try {
            BOARD_PREFIX = extras.getString(ThreadActivity.BOARD_PREFIX_ID);
        } catch (Exception e) {
            // Default board if not found
            BOARD_PREFIX = "b";
        }

        setTitle("/" + BOARD_PREFIX + "/" + " - " + THREAD_NUMBER);
        gallery = (GridView) findViewById(R.id.thread_pics);
        mAdapter = new ThreadImagesAdapter(this, THREAD_NUMBER, BOARD_PREFIX);

        gallery.setAdapter(mAdapter);

        gallery.setOnItemClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mSettings = new CirnoidSettings(this);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long id) {
        Post post = mAdapter.getItem(position);
        if (SELECT_MODE) {

            if (post.selected) {
                post.selected = false;
                mSelectedCount--;
            } else {
                post.selected = true;
                mSelectedCount++;
            }
            supportInvalidateOptionsMenu();
            mAdapter.notifyDataSetInvalidated();
        } else {
            showImage(post.image);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.image_save:
                ArrayList<String> urls = new ArrayList<String>();
                for (Post post : mAdapter.getItems()) {
                    if (post.selected)
                        urls.add(Registry.SITE_URL + post.image);
                }

				mImageUrls = urls;
				checkPermissionsAndDownload();
                break;

            case R.id.mark_mode:
                if (SELECT_MODE) {
                    SELECT_MODE = false;
                    mAdapter.deselectAll();
                    mAdapter.notifyDataSetInvalidated();
                } else
                    SELECT_MODE = true;
                supportInvalidateOptionsMenu();
                break;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.gallery_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        if (mSelectedCount > 0)
            menu.findItem(R.id.image_save).setEnabled(true);
        else
            menu.findItem(R.id.image_save).setEnabled(false);
        if (SELECT_MODE)
            menu.findItem(R.id.image_save).setVisible(true);
        else
            menu.findItem(R.id.image_save).setVisible(false);
        return true;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
    }

    private void startDownload(ArrayList<String> urls) {
        Intent downloadIntent = new Intent(this, ImageDownloadService.class);
        downloadIntent.putExtra("images", urls);
        startService(downloadIntent);
    }

    private void showImage(String url) {
        Intent imageViewerIntent = new Intent(this, PictureActivity.class);
        imageViewerIntent.putExtra(PictureActivity.IMAGE_URL, Registry.SITE_URL
                + url);
        startActivity(imageViewerIntent);
    }

    private void checkPermissionsAndDownload() {
        Uri savedImagesPathUri = Uri.parse(mSettings.getSavedImagesPath());
        if (!PlatformUtils.hasPermissionToUri(this, savedImagesPathUri)) {
            Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
            startActivityForResult(intent, DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE);
        } else {
            startDownload(mImageUrls);
        }
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent resultData) {
		super.onActivityResult(requestCode, resultCode, resultData);
		if (requestCode == DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				Uri uri = null;
				if (resultData != null) {
					uri = resultData.getData();
					// Perform operations on the document using its URI.
					final int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION
							| Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

					// Save permission to uri
					getContentResolver().takePersistableUriPermission(uri, takeFlags);

					mSettings.setSavedImagesPath(uri.toString());
					startDownload(mImageUrls);
				}
			} else {
				mImageUrls = null;
			}
		}
	}
}
