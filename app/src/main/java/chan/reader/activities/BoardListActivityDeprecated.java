package chan.reader.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import chan.reader.Boards;
import chan.reader.CirnoidSettings;
import chan.reader.R;
import chan.reader.adapters.BoardGridAdapter;
import chan.reader.objects.BoardUrl;
import chan.reader.services.ThreadsUpdateService;

public class BoardListActivityDeprecated extends AppCompatActivity implements OnItemClickListener {
    private static final String TAG = BoardActivityDeprecated.class.getSimpleName();
	private BoardGridAdapter mAdapter;
    private CirnoidSettings mSettings;
    //private SharedPreferences preferences;
	public final static int REQUEST_PERMISSIONS_CODE = 999;
	private GridView boardList;

	/** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	System.setProperty("http.agent", "");

        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);
        setContentView(R.layout.boards_grid);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setTitle(getResources().getString(R.string.choose_board));
        
        BoardUrl boards[] = Boards.getInstance(getApplicationContext()).getArray();

        
        mAdapter = new BoardGridAdapter(this, boards);
        boardList = (GridView) findViewById(R.id.icons_grid);
        boardList.setAdapter(mAdapter);

        mSettings = new CirnoidSettings(this);
        
        boolean isUpdaterEnabled = mSettings.getUpdateThreadsEnabled();//= preferences.getBoolean(PreferencesActivity.UPDATE_THREADS, false);
        if(isUpdaterEnabled){
        	startService(new Intent(this, ThreadsUpdateService.class));
        }

		boardList.setOnItemClickListener(this);
	}

	@Override
	public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
		super.onRequestPermissionsResult(requestCode, permissions, grantResults);
		if (requestCode == REQUEST_PERMISSIONS_CODE) {
			if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
				boardList.setOnItemClickListener(this);
			} else {
				ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_PERMISSIONS_CODE);
			}
		}
	}

	@Override
    protected void onStart() {
    	super.onStart();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
    	MenuInflater inflater = getMenuInflater();
    	inflater.inflate(R.menu.main_menu, (Menu)menu);
    	return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
    	Intent intent;
    	switch (item.getItemId()) {
		case R.id.settings:
			intent = new Intent(getBaseContext(), PreferencesActivity.class);
			startActivity(intent);
			break;
		case R.id.subscriptions:
			intent = new Intent(getBaseContext(), SubscriptionsActivity.class);
			startActivity(intent);
			break;
		case android.R.id.home:
			onBackPressed();
            return true;
		default:
			break;
		}
    	return super.onOptionsItemSelected(item);
    }
    
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
	}

	@Override
	public void onItemClick(AdapterView<?> adapterView, View view,
			int position, long id) {
		BoardUrl board = mAdapter.getItem(position);
		Intent intent = new Intent();
		intent.putExtra(BoardActivityDeprecated.BOARD_URL_ID, board.url);
		intent.putExtra(BoardActivityDeprecated.BOARD_TITLE_ID, board.title + " - " + board.shortTitle);
		intent.putExtra(BoardActivityDeprecated.BOARD_PREFIX_ID, board.shortTitle.replace("/", ""));
		intent.setClass(this, BoardActivityDeprecated.class);
		startActivity(intent);
	}
}
