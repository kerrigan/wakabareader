package chan.reader.activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceActivity;
import android.view.MenuItem;

import chan.reader.CirnoidSettings;
import chan.reader.R;
import chan.reader.services.ThreadsUpdateService;

public class PreferencesActivity extends PreferenceActivity implements
		OnPreferenceChangeListener {

	public static final String SAVED_IMAGES_PATH_OPTION = "saved_images_path";
	public static final String NICKNAME_OPTION = "nickname";
	public static final String UPDATE_THREADS = "update_threads";
	public static final String PASSWORD_OPTION = "password";
	public static final String THEME_OPTION = "theme";
	public static final String CLEAR_CACHE_OPTION = "clear_cache";
	public static final String THREAD_CACHE_SIZE_OPTION = "max_threads";
	public static final String IMAGES_CACHE_SIZE_OPTION = "max_images";
	public static final String DOWNLOAD_IMAGES_OPTION = "download_images";
	public static final String ENABLE_VIBRATION = "enable_vibration";
	public static final String SHOW_FORM_FIELDS_OPTION = "show_form_fields";
    public static final String SAVE_LAST_MESSAGE_OPTION = "save_last_message";
    public static final String SUBSCRIBE_TO_POSTED_OPTION = "subscribe_to_posted";

	private static final int DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE = 300;

	//private SharedPreferences preferences;
    private CirnoidSettings settings;

	@Override
	protected void onStart() {
		//preferences = getSharedPreferences("options", MODE_PRIVATE);
        settings = new CirnoidSettings(this);

		CheckBoxPreference download_images = (CheckBoxPreference) findPreference(DOWNLOAD_IMAGES_OPTION);
		CheckBoxPreference show_form_fields = (CheckBoxPreference) findPreference(SHOW_FORM_FIELDS_OPTION);
        CheckBoxPreference save_last_message = (CheckBoxPreference)findPreference(SAVE_LAST_MESSAGE_OPTION);
        CheckBoxPreference subscribe_to_posted = (CheckBoxPreference)findPreference(SUBSCRIBE_TO_POSTED_OPTION);

		ListPreference theme = (ListPreference) findPreference(THEME_OPTION);
		ListPreference cache_size = (ListPreference) findPreference(THREAD_CACHE_SIZE_OPTION);
		ListPreference images_cache_size = (ListPreference) findPreference(IMAGES_CACHE_SIZE_OPTION);


		Preference download_path = findPreference(SAVED_IMAGES_PATH_OPTION);
        EditTextPreference nickname = (EditTextPreference)findPreference(NICKNAME_OPTION);
        EditTextPreference password = (EditTextPreference)findPreference(PASSWORD_OPTION);


        nickname.setText(settings.getNickname());
        password.setText((settings.getPassword()));

		//download_path.setText(settings.getSavedImagesPath());
		download_path.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
			@Override
			public boolean onPreferenceClick(Preference preference) {
				//TODO: request SAF
				Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT_TREE);
				startActivityForResult(intent, DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE);
				return false;
			}
		});
		download_images.setChecked(settings.getImagesEnabled());

		show_form_fields.setChecked(settings.getShowFormFields());
        subscribe_to_posted.setChecked(settings.getSubscribeToPosted());
		theme.setValueIndex(settings.getTheme());
		cache_size.setValue(Integer.toString(settings.getThreadCacheSize()));
		images_cache_size.setValue(Integer.toString(settings.getImagesCacheSize()));
        save_last_message.setDefaultValue(settings.getSaveLastMessage());
		super.onStart();
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		//setTheme(R.style.Theme_Sherlock);
		super.onCreate(savedInstanceState);
		overridePendingTransition(R.anim.activity_push_show, R.anim.activity_push_hide);
		addPreferencesFromResource(R.xml.preferences);
		
		getActionBar().setDisplayHomeAsUpEnabled(true);

		Preference download_images = findPreference(DOWNLOAD_IMAGES_OPTION);
		EditTextPreference password = (EditTextPreference) findPreference(PASSWORD_OPTION);
		Preference clear_cache = findPreference(CLEAR_CACHE_OPTION);
		EditTextPreference nickname = (EditTextPreference) findPreference(NICKNAME_OPTION);
		ListPreference theme = (ListPreference) findPreference(THEME_OPTION);
		Preference download_path = findPreference(SAVED_IMAGES_PATH_OPTION);

		CheckBoxPreference show_form_fields = (CheckBoxPreference) findPreference(SHOW_FORM_FIELDS_OPTION);
		ListPreference cache_size = (ListPreference) findPreference(THREAD_CACHE_SIZE_OPTION);
		ListPreference images_cache_size = (ListPreference) findPreference(IMAGES_CACHE_SIZE_OPTION);
        CheckBoxPreference save_last_message = (CheckBoxPreference)findPreference(SAVE_LAST_MESSAGE_OPTION);
		
		CheckBoxPreference update_threads = (CheckBoxPreference) findPreference(UPDATE_THREADS);

		
		
		download_images.setOnPreferenceChangeListener(this);
		password.setOnPreferenceChangeListener(this);
		clear_cache.setOnPreferenceChangeListener(this);
		nickname.setOnPreferenceChangeListener(this);
		theme.setOnPreferenceChangeListener(this);
		download_path.setOnPreferenceChangeListener(this);
		show_form_fields.setOnPreferenceChangeListener(this);
		cache_size.setOnPreferenceChangeListener(this);
		images_cache_size.setOnPreferenceChangeListener(this);
		update_threads.setOnPreferenceChangeListener(this);
	}
	
	@Override
	public void onBackPressed() {
		super.onBackPressed();
		overridePendingTransition(R.anim.activity_pop_show, R.anim.activity_pop_hide);
	}

	@Override
	public boolean onPreferenceChange(Preference preference, Object newValue) {
		// Сохранение настроек
		String preferenceKey = preference.getKey();
		if (preferenceKey.equals(NICKNAME_OPTION)) {
			//Настройка ника
			String nickname = (String) newValue;
            settings.setNickName(nickname);
		} else if (preferenceKey.equals(PASSWORD_OPTION)) {
			//Настройка пароля
			String password = (String) newValue;
            settings.setPassword(password);
		} else if (preferenceKey.equals(SHOW_FORM_FIELDS_OPTION)) {
			//Настройка показа полей 
			boolean showFormFields = (Boolean) newValue;
            settings.setShowFormFields(showFormFields);
		} else if (preferenceKey.equals(DOWNLOAD_IMAGES_OPTION)) {
			//Настройка загрузки изображений
			boolean dlImages = (Boolean) newValue;
            settings.setImagesEnabled(dlImages);
		} else if (preferenceKey.equals(SAVED_IMAGES_PATH_OPTION)) {
			//Настройка пути для сохранения изображений
			String path = (String) newValue;
            settings.setSavedImagesPath(path);
		} else if (preferenceKey.equals(THREAD_CACHE_SIZE_OPTION)) {
			//Размер кеша для отдельной доски
			String size = (String) newValue;
            settings.setThreadCacheSize(Integer.valueOf(size));
		} else if (preferenceKey.equals(IMAGES_CACHE_SIZE_OPTION)) {
			//Размер кеша миниатюр
			String size = (String) newValue;
            settings.setImagesCacheSize(Integer.valueOf(size));
		} else if(preferenceKey.equals(THEME_OPTION)){
			//Размер шрифта
			int theme = Integer.parseInt((String)newValue);
            settings.setTheme(theme);
		} else if(preferenceKey.equals(UPDATE_THREADS)){
            Boolean enabled = (Boolean) newValue;
			Intent intent = new Intent();
			intent.setClass(this, ThreadsUpdateService.class);
			if((Boolean)newValue){
				startService(intent);
			} else {
				stopService(intent);
			}
            settings.setUpdateThreadsEnabled(enabled);
		} else if(preferenceKey.equals(SAVE_LAST_MESSAGE_OPTION)){
            Boolean enabled = (Boolean) newValue;
            settings.setSaveLastMessage(enabled);
        } else if(preferenceKey.equals(SUBSCRIBE_TO_POSTED_OPTION)){
            Boolean enabled = (Boolean)newValue;
            settings.setSubscribeToPosted(enabled);
        }
		return true;
	}


	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			onBackPressed();
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent resultData) {
		super.onActivityResult(requestCode, resultCode, resultData);
		if (requestCode == DOWNLOAD_IMAGE_PATH_SELECT_REQUEST_CODE) {
			if (resultCode == Activity.RESULT_OK) {
				Uri uri = null;
				if (resultData != null) {
					uri = resultData.getData();
					// Perform operations on the document using its URI.
					final int takeFlags = (Intent.FLAG_GRANT_READ_URI_PERMISSION
							| Intent.FLAG_GRANT_WRITE_URI_PERMISSION);

					// Save permission to uri
					getContentResolver().takePersistableUriPermission(uri, takeFlags);
					settings.setSavedImagesPath(uri.toString());
				}
			}
		}
	}
}
