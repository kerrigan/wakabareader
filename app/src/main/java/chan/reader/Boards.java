package chan.reader;

import android.content.Context;

import org.yaml.snakeyaml.Yaml;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import chan.reader.objects.BoardUrl;

public class Boards {
	private List<BoardUrl> boards = null;
	private Map<String, BoardUrl> boardMap = null;
	
	private Boards(Context ctx){
		boards = new LinkedList<BoardUrl>();
		boardMap = new HashMap<String, BoardUrl>();
		loadData(ctx);
	}


	private void loadData(Context ctx){
		Yaml yaml = new Yaml();

		try {
			//Загрузка описаний из yaml-файла
			List<Map<String, String>> data = (List<Map<String, String>>)
					yaml.load(ctx.getAssets().open("boards.yml"));

			Map<String,String> item;
			
			BoardUrl newBoard;
			for (int i = 0; i < data.size(); ++i) {
				item = data.get(i);
				newBoard = new BoardUrl(
						item.get("title"),
						item.get("prefix"),
						item.get("url"),
						ctx.getResources().getIdentifier(item.get("pic"),
								"drawable", ctx.getPackageName()));
				boards.add(newBoard);
				boardMap.put(newBoard.shortTitle.replace("/", ""), newBoard);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public static Boards getInstance(Context ctx){
		if(CirnoidApplication.boardsInstance == null){
			CirnoidApplication.boardsInstance = new Boards(ctx);
		}
		
		return CirnoidApplication.boardsInstance;
	}
	
	public BoardUrl[] getArray(){
		return boards.toArray(new BoardUrl[boards.size()]);
	}

	public List<BoardUrl> getList(){
		return boards;
	}
	
	public BoardUrl findByPrefix(String prefix){
		return boardMap.get(prefix);
	}
}
