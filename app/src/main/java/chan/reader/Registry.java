package chan.reader;

import android.database.sqlite.SQLiteDatabase;

import java.util.regex.Pattern;

public class Registry {
	public static SQLiteDatabase db;
	public final static String SITE_URL = "https://iichan.hk";
	public final static String USER_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:16.0) Gecko/20100101 Firefox/16.0";
	public final static String ACRA_KEY = "dHhUaGZ6dnVRSTltN29yazRuNzBsenc6MQ";
	public final static Pattern refRegex = Pattern.compile(">>([0-9]+)");
    public final static Pattern omittedRegex = Pattern.compile("Пропущено\\s+([0-9]+)\\s+сообщений");
    public final static Pattern omittedWithImagesRegex = Pattern.compile("Пропущено\\s+([0-9]+)\\s+сообщений\\s+и\\s+([0-9]+)\\s+изображений.");
	
	public final static Object dbMonitor = new Object();
	
	//DEBUG OPTIONS
	public final static boolean CACHE_ENABLED = true;
}
