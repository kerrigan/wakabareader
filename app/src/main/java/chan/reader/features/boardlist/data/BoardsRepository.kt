package chan.reader.features.boardlist.data

import chan.reader.objects.BoardUrl

interface BoardsRepository {
    suspend fun getBoards(): List<BoardUrl>
}
