package chan.reader.features.boardlist

import android.os.Bundle
import androidx.activity.compose.setContent
import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.IntrinsicSize
import androidx.compose.foundation.layout.aspectRatio
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.widthIn
import androidx.compose.foundation.lazy.grid.GridCells
import androidx.compose.foundation.lazy.grid.LazyVerticalGrid
import androidx.compose.foundation.lazy.grid.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.Settings
import androidx.compose.material.icons.filled.Star
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.platform.LocalDensity
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import chan.reader.R
import chan.reader.common.KoinComponentActivity
import chan.reader.features.boardlist.ui.theme.Maroon
import chan.reader.features.boardlist.ui.theme.Teal
import chan.reader.features.boardlist.ui.theme.WakabaTheme
import chan.reader.objects.BoardUrl
import org.koin.androidx.viewmodel.ext.android.viewModel

@OptIn(ExperimentalMaterial3Api::class)
class BoardListActivity : KoinComponentActivity() {

    private val viewModel: BoardListViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContent {
            WakabaTheme {
                Scaffold(
                    topBar = {
                        TopAppBar(
                            colors = TopAppBarDefaults.centerAlignedTopAppBarColors(),
                            title = {
                                Text("Ычан")
                            },
                            actions = {
                                IconButton(onClick = { viewModel.onFavoriteClicked() }) {
                                    Icon(
                                        imageVector = Icons.Filled.Star,
                                        contentDescription = "Localized description"
                                    )
                                }
                                IconButton(onClick = { viewModel.onSettingsClicked() }) {
                                    Icon(
                                        imageVector = Icons.Filled.Settings,
                                        contentDescription = "Localized description"
                                    )
                                }
                            },
                            navigationIcon = {
                                IconButton(onClick = { finish() }) {
                                    Icon(
                                        imageVector = Icons.Filled.ArrowBack,
                                        contentDescription = "Localized description"
                                    )
                                }
                            }
                        )
                    }
                ) { padding ->
                    Surface(
                        modifier = Modifier
                            .padding(padding)
                            .fillMaxSize(), color = Teal
                    ) {
                        BoardList(viewModel = viewModel)
                    }
                }
            }
        }
    }
}

@Composable
fun BoardList(viewModel: BoardListViewModel) {
    val boardListUiState by viewModel.uiState.collectAsState()
    LazyVerticalGrid(
        columns = GridCells.Fixed(4),
        content = {
            items(boardListUiState.boards) { board ->
                BoardItemView(model = board) {
                    viewModel.onBoardClicked(board)
                }
            }
        })
}

@Composable
fun BoardItemView(model: BoardUrl, onClick: () -> Unit) {
    Column(
        modifier = Modifier
            .padding(start = 19.dp, end = 19.dp, top = 8.dp)
            .fillMaxSize()
            .clickable {
                onClick.invoke()
            }, horizontalAlignment = Alignment.CenterHorizontally
    ) {

        val painter = painterResource(id = model.favicon)
        val imageRatio = painter.intrinsicSize.width / painter.intrinsicSize.height
        Image(
            modifier = Modifier
                .fillMaxWidth()
                .aspectRatio(imageRatio)
                .widthIn(0.dp, 200.dp)
                .height(IntrinsicSize.Min),
            painter = painter,
            contentDescription = model.title,
            contentScale = ContentScale.Fit,
            alignment = Alignment.CenterStart,
        )

        val textStyle = TextStyle(
            color = Maroon,
            fontSize = 16.sp.nonScaledSp(),
        )
        Text(text = model.shortTitle, style = textStyle)
    }
}

@Composable
fun TextUnit.nonScaledSp(): TextUnit {
    return (this.value / LocalDensity.current.fontScale).sp
}

@Preview(showBackground = true)
@Composable
fun BoardItemViewPreview() {
    val model = BoardUrl("au", "/au/", "au", R.drawable.au)
    BoardItemView(model = model) {}
}

//class BoardListViewModelFactory(private val context: Context) : ViewModelProvider.NewInstanceFactory() {
//    override fun <T : ViewModel> create(modelClass: Class<T>): T {
//        val navigator = Navigator(context = context)
//        return BoardListViewModel(context, navigator) as T
//    }
//}