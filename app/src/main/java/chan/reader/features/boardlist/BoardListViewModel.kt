package chan.reader.features.boardlist

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import chan.reader.features.boardlist.data.BoardsRepository
import chan.reader.objects.BoardUrl
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class BoardListViewModel(
    private val boardsRepository: BoardsRepository,
    private val navigator: Navigator,
    ) : ViewModel() {
    private val _uiState = MutableStateFlow(BoardListUiState())

    val uiState: StateFlow<BoardListUiState> = _uiState.asStateFlow()

    init {
        viewModelScope.launch {
            val boards = boardsRepository.getBoards()
            _uiState.value = BoardListUiState(boards = boards)
        }
    }

    fun onBoardClicked(model: BoardUrl) {
        navigator.openBoard(model = model)
    }

    fun onSettingsClicked() {
        navigator.openSettings()
    }

    fun onFavoriteClicked() {
        navigator.openFavorites()
    }
}

data class BoardListUiState(
    val boards: List<BoardUrl> = listOf()
)
