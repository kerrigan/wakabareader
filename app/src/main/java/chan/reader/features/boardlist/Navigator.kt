package chan.reader.features.boardlist

import android.content.Context
import android.content.Intent
import chan.reader.activities.BoardActivityDeprecated
import chan.reader.activities.PreferencesActivity
import chan.reader.activities.SubscriptionsActivity
import chan.reader.objects.BoardUrl

class Navigator(private val context: Context) {
    fun openBoard(model: BoardUrl) {
        val intent = Intent()
        intent.putExtra(BoardActivityDeprecated.BOARD_URL_ID, model.url)
        intent.putExtra(BoardActivityDeprecated.BOARD_TITLE_ID, model.title + " - " + model.shortTitle)
        intent.putExtra(BoardActivityDeprecated.BOARD_PREFIX_ID, model.shortTitle.replace("/", ""))
        intent.setClass(context, BoardActivityDeprecated::class.java)
        context.startActivity(intent)
    }

    fun openSettings() {
        val intent = Intent(context, PreferencesActivity::class.java)
        context.startActivity(intent)
    }

    fun openFavorites() {
        val intent = Intent(context, SubscriptionsActivity::class.java)
        context.startActivity(intent)
    }
}