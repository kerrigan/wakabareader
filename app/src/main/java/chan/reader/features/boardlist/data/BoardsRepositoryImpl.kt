package chan.reader.features.boardlist.data

import android.content.Context
import chan.reader.Boards
import chan.reader.objects.BoardUrl
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class BoardsRepositoryImpl(private val context: Context) : BoardsRepository {
    override suspend fun getBoards(): List<BoardUrl> {
        return withContext(Dispatchers.IO) {
            Boards.getInstance(context).list
        }
    }
}
