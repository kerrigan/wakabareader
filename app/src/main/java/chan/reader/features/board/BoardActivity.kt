package chan.reader.features.board

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material.icons.filled.ArrowForward
import androidx.compose.material.icons.filled.Refresh
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.ui.Modifier
import chan.reader.features.board.ui.theme.WakabareaderTheme
import org.koin.android.ext.android.inject

@OptIn(ExperimentalMaterial3Api::class)
class BoardActivity : ComponentActivity() {

    private val viewModel: BoardViewModel by inject()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            WakabareaderTheme {
                Scaffold(topBar = {
                    TopAppBar(
                        colors = TopAppBarDefaults.centerAlignedTopAppBarColors(),
                        title = {
                            Text("Ычан")
                        },
                        actions = {
                            IconButton(onClick = {  }) {
                                Icon(
                                    imageVector = Icons.Filled.ArrowForward,
                                    contentDescription = "Localized description"
                                )
                            }
                            IconButton(onClick = {  }) {
                                Icon(
                                    imageVector = Icons.Filled.Refresh,
                                    contentDescription = "Localized description"
                                )
                            }
                        },
                        navigationIcon = {
                            IconButton(onClick = { finish() }) {
                                Icon(
                                    imageVector = Icons.Filled.ArrowBack,
                                    contentDescription = "Localized description"
                                )
                            }
                        }
                    )
                }) {
                    Surface(modifier = Modifier.padding(it), color = MaterialTheme.colorScheme.background) {
                        LazyColumn(content = {

                        })
                    }
                }
                // A surface container using the 'background' color from the theme

            }
        }
    }
}