package chan.reader;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by kerrigan on 07.09.14.
 */
public class CirnoidSettings {
    public static final String SAVED_IMAGES_PATH_OPTION = "saved_images_path";
    public static final String NICKNAME_OPTION = "nickname";
    public static final String UPDATE_THREADS = "update_threads";
    public static final String PASSWORD_OPTION = "password";
    public static final String THEME_OPTION = "theme";
    public static final String THREAD_CACHE_SIZE_OPTION = "max_threads";
    public static final String IMAGES_CACHE_SIZE_OPTION = "max_images";
    public static final String DOWNLOAD_IMAGES_OPTION = "download_images";
    public static final String ENABLE_VIBRATION = "enable_vibration";
    public static final String SHOW_FORM_FIELDS_OPTION = "show_form_fields";


    public static final String LAST_SAVED_MESSAGE = "last_saved_message";
    public static final String SAVE_LAST_MESSAGE_OPTION = "save_last_message";
    public static final String SUBSCRIBE_TO_POSTED_OPTION = "subscribe_to_posted";

    public static final String ANTI_DDOS_COOKIE = "cookie";

    private final SharedPreferences mSettings;


    public CirnoidSettings(Context ctx){
        mSettings = ctx.getSharedPreferences("options", Context.MODE_PRIVATE);
    }


    public String getSavedImagesPath(){
        return mSettings.getString(SAVED_IMAGES_PATH_OPTION, "");
    }


    public void setSavedImagesPath(String path){
        mSettings.edit()
                .putString(SAVED_IMAGES_PATH_OPTION, path)
                .commit();
    }


    public String getNickname(){
        return mSettings.getString(NICKNAME_OPTION, "");
    }


    public void setNickName(String nickname){
        mSettings.edit()
                .putString(NICKNAME_OPTION, nickname)
                .commit();
    }


    public boolean getUpdateThreadsEnabled(){
        return mSettings.getBoolean(UPDATE_THREADS, false);
    }


    public void setUpdateThreadsEnabled(boolean enabled){
        mSettings.edit()
                .putBoolean(UPDATE_THREADS, enabled)
                .commit();
    }


    public String getPassword(){
        String password = mSettings.getString(PASSWORD_OPTION, "");
        if("".equals(password)){
            password = Utils.generatePassword();
            mSettings.edit()
                    .putString(PASSWORD_OPTION, password)
                    .commit();
        }

        return password;
    }

    public void setPassword(String password){
        mSettings.edit()
                .putString(PASSWORD_OPTION, password)
                .commit();
    }


    public Integer getTheme(){
        return mSettings.getInt(THEME_OPTION, 0);
    }

    public void setTheme(Integer themeId){
        mSettings.edit()
                .putInt(THEME_OPTION, themeId)
                .commit();
    }


    public Integer getThreadCacheSize(){
        return mSettings.getInt(THREAD_CACHE_SIZE_OPTION, 50);
    }

    public void setThreadCacheSize(Integer size){
        mSettings.edit()
                .putInt(THREAD_CACHE_SIZE_OPTION, size)
                .commit();
    }


    public Integer getImagesCacheSize(){
        return mSettings.getInt(IMAGES_CACHE_SIZE_OPTION, 20);
    }

    public void setImagesCacheSize(Integer size){
        mSettings.edit()
                .putInt(IMAGES_CACHE_SIZE_OPTION, size)
                .commit();
    }


    public boolean getImagesEnabled(){
        return mSettings.getBoolean(DOWNLOAD_IMAGES_OPTION, true);
    }

    public void setImagesEnabled(boolean enabled){
        mSettings.edit()
                .putBoolean(DOWNLOAD_IMAGES_OPTION, enabled)
                .commit();
    }


    public boolean getVibrationEnabled(){
        return mSettings.getBoolean(ENABLE_VIBRATION, false);
    }

    public void setVibrationEnabled(boolean enabled){
        mSettings.edit()
                .putBoolean(ENABLE_VIBRATION, enabled)
                .commit();
    }


    public boolean getShowFormFields(){
        return mSettings.getBoolean(SHOW_FORM_FIELDS_OPTION, true);
    }

    public void setShowFormFields(boolean show){
        mSettings.edit()
                .putBoolean(SHOW_FORM_FIELDS_OPTION, show)
                .commit();
    }


    public String getLastSavedMessage(){
        return mSettings.getString(LAST_SAVED_MESSAGE, "");
    }


    public void setLastSavedMessage(String message){
        mSettings.edit()
                .putString(LAST_SAVED_MESSAGE, message)
                .commit();
    }



    public boolean getSaveLastMessage(){
        return mSettings.getBoolean(SAVE_LAST_MESSAGE_OPTION, false);
    }


    public void setSaveLastMessage(boolean enabled){
        mSettings.edit()
                .putBoolean(SAVE_LAST_MESSAGE_OPTION, enabled)
                .commit();
    }


    public boolean getSubscribeToPosted(){
        return mSettings.getBoolean(SUBSCRIBE_TO_POSTED_OPTION, false);
    }

    public void setSubscribeToPosted(boolean enabled){
        mSettings.edit()
                .putBoolean(SUBSCRIBE_TO_POSTED_OPTION, enabled)
                .commit();
    }

    public String getAntiDdosCookie() {
        return mSettings.getString(ANTI_DDOS_COOKIE,  null);
    }

    public void setAntiDdosCookie(String antiDdosCookie) {
        mSettings.edit()
                .putString(ANTI_DDOS_COOKIE, antiDdosCookie)
                .commit();
    }
}
