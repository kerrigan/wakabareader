package chan.reader.objects;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.util.Pair;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import chan.reader.KUtils;
import chan.reader.parsers.PostParser;
import chan.reader.spannables.PostReferenceSpan;


public class Post {
    public Integer id = 0;
    public String text = "";
    //public String author;
    //public String date;
    public String image = "";
    public String thumb = "";
    public String board = "";
    public String name = "";
    public String title = "";

    public SpannableStringBuilder buildedText;
    public boolean selected = false;
    public String datestr = "";
    public Date timestamp = new Date();

    public List<Integer> outReplies = null;

    public boolean exists = true;
    public boolean omittedFound = false;
    public int omittedCount = 0;
    public int omittedImagesCount = 0;

    public List<Post> lastReplies = null;

    public Post() {
        outReplies = new LinkedList<Integer>();
    }

    @Override
    public String toString() {
        return "Post: " + id + " " + ((text.length() > 15) ? text.substring(0, 15) : text);
    }

    public static void buildOutLinks(List<Post> comments, Context context) {

        //Первый проход - сборка разметки, включая прямые ссылки

        Post post;
        for (int i = 0; i < comments.size(); ++i) {
            post = comments.get(i);
            if (post.buildedText == null) {
                PostParser postParser = new PostParser(comments, KUtils.initUntrustImageLoader(context), post.id);
                post.buildedText = postParser.parsePost(post.text);
            }
        }

        //Второй проход - сборка обратных ссылок

        Integer outId, leftSpanIndex, rightSpanIndex, outLinksAdded;
        String outLink;
        for (int i = 0; i < comments.size(); ++i) {
            post = comments.get(i);
            if (post.outReplies.isEmpty()) {
                post.outReplies = null;
            } else {
                outLinksAdded = 0;
                leftSpanIndex = post.buildedText.length() + 2;
                for (int j = 0; j < post.outReplies.size(); ++j) {
                    outId = post.outReplies.get(j);


                    if (outId <= post.id) {
                        continue;
                    }
                    outLinksAdded++;
                    if (outLinksAdded == 1) {
                        post.buildedText.append("\n\n");
                    }

                    outLink = outId + ">>";
                    rightSpanIndex = leftSpanIndex + outLink.length();
                    post.buildedText.append(outLink);
                    post.buildedText.setSpan(
                            new PostReferenceSpan(comments, outId, post.id, KUtils.initUntrustImageLoader(context), false),
                            leftSpanIndex, rightSpanIndex, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
                    if (j < post.outReplies.size() - 1) {
                        post.buildedText.append(" ");
                    }
                    leftSpanIndex = post.buildedText.length();
                }
            }
        }
    }


    public static Pair<Integer, Integer> countPostsData(List<Post> posts) {
        Integer imagesCount = 0;
        for (Post post : posts) {
            if (post.image != null && !post.image.trim().equals("")) {
                imagesCount++;
            }
        }

        return new Pair<Integer, Integer>(posts.size(), imagesCount);
    }
}
