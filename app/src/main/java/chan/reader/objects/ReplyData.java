package chan.reader.objects;

import android.os.Parcel;
import android.os.Parcelable;

public class ReplyData implements Parcelable {
	public String baseUrl;
	public String prefix;
	public String boardTitle = "";
	public String name;
	public String threadId;
	public String topic;
	public String text;
	public String imageUri;
	public String captcha;
	public String password;

	public int describeContents() {
		// TODO Auto-generated method stub
		return hashCode();
	}

	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(baseUrl);
		dest.writeString(prefix);
		dest.writeString(name);
		dest.writeString(threadId);
		dest.writeString(topic);
		dest.writeString(text);
		dest.writeString(imageUri);
		dest.writeString(captcha);
		dest.writeString(password);
		// TODO Auto-generated method stub

	}

	public static final Parcelable.Creator<ReplyData> CREATOR = new Parcelable.Creator<ReplyData>() {
		// распаковываем объект из Parcel
		public ReplyData createFromParcel(Parcel in) {
			return new ReplyData(in);
		}

		public ReplyData[] newArray(int size) {
			return new ReplyData[size];
		}
	};

	// конструктор, считывающий данные из Parcel
	public ReplyData() {
		// TODO Auto-generated constructor stub
	}

	private ReplyData(Parcel parcel) {
		baseUrl = parcel.readString();
		prefix = parcel.readString();
		name = parcel.readString();
		threadId = parcel.readString();
		topic = parcel.readString();
		text = parcel.readString();
		imageUri = parcel.readString();
		captcha = parcel.readString();
		password = parcel.readString();
	}
}
