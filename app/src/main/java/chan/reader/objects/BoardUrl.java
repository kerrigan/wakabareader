package chan.reader.objects;

public class BoardUrl {
	public final String title;
	public final String url;
	public final String shortTitle;
	public final int favicon;

	public BoardUrl(String title, String shortTitle, String url, int favicon) {
		this.favicon = favicon;
		this.title = title;
		this.shortTitle = shortTitle;
		this.url = url;
	}
}
