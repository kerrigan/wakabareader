package chan.reader.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils.InsertHelper;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import chan.reader.Registry;
import chan.reader.objects.Post;
import chan.reader.objects.PostDefinition;

public class DbHandler {
    private static DbOpenHelper dbHelper;

    public DbHandler(Context context) {
        if (dbHelper == null) {
            String dbDir = context.getFilesDir().getAbsolutePath() + "/threads.db";
            dbHelper = new DbOpenHelper(context, dbDir);
        }
    }

    public boolean pageChanged(String boardPrefix, Integer page, String newHash) {
        // проверка изменения треда

        if (newHash.equals("")) {
            return true;
        }

        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery("SELECT hash FROM boardhashes WHERE board = ? AND page = ?", new String[]{boardPrefix, page.toString()});
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            String hash = cursor.getString(cursor.getColumnIndex("hash"));
            cursor.close();
            if (newHash.equals(hash)) {
                return false;
            }
        }
        cursor.close();
        // db.close();
        return true;
    }

    public boolean threadChanged(String boardPrefix, Integer threadId, String newHash) {
        if (newHash.equals("")) {
            return true;
        }

        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery("SELECT hash FROM threadhashes WHERE board = ? AND thread_id = ?", new String[]{boardPrefix, threadId.toString()});
        if (cursor.getCount() > 0) {
            cursor.moveToFirst();
            String hash = cursor.getString(cursor.getColumnIndex("hash"));
            if (newHash.equals(hash)) {
                cursor.close();
                return false;
            }
        }

        cursor.close();
        // db.close();
        return true;
    }

    public List<Post> getThreads(String board_prefix, Integer page) {
        // получение тредов
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery(SQLQueries.SQL_SELECT_PAGE, new String[]{board_prefix, page.toString()});

        final int post_id = cursor.getColumnIndex("post_id");
        final int board_id = cursor.getColumnIndex("board");
        final int title_id = cursor.getColumnIndex("title");
        final int thumb_id = cursor.getColumnIndex("thumb");
        final int image_id = cursor.getColumnIndex("image");
        final int text_id = cursor.getColumnIndex("text");
        final int name_id = cursor.getColumnIndex("name");
        final int timestamp_id = cursor.getColumnIndex("timestamp");

        List<Post> posts = new LinkedList<Post>();

        Cursor repliesCursor, omittedCursor;
        while (cursor.moveToNext()) {
            Integer id = cursor.getInt(post_id);
            String board = cursor.getString(board_id);
            String title = cursor.getString(title_id);
            String thumb = cursor.getString(thumb_id);
            String image = cursor.getString(image_id);
            String text = cursor.getString(text_id);
            String name = cursor.getString(name_id);
            Long unixtime = cursor.getLong(timestamp_id);

            Post post = new Post();

            post.id = id;
            post.board = board;
            post.title = title;
            post.thumb = thumb;
            post.image = image;
            post.text = text;
            post.name = name;
            post.timestamp = new Date(unixtime * 1000);
            post.lastReplies = null;

            omittedCursor = db.rawQuery("SELECT omitted, omitted_images FROM omitted WHERE board = ? AND post_id = ?", new String[]{post.board, post.id.toString()});

            final int omitted_id = omittedCursor.getColumnIndex("omitted");
            final int omitted_images = omittedCursor.getColumnIndex("omitted_images");
            if (omittedCursor.getCount() > 0) {
                omittedCursor.moveToFirst();
                post.omittedFound = true;
                post.omittedCount = omittedCursor.getInt(omitted_id);
                post.omittedImagesCount = omittedCursor.getInt(omitted_images);
            }

            repliesCursor = db.rawQuery("SELECT p.post_id, p.board, p.title, p.name, p.text, p.thumb, p.image, p.timestamp FROM (SELECT p.post_id, p.board, p.title, p.name, p.text, p.thumb, p.image, p.timestamp " + "FROM posts p WHERE parent = ? ORDER BY p.post_id DESC LIMIT 10) AS p ORDER BY p.post_id ASC", new String[]{id.toString()});
            int lastRepliesCount = repliesCursor.getCount();
            if (lastRepliesCount > 0) {
                post.lastReplies = new ArrayList<>(lastRepliesCount);
                Post reply;
                while (repliesCursor.moveToNext()) {
                    Integer reply_id = repliesCursor.getInt(post_id);
                    String reply_board = repliesCursor.getString(board_id);
                    String reply_title = repliesCursor.getString(title_id);
                    String reply_thumb = repliesCursor.getString(thumb_id);
                    String reply_image = repliesCursor.getString(image_id);
                    String reply_text = repliesCursor.getString(text_id);
                    String reply_name = repliesCursor.getString(name_id);
                    Long reply_unixtime = repliesCursor.getLong(timestamp_id);

                    reply = new Post();
                    reply.id = reply_id;
                    reply.board = reply_board;
                    reply.title = reply_title;
                    reply.thumb = reply_thumb;
                    reply.image = reply_image;
                    reply.text = reply_text;
                    reply.name = reply_name;
                    reply.timestamp = new Date(reply_unixtime * 1000);
                    post.lastReplies.add(reply);
                    post.omittedFound = true;
                }
            }

            posts.add(post);
        }
        cursor.close();
        return posts;
    }

    public List<Post> getThread(String board_prefix, Integer thread_id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery(SQLQueries.SQL_SELECT_THREAD, new String[]{thread_id.toString(), thread_id.toString(), board_prefix});

        final int post_id = cursor.getColumnIndex("post_id");
        final int board_id = cursor.getColumnIndex("board");
        final int title_id = cursor.getColumnIndex("title");
        final int text_id = cursor.getColumnIndex("text");
        final int name_id = cursor.getColumnIndex("name");
        final int thumb_id = cursor.getColumnIndex("thumb");
        final int image_id = cursor.getColumnIndex("image");
        final int timestamp_id = cursor.getColumnIndex("timestamp");
        // final int parent_id = cursor.getColumnIndex("parent");

        List<Post> posts = new LinkedList<Post>();

        while (cursor.moveToNext()) {
            Integer id = cursor.getInt(post_id);
            String board = cursor.getString(board_id);
            String title = cursor.getString(title_id);
            String thumb = cursor.getString(thumb_id);
            String image = cursor.getString(image_id);
            String name = cursor.getString(name_id);
            String text = cursor.getString(text_id);
            Long unixtime = cursor.getLong(timestamp_id);
            // Integer parent = cursor.getInt(parent_id);

            Post post = new Post();
            post.id = id;
            post.board = board;
            post.title = title;
            post.name = name;
            post.text = text;
            post.board = board;
            post.thumb = thumb;
            post.image = image;
            post.timestamp = new Date(unixtime * 1000);
            // post.parent = parent;

            posts.add(post);
        }
        cursor.close();
        // db.close();
        return posts;
    }

    /*
     * private String getHash(List<Post> posts) { StringBuffer s = new
     * StringBuffer(); for (Post post : posts) { s.append(post.id);
     * s.append(","); } return s.toString(); }
     */

    public boolean upsertBoardPage(String boardPrefix, Integer page, List<Post> posts, String hash) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        synchronized (db) {
            if (Registry.db == null) Registry.db = db;

            db.beginTransaction();


            InsertHelper helper = new InsertHelper(db, "boardhashes");

            final int bh_board_id = helper.getColumnIndex("board");
            final int bh_id = helper.getColumnIndex("id");
            final int bh_page_id = helper.getColumnIndex("page");
            final int bh_hash_id = helper.getColumnIndex("hash");
            final int bh_length_id = helper.getColumnIndex("length");

            try {
                helper.prepareForReplace();
                helper.bind(bh_id, page + boardPrefix);
                helper.bind(bh_board_id, boardPrefix);
                helper.bind(bh_page_id, page);
                helper.bind(bh_length_id, 0);
                helper.bind(bh_hash_id, hash);
                helper.execute();
            } finally {
                helper.close();
            }

            helper = new InsertHelper(db, "boards");
            final int b_page_id = helper.getColumnIndex("page");
            final int b_board_id = helper.getColumnIndex("board");
            final int b_order_id = helper.getColumnIndex("order");
            final int b_threadid_id = helper.getColumnIndex("thread_id");
            final int b_id = helper.getColumnIndex("id");

            try {
                for (int i = 0; i < posts.size(); i++) {
                    Post post = posts.get(i);
                    helper.prepareForReplace();

                    helper.bind(b_id, page + "/" + boardPrefix + "/" + i);
                    helper.bind(b_page_id, page);
                    helper.bind(b_board_id, post.board);
                    helper.bind(b_threadid_id, post.id);
                    helper.bind(b_order_id, i);

                    helper.execute();
                    // Log.d("UPDATE BOARD", "Thread " + post.id + " inserted");
                }
            } finally {
                helper.close();

                db.setTransactionSuccessful();
                db.endTransaction();
                // db.close();
            }
        }

        upsertPosts(boardPrefix, posts, false);
        return true;
    }

    public synchronized boolean upsertThread(String board, List<Post> posts, String hash) {
        if (posts.size() > 0) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            synchronized (db) {
                if (Registry.db == null) Registry.db = db;

                db.beginTransaction();
                InsertHelper helper = new InsertHelper(db, "threadhashes");

                final int th_board_id = helper.getColumnIndex("board");
                final int th_id = helper.getColumnIndex("id");
                final int th_threadid = helper.getColumnIndex("thread_id");
                final int th_length_id = helper.getColumnIndex("length");
                final int th_hash_id = helper.getColumnIndex("hash");

                Post opPost = posts.get(0);
                try {
                    helper.prepareForReplace();
                    helper.bind(th_id, opPost.id + board);
                    helper.bind(th_threadid, opPost.id);
                    helper.bind(th_board_id, board);
                    helper.bind(th_length_id, 0);
                    helper.bind(th_hash_id, hash);
                    helper.execute();
                } finally {
                    helper.close();
                    db.setTransactionSuccessful();
                    db.endTransaction();
                    // db.close();
                }

            }
            upsertPosts(board, posts, true);
            return true;
        }

        return false;
    }

    public boolean upsertPosts(String board, List<Post> posts, boolean isThread) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        synchronized (db) {
            if (Registry.db == null) Registry.db = db;

            InsertHelper helper = new InsertHelper(db, "posts");
            final int id_id = helper.getColumnIndex("id");
            final int post_id = helper.getColumnIndex("post_id");
            final int board_id = helper.getColumnIndex("board");
            final int title_id = helper.getColumnIndex("title");
            final int name_id = helper.getColumnIndex("name");
            final int text_id = helper.getColumnIndex("text");
            final int thumb_id = helper.getColumnIndex("thumb");
            final int image_id = helper.getColumnIndex("image");
            final int parent_id = helper.getColumnIndex("parent");
            final int timestamp_id = helper.getColumnIndex("timestamp");

            try {
                db.beginTransaction();

                Post post;
                InsertHelper omittedHelper = new InsertHelper(db, "omitted");
                final int omitted_id = omittedHelper.getColumnIndex("id");
                final int omitted_board_id = omittedHelper.getColumnIndex("board");
                final int omitted_post_id = omittedHelper.getColumnIndex("post_id");
                final int omitted_omitted_id = omittedHelper.getColumnIndex("omitted");
                final int omitted_omitted_images_id = omittedHelper.getColumnIndex("omitted_images");

                for (int i = 0; i < posts.size(); ++i) {
                    post = posts.get(i);
                    int parent;
                    // Log.e("INSERT POST", post.id + " " + post.board);
                    if (!isThread) {
                        if (post.lastReplies != null) {
                            Post reply;
                            for (int j = 0; j < post.lastReplies.size(); ++j) {
                                reply = post.lastReplies.get(j);
                                helper.prepareForReplace();
                                helper.bind(id_id, reply.id + reply.board);
                                helper.bind(post_id, reply.id);
                                helper.bind(board_id, reply.board);
                                helper.bind(name_id, reply.name);
                                helper.bind(title_id, reply.title);
                                helper.bind(thumb_id, reply.thumb);
                                helper.bind(image_id, reply.image);
                                helper.bind(text_id, reply.text);
                                helper.bind(parent_id, post.id);
                                helper.bind(timestamp_id, reply.timestamp.getTime() / 1000L);
                                helper.execute();
                            }

                            if (post.omittedFound) {
                                omittedHelper.prepareForReplace();
                                omittedHelper.bind(omitted_id, post.id + post.board);
                                omittedHelper.bind(omitted_board_id, post.board);
                                omittedHelper.bind(omitted_post_id, post.id);
                                omittedHelper.bind(omitted_omitted_id, post.omittedCount);
                                omittedHelper.bind(omitted_omitted_images_id, post.omittedImagesCount);
                                omittedHelper.execute();
                            }
                        }

                        if (isExists(db, post.board, post.id)) {
                            continue;
                        }
                    }
                    if (i == 0 || !isThread) {
                        parent = 0;
                    } else {
                        parent = posts.get(0).id;
                    }

                    helper.prepareForReplace();

                    helper.bind(id_id, post.id + post.board);
                    helper.bind(post_id, post.id);
                    helper.bind(board_id, post.board);
                    helper.bind(name_id, post.name);
                    helper.bind(title_id, post.title);
                    helper.bind(thumb_id, post.thumb);
                    helper.bind(image_id, post.image);
                    helper.bind(text_id, post.text);
                    helper.bind(parent_id, parent);
                    helper.bind(timestamp_id, post.timestamp.getTime() / 1000L);

                    helper.execute();
                    // Log.d("UPDATE POSTS", "Post " + post.id + " inserted");
                }
            } finally {
                helper.close();
                db.setTransactionSuccessful();
                db.endTransaction();
                // db.close();
            }

        }
        return false;
    }

    public List<PostDefinition> getSubscriptions() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery("SELECT thread_id, board, \"exists\" FROM subscriptions", new String[]{});
        List<PostDefinition> result = new LinkedList<PostDefinition>();

        final int threadid_id = cursor.getColumnIndex("thread_id");
        final int board_id = cursor.getColumnIndex("board");
        final int exists_id = cursor.getColumnIndex("exists");
        while (cursor.moveToNext()) {
            PostDefinition sub = new PostDefinition();
            sub.board = cursor.getString(board_id);
            sub.threadId = cursor.getInt(threadid_id);
            sub.exists = (cursor.getInt(exists_id) == 1);
            result.add(sub);
        }
        cursor.close();
        // db.close();
        return result;
    }

    public List<Post> getSubscribed() {
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.rawQuery("SELECT p.id, p.post_id, p.board, p.title, p.name, p.text, s.\"exists\"" + " FROM posts p JOIN subscriptions s ON s.id = p.id" + " WHERE parent = 0 ORDER BY p.board, p.post_id", new String[]{});

        final int post_id = cursor.getColumnIndex("post_id");
        final int board_id = cursor.getColumnIndex("board");
        final int title_id = cursor.getColumnIndex("title");
        // final int thumb_id = cursor.getColumnIndex("thumb");
        // final int image_id = cursor.getColumnIndex("image");
        final int text_id = cursor.getColumnIndex("text");
        final int name_id = cursor.getColumnIndex("name");
        final int exists_id = cursor.getColumnIndex("exists");

        List<Post> posts = new LinkedList<Post>();
        while (cursor.moveToNext()) {
            Integer id = cursor.getInt(post_id);
            String board = cursor.getString(board_id);
            String title = cursor.getString(title_id);
            // String thumb = cursor.getString(thumb_id);
            // String image = cursor.getString(image_id);
            String text = cursor.getString(text_id);
            String name = cursor.getString(name_id);

            Post post = new Post();

            post.id = id;
            post.board = board;
            post.title = title;
            // post.thumb = thumb;
            // post.image = image;
            post.text = text;
            post.name = name;
            post.exists = (cursor.getInt(exists_id) == 1);

            posts.add(post);
        }
        cursor.close();
        return posts;
    }

    public synchronized boolean subscribeThread(String board, Integer threadId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        InsertHelper helper = new InsertHelper(db, "subscriptions");

        final int id = helper.getColumnIndex("id");
        final int board_id = helper.getColumnIndex("board");
        final int threadid_id = helper.getColumnIndex("thread_id");
        try {
            db.beginTransaction();
            helper.prepareForReplace();

            helper.bind(id, threadId + board);
            helper.bind(threadid_id, threadId);
            helper.bind(board_id, board);
            helper.execute();
        } finally {
            helper.close();
            db.setTransactionSuccessful();
            db.endTransaction();
            // db.close();
        }
        return true;
    }

    public synchronized boolean unsubscribeThread(String board, Integer threadId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        db.beginTransaction();
        db.delete("subscriptions", "board = ? AND thread_id = ?", new String[]{board, threadId.toString()});
        db.setTransactionSuccessful();
        db.endTransaction();
        // db.close();
        return true;
    }

    public synchronized boolean isSubscribed(String board, Integer threadId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery("SELECT thread_id FROM subscriptions WHERE id = ?", new String[]{threadId + board});
        boolean result;
        result = cursor.getCount() > 0;
        cursor.close();
        // db.close();
        return result;
    }

    public List<PostDefinition> getBanned(String board) {
        List<PostDefinition> banned = new LinkedList<PostDefinition>();
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(SQLQueries.SELECT_BANNED, new String[]{board});
        final int threadid_id = cursor.getColumnIndex("thread_id");
        while (cursor.moveToNext()) {
            Integer thread_id = cursor.getInt(threadid_id);
            PostDefinition pd = new PostDefinition();
            pd.threadId = thread_id;
            pd.board = board;
            banned.add(pd);
        }
        cursor.close();
        return banned;
    }

    public boolean banThread(String board, Integer threadId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        synchronized (db) {

            if (Registry.db == null) Registry.db = db;

            InsertHelper helper = new InsertHelper(db, "banned");

            final int id = helper.getColumnIndex("id");
            final int board_id = helper.getColumnIndex("board");
            final int threadid_id = helper.getColumnIndex("thread_id");
            try {
                db.beginTransaction();
                helper.prepareForReplace();

                helper.bind(id, threadId + board);
                helper.bind(threadid_id, threadId);
                helper.bind(board_id, board);
                helper.execute();
            } finally {
                helper.close();
                db.setTransactionSuccessful();
                db.endTransaction();
            }

        }
        return true;
    }

    public synchronized boolean unbanThread(String board, Integer threadId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        db.beginTransaction();
        db.delete("banned", "board = ? AND thread_id = ?", new String[]{board, threadId.toString()});
        db.setTransactionSuccessful();
        db.endTransaction();
        return true;
    }

    public boolean isBanned(String board, Integer threadId) {
        boolean result;
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery("SELECT thread_id FROM banned WHERE id = ?", new String[]{threadId + board});
        result = cursor.getCount() > 0;
        cursor.close();
        return result;
    }

    public void clearCache() {
        // TODO полная очистка кеша
        /*
         * SQLiteDatabase db = dbHelper.getWritableDatabase();
         * db.beginTransaction();
         */
        /*
         * Очистить таблицы "posts" "boards" "boardhashes" "threadhashes"
         */
        /*
         * db.setTransactionSuccessful(); db.endTransaction();
         */
    }

    public List<Post> getThreadPostsWithImages(String board_prefix, Integer thread_id) {
        // TODO Auto-generated method stub
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery(SQLQueries.SQL_SELECT_IMAGES_FROM_THREAD, new String[]{thread_id.toString(), thread_id.toString(), board_prefix});

        final int post_id = cursor.getColumnIndex("post_id");
        final int board_id = cursor.getColumnIndex("board");
        final int title_id = cursor.getColumnIndex("title");
        final int text_id = cursor.getColumnIndex("text");
        final int name_id = cursor.getColumnIndex("name");
        final int thumb_id = cursor.getColumnIndex("thumb");
        final int image_id = cursor.getColumnIndex("image");
        // final int parent_id = cursor.getColumnIndex("parent");

        List<Post> posts = new LinkedList<Post>();

        while (cursor.moveToNext()) {
            Integer id = cursor.getInt(post_id);
            String board = cursor.getString(board_id);
            String title = cursor.getString(title_id);
            String thumb = cursor.getString(thumb_id);
            String image = cursor.getString(image_id);
            String name = cursor.getString(name_id);
            String text = cursor.getString(text_id);
            // Integer parent = cursor.getInt(parent_id);

            Post post = new Post();
            post.id = id;
            post.board = board;
            post.title = title;
            post.name = name;
            post.text = text;
            post.thumb = thumb;
            post.image = image;
            // post.parent = parent;

            posts.add(post);
        }
        cursor.close();
        // db.close();
        return posts;
    }

    public boolean isExists(SQLiteDatabase db, String board_prefix, Integer post_id) {

        Cursor cursor = db.rawQuery(SQLQueries.SQL_SELECT_POST, new String[]{post_id.toString(), board_prefix});
        int count = cursor.getCount();
        cursor.close();
        return count > 0;
    }

    public void markNonExistent(String board_prefix, Integer post_id) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        try {
            db.beginTransaction();
            ContentValues newValues = new ContentValues();
            newValues.put("\"exists\"", 0);
            String[] whereArgs = new String[]{board_prefix, Integer.toString(post_id)};
            db.update("subscriptions", newValues, "board = ? AND thread_id = ?", whereArgs);
        } finally {
            db.setTransactionSuccessful();
            db.endTransaction();
        }
    }


    public Post getPost(String board_prefix, Integer post_id) {
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery(SQLQueries.SQL_SELECT_POST, new String[]{post_id.toString(), board_prefix, board_prefix});

        /*
         * final int postid_id = cursor.getColumnIndex("post_id"); final int
         * board_id = cursor.getColumnIndex("board");
         */
        final int title_id = cursor.getColumnIndex("title");
        final int text_id = cursor.getColumnIndex("text");
        final int name_id = cursor.getColumnIndex("name");
        final int thumb_id = cursor.getColumnIndex("thumb");
        final int image_id = cursor.getColumnIndex("image");
        final int timestamp_id = cursor.getColumnIndex("timestamp");

        cursor.moveToFirst();

        Post post = new Post();

        post.board = board_prefix;
        post.id = post_id;
        post.name = cursor.getString(name_id);
        post.image = cursor.getString(image_id);
        post.thumb = cursor.getString(thumb_id);
        post.text = cursor.getString(text_id);
        post.title = cursor.getString(title_id);
        post.timestamp = new Date(cursor.getLong(timestamp_id) * 1000);

        cursor.close();

        return post;

    }

    public void cleanBoardCache(String boardPrefix, Integer count) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        if (Registry.db == null) Registry.db = db;

        Cursor cursor = db.rawQuery(SQLQueries.SQL_BOARD_THREADS_COUNT, new String[]{boardPrefix});
        cursor.moveToFirst();
        Integer foundThreads = cursor.getInt(0);
        cursor.close();

        if (foundThreads >= count) {
            String[] args = {boardPrefix, boardPrefix, Integer.toString(foundThreads - count + 1)};
            cursor = db.rawQuery(SQLQueries.SQL_SELECT_THREADS_FOR_DELETE, args);
            cursor.moveToFirst();
            List<Integer> threads = new ArrayList<Integer>(foundThreads - count);
            while (cursor.moveToNext()) {
                threads.add(cursor.getInt(0));
            }

            cursor.close();

            try {
                db.beginTransaction();
                for (int i = 0; i < threads.size(); i++) {
                    String threadId = threads.get(i).toString();
                    String[] delArgs = {boardPrefix, threadId, threadId};
                    db.delete("posts", "board = ? AND (post_id = ? OR parent = ?)", delArgs);
                }
            } finally {
                db.setTransactionSuccessful();
                db.endTransaction();
            }
        }
    }

}
