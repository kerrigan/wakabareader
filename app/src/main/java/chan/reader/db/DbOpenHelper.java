package chan.reader.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.Arrays;
import java.util.List;

import chan.reader.DatabaseContext;

public class DbOpenHelper extends SQLiteOpenHelper {
	private interface Patch {
		public void apply(SQLiteDatabase db);
	}

	private final List<Patch> migrations = Arrays.asList(new Patch[] { null,
			new Patch() {
				@Override
				public void apply(SQLiteDatabase db) {
					db.execSQL("DELETE FROM posts WHERE id not in("
							+ " SELECT thread_id || board FROM subscriptions)");
					db.execSQL("ALTER TABLE posts ADD COLUMN timestamp long default 0");
				}
			},
			new Patch() {

				@Override
				public void apply(SQLiteDatabase db) {
					// TODO Auto-generated method stub
					db.execSQL("ALTER TABLE subscriptions ADD COLUMN \"exists\" BOOLEAN NOT NULL DEFAULT 1");
				}
				
			},
            new Patch() {
                @Override
                public void apply(SQLiteDatabase db) {
                    db.execSQL("DELETE FROM boardhashes;");
                    db.execSQL("DELETE FROM threadhashes;");
                    db.execSQL("ALTER TABLE boardhashes ADD COLUMN \"hash\" STRING NOT NULL DEFAULT \"\"");
                    db.execSQL("ALTER TABLE threadhashes ADD COLUMN \"hash\" STRING NOT NULL DEFAULT \"\"");
                }
            },
            new Patch() {
                @Override
                public void apply(SQLiteDatabase db) {
                    db.execSQL("CREATE TABLE \"omitted\" ("
                             + "\"id\" STRING PRIMARY KEY NOT NULL,"
                             + "\"board\" TEXT NOT NULL,"
                             + "\"post_id\" INTEGER NOT NULL,"
                             + "\"omitted\" INTEGER NOT NULL,"
                             + "\"omitted_images\" INTEGER NOT NULL)");
                }
            }
	});

	public DbOpenHelper(Context context, String name) {
		super(new DatabaseContext(context), name, null, 5);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		db.execSQL("CREATE TABLE \"posts\" ("
				+ "\"id\" STRING PRIMARY KEY NOT NULL,"
				+ "\"post_id\" INTEGER NOT NULL, " + "\"board\" TEXT NOT NULL,"
				+ "\"title\" TEXT," + "\"text\" TEXT," + "\"name\" TEXT,"
				+ "\"thumb\" TEXT," + "\"image\" TEXT,"
				+ "\"parent\" INTEGER NOT NULL DEFAULT (0),"
				+ "\"timestamp\" INTEGER DEFAULT 0)");

		db.execSQL("CREATE TABLE \"boards\" ("
				+ "\"id\" STRING PRIMARY KEY NOT NULL,"
				+ "\"page\" INTEGER NOT NULL," + "\"board\" TEXT NOT NULL,"
				+ "\"order\" INTEGER NOT NULL,"
				+ "\"thread_id\" INTEGER NOT NULL )");

		db.execSQL("CREATE TABLE \"boardhashes\" ("
				+ "\"id\" STRING PRIMARY KEY NOT NULL,"
				+ "\"board\" TEXT NOT NULL,"
                + " \"page\" INTEGER NOT NULL,"
                + "\"hash\" STRING NOT NULL DEFAULT \"\","
                + "\"length\" INTEGER NOT NULL )");

		db.execSQL("CREATE TABLE \"threadhashes\" ("
				+ "\"id\" STRING PRIMARY KEY NOT NULL,"
				+ "\"thread_id\" INTEGER NOT NULL,"
				+ "\"board\" STRING NOT NULL,"
                + "\"hash\" STRING NOT NULL DEFAULT '',"
                + "\"length\" INTEGER NOT NULL)");

		db.execSQL("CREATE TABLE \"subscriptions\" ("
				+ "\"id\" STRING PRIMARY KEY NOT NULL,"
				+ "\"board\" TEXT NOT NULL,"
				+ "\"thread_id\" INTEGER NOT NULL,"
				+ "\"interval\" INTEGER NOT NULL DEFAULT (60),"
				+ "\"exists\" BOOLEAN NOT NULL DEFAULT 1)");

		db.execSQL("CREATE TABLE \"banned\" ("
				+ "\"id\" STRING PRIMARY KEY NOT NULL,"
				+ "\"board\" TEXT NOT NULL,"
				+ "\"thread_id\" INTEGER NOT NULL)");
        db.execSQL("CREATE TABLE \"omitted\" ("
                + "\"id\" STRING PRIMARY KEY NOT NULL,"
                + "\"board\" TEXT NOT NULL,"
                + "\"post_id\" INTEGER NOT NULL,"
                + "\"omitted\" INTEGER NOT NULL,"
                + "\"omitted_images\" INTEGER NOT NULL)");
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int prevVersion, int currentVersion) {
		// TODO Auto-generated method stub 
		for (int i = prevVersion; i < currentVersion; i++) {
			Object patch = migrations.get(i);
			if(patch != null){
				((Patch)patch).apply(db);
				Log.i("Migration", "Migration " + i + " completed");
			}
			
		}
		db.execSQL("DELETE FROM boards");
		db.execSQL("DELETE FROM boardhashes");
		db.execSQL("DELETE FROM threadhashes");
		//db.execSQL("DROP TABLE subscriptions");
		db.execSQL("DELETE FROM banned");

	}
}
