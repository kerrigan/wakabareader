package chan.reader.db;

public class SQLQueries {

	public static final String SELECT_BANNED = "SELECT thread_id FROM banned WHERE board = ?";
	public final static String SQL_SELECT_THREAD = "SELECT id, post_id, board, title, name, text, thumb, image, parent, timestamp"
			+ " FROM posts WHERE (parent = ? OR post_id = ?) AND board = ? ORDER BY post_id";
	public final static String SQL_SELECT_PAGE = "SELECT p.post_id, p.board, p.title, p.name, p.text, p.thumb, p.image, p.timestamp "
			+ "FROM posts p JOIN boards b ON p.post_id = b.thread_id AND p.board = b.board "
			+ "WHERE p.board = ? AND b.page = ? ORDER BY b.\"order\"";
	public static final String SQL_SELECT_POST = "SELECT id, post_id, board, title, name, text, thumb, image, parent FROM posts WHERE post_id = ? AND board = ?";
	public static final String SQL_SELECT_IMAGES_FROM_THREAD = "SELECT id, post_id, board, title, name, text, thumb, image, parent"
			+ " FROM posts WHERE (parent = ? OR post_id = ?) AND board = ? AND thumb != '' ORDER BY post_id";
	public static final String SQL_BOARD_THREADS_COUNT = "SELECT count(id) FROM posts WHERE parent = 0 AND board = ?";
	public static final String SQL_SELECT_THREADS_FOR_DELETE = "SELECT post_id,"
			+ "CASE "
			+ "WHEN (last_id is null) THEN post_id "
			+ "ELSE last_id "
			+ "END as last_id "
			+ "FROM ("
			+ " SELECT post_id, "
			+ "(SELECT max(post_id) FROM posts WHERE parent = p.post_id ) AS last_id "
			+ "FROM posts p "
			+ "WHERE parent = 0 AND board = ? ) "
			+ "WHERE post_id not in (SELECT thread_id FROM subscriptions WHERE board = ?) "
			+ " ORDER by last_id ASC LIMIT ?";

}
