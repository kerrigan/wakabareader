package chan.reader.spannables;

import android.graphics.Color;
import android.text.TextPaint;
import android.text.style.ClickableSpan;
import android.view.View;

public class SpoilerSpan extends ClickableSpan {
	private boolean isHidden = true;
	private static final int spoilerColor = Color.parseColor("#F0D0B6");

	public SpoilerSpan() {
		
	}

	@Override
	public void updateDrawState(TextPaint ds) {
		ds.bgColor = spoilerColor;
		if (isHidden) {
			ds.setColor(spoilerColor);
		} else {
			ds.setColor(Color.parseColor("#800000"));
		}
	}

	@Override
	public void onClick(View widget) {
		isHidden = !isHidden;
		widget.invalidate();
	}

}
