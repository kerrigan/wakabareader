package chan.reader.spannables;

import android.app.Dialog;
import android.content.Context;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.objects.Post;
import chan.reader.parsers.PostParser;
import coil.ImageLoader;
import coil.request.ImageRequest;

public class PostReferenceSpan extends ClickableSpan {
	private final Post mPost;
	private final List<Post> mPosts;
	private final ImageLoader mImageLoader;
	private TextPaint textpaint;
	private final boolean addPost;

	public PostReferenceSpan(
			List<Post> posts,
			Integer postId,
			Integer currentPostId,
			ImageLoader imLoader, boolean addPost) {
		mImageLoader = imLoader;
		
		this.addPost = addPost; 
		mPosts = posts;
		mPost = getPost(posts, postId);
		if (addPost && mPost != null) {
			mPost.outReplies.add(currentPostId);
		}
	}

	public PostReferenceSpan(
			List<Post> posts,
			Integer postId,
			Integer currentPostId,
			ImageLoader imLoader) {
		this(posts, postId, currentPostId, imLoader, true);
	}

	private Post getPost(List<Post> posts, Integer postId) {
		for (Post post : posts) {
			if (post.id.equals(postId)) {
				return post;
			}
		}
		return null;
	}

	@Override
	public void onClick(View widget) {

		if (mPost != null) {
			final Context mContext = widget.getContext();
			final Dialog dialog = new Dialog(mContext);
			dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
			//dialog.setTitle("Ссылка на пост");
			dialog.setContentView(R.layout.post);
			
			TextView text = (TextView) dialog.findViewById(R.id.text);
			ImageView mThumb = (ImageView) dialog.findViewById(R.id.thumb);
			TextView title = (TextView)dialog.findViewById(R.id.title);
			TextView topic = (TextView)dialog.findViewById(R.id.topic);
			TextView timestamp = (TextView)dialog.findViewById(R.id.timestamp);
			TextView number = (TextView)dialog.findViewById(R.id.number);
			

			if(mPost.buildedText == null){
				PostParser postParser = new PostParser(mPosts, mImageLoader, mPost.id);
				mPost.buildedText = postParser.parsePost(mPost.text);
			}
			
			if(mPost.buildedText.length() > 0){
				text.setVisibility(View.VISIBLE);
				text.setText(mPost.buildedText);
			} else {
				text.setVisibility(View.GONE);	
			}
			
			text.setMovementMethod(LinkMovementMethod.getInstance());


			if (mPost.thumb.equals("")) {
				mThumb.setImageResource(R.drawable.empty);
			} else {

				ImageRequest request = new ImageRequest.Builder(mContext)
						.data(Registry.SITE_URL + mPost.thumb)
						.crossfade(true)
						.target(mThumb)
						.build();
				mImageLoader.enqueue(request);

				mThumb.setOnClickListener(new View.OnClickListener() {

					public void onClick(View v) {
						// TODO Auto-generated method stub
						Utils.showImage(mPost.image, mContext);
					}
				});
			}

			

			if (!mPost.title.equals("")) {
				topic.setVisibility(View.VISIBLE);
				topic.setText(mPost.title);
			} else {
				topic.setVisibility(View.GONE);
			}
			title.setText(mPost.name);
			number.setText(mPost.id + "");
			timestamp.setText(Utils.format.format(mPost.timestamp));
			
			dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT,
					LayoutParams.WRAP_CONTENT);

			dialog.show();
		}
	}
}