package chan.reader.spannables;

import android.graphics.Typeface;
import android.text.TextPaint;
import android.text.style.TypefaceSpan;

public class CodeSpan extends TypefaceSpan {

	private final Typeface typeface;

	public CodeSpan() {
		super("sans-serif");
		typeface = Typeface.MONOSPACE;
		// TODO Auto-generated constructor stub
	}
	
	@Override
	public void updateDrawState(TextPaint ds) {
		ds.setTypeface(typeface);
		//ds.setColor(Color.GREEN);
	}

}
