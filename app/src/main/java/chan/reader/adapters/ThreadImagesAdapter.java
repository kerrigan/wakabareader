package chan.reader.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import androidx.appcompat.app.AppCompatActivity;
import chan.reader.KUtils;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.db.DbHandler;
import chan.reader.objects.Post;
import coil.ImageLoader;
import coil.request.ImageRequest;

public class ThreadImagesAdapter extends ArrayAdapter<Post> {


    private final DbHandler dbhandler;
    private final ImageLoader imageLoader;
    private List<Post> mPosts;

    public ThreadImagesAdapter(final Context context, final Integer threadId, final String prefix) {
        super(context, R.layout.post);
        final ProgressBar loading = (ProgressBar) ((AppCompatActivity) context).findViewById(R.id.images_progress);
        dbhandler = new DbHandler(context);

        imageLoader = KUtils.initUntrustImageLoader(context);

        Thread loader = new Thread(new Runnable() {

            public void run() {
                // TODO Auto-generated method stub
                mPosts = dbhandler.getThreadPostsWithImages(prefix, threadId);

                ((Activity) context).runOnUiThread(new Runnable() {

                    public void run() {
                        // TODO Auto-generated method stub
                        loading.setVisibility(View.GONE);
                        for (Post post : mPosts) {
                            add(post);
                        }
                    }
                });

            }
        });

        loader.start();

    }


    public List<Post> getItems() {
        return mPosts;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        final Post post = getItem(position);
        View v = convertView;
        if (v == null || !(v instanceof LinearLayout)) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
                    Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.image, null);

            ViewHolder holder = new ViewHolder();
            holder.title = (TextView) v.findViewById(R.id.title);
            holder.thumb = (ImageView) v.findViewById(R.id.thumb);
            v.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) v.getTag();
        TextView title = holder.title;

        title.setText(post.id.toString());

        ImageRequest request = new ImageRequest.Builder(getContext())
                .data(Registry.SITE_URL + post.thumb)
                .crossfade(true)
                .target(holder.thumb)
                .build();
        imageLoader.enqueue(request);
        if (post.selected)
            v.setBackgroundResource(R.drawable.selected_item);

        return v;

    }

    public void deselectAll() {
        for (Post post : mPosts) {
            post.selected = false;
        }
    }

    class ViewHolder {
        ImageView thumb;
        TextView title;
        CheckBox for_download;
    }

}
