package chan.reader.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import chan.reader.objects.Post;
import chan.reader.views.PostView;

public class ThreadAdapter extends BaseAdapter {
    private final Context mContext;
    private List<Post> mPosts;
    private boolean loadImages = true;


    private static class ViewHolder {
        ImageView thumb;
        TextView title;
        TextView text;
        TextView timestamp;
        TextView topic;
        TextView number;
    }

    public ThreadAdapter(Context context) {
        mContext = context;
        mPosts = new ArrayList<Post>();
    }

    public void setPosts(List<Post> posts) {
        clear();
        mPosts = posts;
        notifyDataSetChanged();
    }

    public View getView(int position, View convertView, final ViewGroup parent) {
        final Post post = getItem(position);
        PostView v = (PostView) convertView;
        if (v == null || !(v instanceof PostView)) {
            v = new PostView(mContext, null);
        }
        v.bindPost(post);
        return v;
    }


    public int getCount() {
        return mPosts.size();
    }

    public Post getItem(int position) {
        return mPosts.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    private void clear() {
        mPosts.clear();
        notifyDataSetChanged();
    }

    private void add(Post post) {
        mPosts.add(post);
    }

}
