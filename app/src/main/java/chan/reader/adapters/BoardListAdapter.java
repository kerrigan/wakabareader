package chan.reader.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import chan.reader.R;
import chan.reader.activities.BoardActivityDeprecated;
import chan.reader.objects.BoardUrl;

public class BoardListAdapter extends ArrayAdapter<BoardUrl> {

	private final Context context;

	public BoardListAdapter(Context context, BoardUrl[] boards) {
		super(context, R.layout.post);
		this.context = context;
		for (BoardUrl board : boards) {
			add(board);
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		final BoardUrl board = getItem(position);
		View v = convertView;
		if (v == null || !(v instanceof LinearLayout)) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.boardurl, null);
			
			ViewHolder holder = new ViewHolder();
			holder.boardIcon = (ImageView)v.findViewById(R.id.board_icon);
			holder.boardDescription = (TextView)v.findViewById(R.id.board_description);
			
			v.setTag(holder);
		}
		
		ViewHolder holder = (ViewHolder)v.getTag();
		
		holder.boardIcon.setImageResource(board.favicon);
		holder.boardDescription.setText(board.shortTitle);
		v.setOnClickListener(new OnClickListener() {
			
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent intent = new Intent();
				intent.putExtra(BoardActivityDeprecated.BOARD_URL_ID, board.url);
				intent.putExtra(BoardActivityDeprecated.BOARD_TITLE_ID, board.title + " - " + board.shortTitle);
				intent.putExtra(BoardActivityDeprecated.BOARD_PREFIX_ID, board.shortTitle.replace("/", ""));
				intent.setClass(context, BoardActivityDeprecated.class);
				context.startActivity(intent);
			}
		});
		return v;
	}
	
	static class ViewHolder {
		ImageView boardIcon;
		TextView boardDescription;
	}

}
