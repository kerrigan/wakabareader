package chan.reader.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import chan.reader.CirnoidSettings;
import chan.reader.db.DbHandler;
import chan.reader.objects.Post;
import chan.reader.views.BannedPostView;
import chan.reader.views.OpPostView;


public class BoardAdapter extends BaseAdapter {
	private static final int POST = 0;
	private static final int BANNED_POST = 1;

    PopupWindow imageForm;
	private final Context mContext;
	private boolean loadImages = true;
	private final DbHandler dbHandler;
	private final List<Post> mPosts = new ArrayList<Post>();
	private final Set<Integer> banned = new TreeSet<Integer>();

    private Set<Integer> opened = new HashSet<>();
	private View.OnClickListener postClickListener;

	public BoardAdapter(Context context) {
		this.mContext = context;
        CirnoidSettings settings = new CirnoidSettings(context);
		loadImages = settings.getImagesEnabled();
		dbHandler = new DbHandler(context);
	}

	private void setPosts(List<Post> result, boolean append) {
		if (!(append)) {
			clear();
		}
		for (Post post : result) {
			add(post);
		}
	}

	public void setPosts(List<Post> result) {
		setPosts(result, false);
		if (banned.size() > 50)
			banned.clear();
		for (Post post : result) {
			if (dbHandler.isBanned(post.board, post.id))
				banned.add(post.id);
		}
	}

	@Override
	public View getView(final int position, View convertView, final ViewGroup parent) {
		final Post post = getItem(position);

		final int viewType = getItemViewType(position);
		
		View v = convertView;

		switch (viewType) {
			case POST:
				if(v == null || !(v instanceof OpPostView)){
					v = new OpPostView(mContext);
				}
				break;

			case BANNED_POST:
				if(v == null || !(v instanceof BannedPostView)){
					v = new BannedPostView(mContext);
				}
				break;
		}
		
		
		switch (viewType) {
		case POST:
			final OpPostView postView = (OpPostView) v;
			postView.bindPost(post);
			break;
		case BANNED_POST:
			final BannedPostView bannedPostView = (BannedPostView)v;
			bannedPostView.bindPost(post);
			break;
		}

		return v;
	}


    @Override
	public int getViewTypeCount() {
		return 2;
	}
	
	@Override
	public int getItemViewType(int position) {
		final Post post = getItem(position);
		if(banned.contains(post.id)){
			return BANNED_POST;
		} else {
			return POST;
		}
	}


	public int getCount() {
		return mPosts.size();
	}

	public Post getItem(int position) {
		return mPosts.get(position);
	}

	public long getItemId(int position) {
		return position;
	}

	private void clear() {
		mPosts.clear();
		notifyDataSetChanged();
	}

	private void add(Post post) {
		mPosts.add(post);
	}
	
	public void banPost(int position) {
		Post post = getItem(position);
		dbHandler.banThread(post.board, post.id);
		banned.add(post.id);
		notifyDataSetChanged();
	}
	
	public void unbanPost(int position) {
		Post post = getItem(position);
		dbHandler.unbanThread(post.board, post.id);
		banned.remove(post.id);
		notifyDataSetChanged();
	}
	
}


