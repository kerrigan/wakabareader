package chan.reader.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import androidx.collection.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import chan.reader.R;
import chan.reader.objects.BoardUrl;

public class BoardGridAdapter extends BaseAdapter {

	private final Context mContext;
	private final BoardUrl[] mBoardUrls;
	private final LruCache<Integer, Drawable> icons;

	public BoardGridAdapter(Context context, BoardUrl[] boards) {
		this.mContext = context;
		icons = new LruCache<Integer, Drawable>(2 * 1024 * 1024);
		mBoardUrls = boards;
	}
	
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return mBoardUrls.length;
	}

	@Override
	public BoardUrl getItem(int position) {
		return mBoardUrls[position];
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		final BoardUrl board = getItem(position);
		View v = convertView;
		if (v == null || !(v instanceof LinearLayout)) {
			LayoutInflater vi = (LayoutInflater)mContext.getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.board_cell, null);
			
			ViewHolder holder = new ViewHolder();
			//holder.boardIcon = (ImageView)v.findViewById(R.id.board_icon);
			holder.name = (TextView)v.findViewById(R.id.name);
			
			v.setTag(holder);
		}
		
		ViewHolder holder = (ViewHolder)v.getTag();
		
		holder.name.setText(board.shortTitle);
		
		
		Drawable icon = icons.get(board.favicon);
		if(icon == null){
			icon = mContext.getResources().getDrawable(board.favicon);
			icons.put(board.favicon, icon);
		}
				
		holder.name.setCompoundDrawablesWithIntrinsicBounds(null, icon, null, null);

		return v;
	}
	
	static class ViewHolder {
		TextView name;
	}

}
