package chan.reader.adapters;

import android.content.Context;
import android.content.Intent;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import chan.reader.R;
import chan.reader.Registry;
import chan.reader.activities.ThreadActivity;
import chan.reader.db.DbHandler;
import chan.reader.objects.Post;

public class SubscriptionsAdapter extends ArrayAdapter<Post>{

	private final DbHandler dbHandler;


	public SubscriptionsAdapter(Context context) {
		super(context, R.layout.subscribed_post);
		// TODO Auto-generated constructor stub
		dbHandler = new DbHandler(context);
	}
	
	
	private void setPosts(List<Post> result, boolean append) {
		if (!(append)) {
			clear();
		}
		for (Post post : result) {
			add(post);
		}
	}

	public void setPosts(List<Post> result) {
		setPosts(result, false);
	}
	
	
	@Override
	public View getView(int position, View convertView, final ViewGroup parent) {
		final Post post = getItem(position);


		View v = convertView;
		if (v == null || !(v instanceof LinearLayout)) {
			LayoutInflater vi = (LayoutInflater) getContext().getSystemService(
					Context.LAYOUT_INFLATER_SERVICE);
			v = vi.inflate(R.layout.subscribed_post, null);
		}
			TextView title = (TextView) v.findViewById(R.id.title);
			TextView board = (TextView) v.findViewById(R.id.board);
			String text = Html.fromHtml(post.text).toString().trim();
			if(text.length() > 20){
				text = text.substring(0, 20);
			}
			title.setText(post.id + " " + text + "...");
			board.setText("/" + post.board + "/");
			
			ImageButton unsubBtn = (ImageButton)v.findViewById(R.id.unsubscribe);
			unsubBtn.setOnClickListener(new View.OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					dbHandler.unsubscribeThread(post.board, post.id);
					remove(post);
					Toast.makeText(getContext(), "Подписка на тред #" + post.id + " удалена", Toast.LENGTH_SHORT).show();
				}
			});
			
			v.setOnClickListener(new View.OnClickListener() {
				
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String THREAD_URL = Registry.SITE_URL + "/" + post.board + "/res/" + post.id + ".html";
					Intent intent = new Intent();
					intent.putExtra(ThreadActivity.BOARD_PREFIX_ID, post.board);
					intent.putExtra(ThreadActivity.THREAD_URL_ID, THREAD_URL);
					Context context = getContext();
					intent.setClass(context, ThreadActivity.class);
					context.startActivity(intent);
				}
			});
	
		return v;
	}
	
}