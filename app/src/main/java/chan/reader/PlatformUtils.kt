package chan.reader

import android.content.Context
import android.content.UriPermission
import android.net.Uri

object PlatformUtils {

    @JvmStatic
    fun hasPermissionToUri(context: Context, uri: Uri) : Boolean {
        if (uri.toString().isBlank()) {
            return false
        }
        val uriPermissions: List<UriPermission> = context.contentResolver.persistedUriPermissions
        return uriPermissions.any {
            it.uri == uri && it.isReadPermission && it.isWritePermission
        }
    }
}