package chan.reader.views;

import android.content.Context;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Collections;

import chan.reader.CirnoidSettings;
import chan.reader.KUtils;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.objects.Post;
import chan.reader.parsers.PostParser;
import coil.ImageLoader;
import coil.request.ImageRequest;

/**
 * Created by kerrigan on 19.03.15.
 */
public class PostView extends FrameLayout {
    private ImageLoader imageLoader;
    private ImageView thumb;
    private TextView title;
    private TextView text;
    private TextView timestamp;
    private TextView topic;
    private TextView number;

    private boolean loadImages = true; //TODO: load from settings in bindPost


    public PostView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public PostView(Context context){
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater.from(context).inflate(R.layout.post, this);
        title = (TextView)findViewById(R.id.title);
        text = (TextView)findViewById(R.id.text);
        thumb = (ImageView)findViewById(R.id.thumb);
        timestamp = (TextView)findViewById(R.id.timestamp);
        topic = (TextView)findViewById(R.id.topic);
        number = (TextView)findViewById(R.id.number);
        imageLoader = KUtils.initUntrustImageLoader(context);

        CirnoidSettings settings = new CirnoidSettings(context);
        loadImages = settings.getImagesEnabled();
    }

    public void bindPost(final Post post){
        text.setMovementMethod(LinkMovementMethod.getInstance());

        if(post.text.length() > 0 || (post.outReplies != null && post.outReplies.size() > 0)){
			if(post.buildedText == null){
				PostParser postParser = new PostParser(Collections.<Post>emptyList(), imageLoader, post.id);
				post.buildedText = postParser.parsePost(post.text);
			}


            text.setText(post.buildedText);
            text.setVisibility(View.VISIBLE);
        } else {
            text.setVisibility(View.GONE);
        }

        if (!post.title.equals("")) {
            topic.setVisibility(View.VISIBLE);
            topic.setText(post.title);
        } else {
            topic.setVisibility(View.GONE);
        }

        title.setText(post.name);
        number.setText(post.id + "");



        timestamp.setText(Utils.format.format(post.timestamp));


        if (loadImages && post.thumb != null && !post.thumb.isBlank()) {
            thumb.setVisibility(View.VISIBLE);
            ImageRequest request = new ImageRequest.Builder(getContext())
                    .data(Registry.SITE_URL + post.thumb)
                    .crossfade(true)
                    .target(thumb)
                    .build();
            imageLoader.enqueue(request);
        } else {
            thumb.setVisibility(View.GONE);
        }

        if (!"".equals(post.image)) {
            thumb.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    Utils.showImage(post.image, getContext());
                }
            });
        }
    }
}
