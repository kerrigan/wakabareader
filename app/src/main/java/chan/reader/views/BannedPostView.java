package chan.reader.views;

import android.content.Context;
import android.text.Html;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.TextView;

import chan.reader.R;
import chan.reader.objects.Post;

/**
 * Created by kerrigan on 20.03.15.
 */
public class BannedPostView extends FrameLayout {

    private TextView title;

    public BannedPostView(Context context) {
        super(context);
        init(context);
    }


    public BannedPostView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public BannedPostView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    private void init(Context context){
        LayoutInflater.from(context).inflate(R.layout.banned_post, this);
        title = (TextView)findViewById(R.id.banned_title);
    }

    public void bindPost(Post post){
        String text = Html.fromHtml(post.text).toString().trim();
        if (text.length() > 20) {
            text = text.substring(0, 20);
        }
        title.setText(post.id + " " + text + "...");
    }
}
