package chan.reader.views;

import android.content.Context;
import android.content.Intent;
import android.text.method.LinkMovementMethod;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import chan.reader.CirnoidSettings;
import chan.reader.KUtils;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.activities.PictureActivity;
import chan.reader.activities.ThreadActivity;
import chan.reader.objects.Post;
import chan.reader.parsers.PostParser;
import coil.ImageLoader;
import coil.request.ImageRequest;

/**
 * Created by kerrigan on 19.03.15.
 */

//TODO: это класс вью для поста на странице доски, здесь также будут показываться последние посты
public class OpPostView extends FrameLayout {
    private TextView title;
    private TextView text;
    private ImageView thumb;
    private TextView timestamp;
    public LinearLayout lastReplies;
    private TextView topic;
    private TextView number;
    private TextView omitted;
    private ImageButton openThread;
    private Button toggleReplies;

    private boolean loadImages;
    private ImageLoader imageLoader;
    private static final Set<Integer> opened = new HashSet<Integer>();


    public OpPostView(Context context) {
        super(context);
        init(context);
    }


    public OpPostView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }


    public OpPostView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }


    public void init(Context context){
        LayoutInflater.from(context).inflate(R.layout.oppost, this);

        CirnoidSettings settings = new CirnoidSettings(context);
        loadImages = settings.getImagesEnabled();

        imageLoader = KUtils.initUntrustImageLoader(context);

        title = (TextView)findViewById(R.id.title);
        text = (TextView)findViewById(R.id.text);
        thumb = (ImageView)findViewById(R.id.thumb);
        timestamp = (TextView)findViewById(R.id.timestamp);
        topic = (TextView)findViewById(R.id.topic);
        number = (TextView)findViewById(R.id.number);
        omitted = (TextView)findViewById(R.id.omitted);
        lastReplies = (LinearLayout)findViewById(R.id.lstLastReplies);
        openThread = (ImageButton)findViewById(R.id.openThread);
        toggleReplies = (Button)findViewById(R.id.toggleReplies);
    }


    public void bindPost(final Post post){
        if(post.text.length() > 0){
            //Парсим всё хитрым парсером с кучей спанов для стилизации текста
            if(post.buildedText == null){
                PostParser postParser = new PostParser(new ArrayList<Post>(),
                        imageLoader, post.id);
                post.buildedText = postParser.parsePost(post.text);
            }
            text.setText(post.buildedText);
            text.setVisibility(View.VISIBLE);
        } else {
            text.setVisibility(View.GONE);
        }
        text.setMovementMethod(LinkMovementMethod.getInstance());
        //Для тестирования нового парсера

        if (!post.title.equals("")) {
            topic.setVisibility(View.VISIBLE);
            topic.setText(post.title);
        } else {
            topic.setVisibility(View.GONE);
        }
        title.setText(post.name);
        number.setText(post.id + "");


        //Конвертирование даты в нужный формат
        timestamp.setText(Utils.format.format(post.timestamp));

        if (loadImages && (post.thumb != "")) {
            ImageRequest request = new ImageRequest.Builder(getContext())
                    .data(Registry.SITE_URL + post.thumb)
                    .crossfade(true)
                    .target(thumb)
                    .build();
            imageLoader.enqueue(request);
            thumb.setOnClickListener(new View.OnClickListener() {

                public void onClick(View v) {
                    showImage(post.image);
                }
            });
        } else {
            thumb.setImageResource(R.drawable.empty);
        }


        openThread.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String THREAD_URL = Registry.SITE_URL + "/" + post.board + "/res/" + post.id + ".html";
                Intent intent = new Intent();
                intent.putExtra(ThreadActivity.BOARD_PREFIX_ID, post.board);
                intent.putExtra(ThreadActivity.THREAD_URL_ID, THREAD_URL);
                intent.setClass(getContext(), ThreadActivity.class);
                getContext().startActivity(intent);
            }
        });


        //Show last replies
        omitted.setText(Integer.toString(post.omittedImagesCount) + "/" + Integer.toString(post.omittedCount));
        lastReplies.removeAllViews();

        if(OpPostView.opened.contains(post.id)){
            lastReplies.setVisibility(View.VISIBLE);
            buildReplies(post);
        } else {
            lastReplies.setVisibility(View.GONE);
        }


        toggleReplies.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (OpPostView.opened.contains(post.id)) {
                    OpPostView.opened.remove(post.id);
                    lastReplies.setVisibility(View.GONE);
                    lastReplies.removeAllViews();
                } else {
                    OpPostView.opened.add(post.id);
                    buildReplies(post);

                    lastReplies.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void buildReplies(Post post) {
        if(post.lastReplies != null && post.lastReplies.size() > 0) {
            lastReplies.removeAllViews();
            PostView lastReplyView;
            Post reply;


            for (int j = 0; j < post.lastReplies.size(); ++j) {
                lastReplyView = new PostView(getContext());
                reply = post.lastReplies.get(j);
                lastReplyView.bindPost(reply);
                lastReplies.addView(lastReplyView);
            }

        }
    }

    private void showImage(String url) {
        Context context = getContext();
        Intent imageViewerIntent = new Intent(context, PictureActivity.class);
        imageViewerIntent.putExtra(PictureActivity.IMAGE_URL, Registry.SITE_URL + url);
        context.startActivity(imageViewerIntent);
    }
}
