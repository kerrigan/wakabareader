package chan.reader.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;
import android.widget.RemoteViews;

import chan.reader.CirnoidSettings;
import chan.reader.CustomMultiPartEntity.ProgressListener;
import chan.reader.Length;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.WResponse;
import chan.reader.activities.BoardActivityDeprecated;
import chan.reader.activities.PostFormActivity;
import chan.reader.activities.ThreadActivity;
import chan.reader.db.DbHandler;
import chan.reader.objects.ReplyData;

public class PostCreateService extends IntentService {

	public PostCreateService(String name) {
		super(name);
		new NotificationCompat.Builder(this);
	}

	public PostCreateService() {
		super("HttpPostService");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
        CirnoidSettings settings = new CirnoidSettings(this);
		try {
		ReplyData data = (ReplyData) intent.getParcelableExtra("data");
		final int threadId = Integer.parseInt(data.threadId);
		Context context = getApplicationContext();
		final NotificationManager notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

		final Length requestLength = new Length();
		
		if(threadId != 0){

			//mBuilder.setContentTitle(title)
			
			final Notification notifyMsgPost = new Notification();
			notifyMsgPost.flags = Notification.FLAG_AUTO_CANCEL;
			notifyMsgPost.icon = android.R.drawable.sym_action_email;
			notifyMsgPost.when = System.currentTimeMillis() + 500;
			notifyMsgPost.tickerText = "Ответ отправляется";
			
			
			/*
			 * Указываем, что должно отобразиться внутри уведомления
			 */
			notifyMsgPost.contentView = new RemoteViews(context.getPackageName(), R.layout.notification);
			notifyMsgPost.contentView.setProgressBar(R.id.status_progress, 100, 0, true);
			notifyMsgPost.contentView.setTextViewText(R.id.status_text, "Ответ отправляется");
		

			/*
			 * Интент для открытия активити
			 */
			Intent notificationIntent = new Intent(context,
					ThreadActivity.class);
			notificationIntent.putExtra(ThreadActivity.BOARD_PREFIX_ID,
					data.prefix);
			notificationIntent.putExtra(ThreadActivity.THREAD_URL_ID,
					Registry.SITE_URL + "/" + data.prefix + "/res/"
							+ data.threadId + ".html");
			
			/*
			 * Интент для контента, который покажет уведомления(
			 * в т.ч. для интента открытия активити)
			 */
			PendingIntent pIntent = PendingIntent.getActivity(context,
					threadId, notificationIntent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
			notifyMsgPost.contentIntent = pIntent;
			
			notifyManager.notify(threadId, notifyMsgPost);
			
			

			WResponse result = Utils.newReply(context, data.baseUrl, data.prefix,
					data.threadId, data.name, data.topic, data.text,
					data.imageUri, data.captcha, data.password, new ProgressListener() {
						
						@Override
						public void transferred(long num) {
							int progress = (int) ((num / (float) requestLength.totalSize) * 100);
							notifyMsgPost.contentView.setProgressBar(R.id.status_progress, 100, progress, false);
							notifyManager.notify(threadId, notifyMsgPost);
						}
					}, requestLength);

			String tickerText;
			String notifyTitle;
			String notifyText;
			
			if(result.success){
				tickerText = "Ответ отправлен";
				notifyTitle = "Ответ отправлен";
				notifyText = "Ответ в тред #" + data.threadId + " отправлен";
				
				Intent broadcastIntent = new Intent(ThreadActivity.UPDATE_THREAD);
				broadcastIntent.putExtra(ThreadActivity.UPDATE_THREAD,  data.prefix + data.threadId);
				LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                settings.setLastSavedMessage("");
                if(settings.getSubscribeToPosted()){
                    DbHandler dbHandler = new DbHandler(this);
                    dbHandler.subscribeThread(data.prefix, Integer.valueOf(data.threadId));
                }
			} else {
				tickerText = "Ошибка отправления ответа";
				notifyTitle = "Ошибка отправления ответа в тред #" + data.threadId;
				notifyText = result.errorMessage;
				
				Intent returnToPostFormIntent = new Intent(this, PostFormActivity.class);
				returnToPostFormIntent.putExtra(PostFormActivity.BOARD_PREFIX_ID, data.prefix);
				returnToPostFormIntent.putExtra(PostFormActivity.IMAGE_URI_ID, data.imageUri);
				returnToPostFormIntent.putExtra(PostFormActivity.INIT_TEXT_ID, data.text);
				returnToPostFormIntent.putExtra(PostFormActivity.NAME_ID, data.name);
				returnToPostFormIntent.putExtra(PostFormActivity.PASSWORD_ID, data.password);
				returnToPostFormIntent.putExtra(PostFormActivity.THREAD_NUMBER_ID, Integer.parseInt(data.threadId));
				returnToPostFormIntent.putExtra(PostFormActivity.TOPIC_ID, data.topic);
				
				
				pIntent = PendingIntent.getActivity(context, threadId, returnToPostFormIntent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
			}


			Notification notifyMsgPosted = new NotificationCompat.Builder(this)
					.setAutoCancel(true)
					.setSmallIcon(android.R.drawable.sym_action_email)
					.setWhen(System.currentTimeMillis() + 500)
					.setTicker(tickerText)
					.setContentTitle(notifyTitle)
					.setContentText(notifyText)
					.setContentIntent(pIntent)
					.build();
			
			notifyManager.notify(threadId, notifyMsgPosted);
		} else {
			Notification notifyMsgPost = new Notification();
			notifyMsgPost.flags = Notification.FLAG_AUTO_CANCEL;
			notifyMsgPost.icon = android.R.drawable.sym_action_email;
			notifyMsgPost.when = System.currentTimeMillis() + 500;
			notifyMsgPost.tickerText = "Ответ отправляется";
			
			notifyMsgPost.contentView = new RemoteViews(context.getPackageName(), R.layout.notification);
			notifyMsgPost.contentView.setProgressBar(R.id.status_progress, 100, 0, true);
			notifyMsgPost.contentView.setTextViewText(R.id.status_text, "Ответ отправляется");
			
			PendingIntent pIntent = PendingIntent.getActivity(context,
					threadId, new Intent(), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
			
			
			notifyMsgPost.contentIntent = pIntent;
			notifyManager.notify(threadId, notifyMsgPost);	
			
			WResponse result = Utils.newThread(context, data.baseUrl, data.prefix,
					data.name, data.topic, data.text,
					data.imageUri, data.captcha, data.password, new ProgressListener() {
						
						@Override
						public void transferred(long num) {}
					}, requestLength);

			
			String tickerText;
			String notifyTitle;
			String notifyText;
			
			if(result.success){
				tickerText = "Тред создан";
				notifyTitle = "Тред создан";
				notifyText = "Тред в /" + data.prefix + "/ успешно создан";
				
				Intent broadcastIntent = new Intent(BoardActivityDeprecated.UPDATE_BOARD);
				broadcastIntent.putExtra(BoardActivityDeprecated.UPDATE_BOARD, data.prefix);
				LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
			} else {
				//Toast.makeText(this, result.errorMessage, Toast.LENGTH_LONG).show();
				tickerText = "Ошибка создания треда в /" + data.prefix + "/";
				notifyTitle = "Ошибка создания треда";
				notifyText = result.errorMessage;
				
				Intent returnToPostFormIntent = new Intent(this, PostFormActivity.class);
				returnToPostFormIntent.putExtra(PostFormActivity.BOARD_PREFIX_ID, data.prefix);
				returnToPostFormIntent.putExtra(PostFormActivity.IMAGE_URI_ID, data.imageUri);
				returnToPostFormIntent.putExtra(PostFormActivity.INIT_TEXT_ID, data.text);
				returnToPostFormIntent.putExtra(PostFormActivity.NAME_ID, data.name);
				returnToPostFormIntent.putExtra(PostFormActivity.PASSWORD_ID, data.password);
				returnToPostFormIntent.putExtra(PostFormActivity.TOPIC_ID, data.topic);
				
				pIntent = PendingIntent.getActivity(context, threadId, returnToPostFormIntent, PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
			}

			Notification notifyMsgPosted = new NotificationCompat.Builder(this)
					.setAutoCancel(true)
					.setSmallIcon(android.R.drawable.sym_action_email)
					.setWhen(System.currentTimeMillis() + 500)
					.setTicker(tickerText)
					.setContentTitle(notifyTitle)
					.setContentText(notifyText)
					.setContentIntent(pIntent).build();
			
			notifyManager.notify(threadId, notifyMsgPosted);
		}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			// Show error in notification
		}
	}

}
