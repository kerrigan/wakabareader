package chan.reader.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import chan.reader.CirnoidSettings;
import chan.reader.R;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.WResponse;
import chan.reader.activities.BoardActivityDeprecated;
import chan.reader.activities.ThreadActivity;

/**
 * Created by kerrigan on 09.04.15.
 */
public class PostDeleteService extends IntentService {
    private static final String TAG = PostDeleteService.class.getSimpleName();

    public static final String DELETE_POST_ID = "DELETE_POST_ID";
    public static final String BOARD_PREFIX_ID = "BOARD_PREFIX_ID";
    private static final String DELETE_THREAD = "DELETE_THREAD";

    public PostDeleteService() {
        super("HttpDeleteService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        CirnoidSettings settings = new CirnoidSettings(this);
        NotificationManager notifyManager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

        int postIdToDelete = intent.getIntExtra(DELETE_POST_ID, -1);
        String boardPrefix = intent.getStringExtra(BOARD_PREFIX_ID);
        boolean deleteThread = intent.getBooleanExtra(DELETE_THREAD, false);


        PendingIntent pIntent = PendingIntent.getActivity(this,
                0, new Intent(), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);


        NotificationCompat.Builder progressBuilder = new NotificationCompat.Builder(this)
                .setContentTitle("Подождите")
                .setSmallIcon(R.drawable.ic_launcher)
                .setAutoCancel(false)
                .setProgress(0, 100, true)
                .setContentIntent(pIntent);

        Notification progressNotify;
        if(deleteThread){
            progressNotify =  progressBuilder.setContentText("Тред удаляется")
                    .build();
        } else {
            progressNotify =  progressBuilder.setContentText("Пост удаляется")
                    .build();
        }

        notifyManager.notify(001, progressNotify);


        WResponse result = Utils.deletePost(this, Registry.SITE_URL, boardPrefix, postIdToDelete, settings.getPassword());

        NotificationCompat.Builder resultNotifyBuilder = new NotificationCompat.Builder(this)
                                                                   .setAutoCancel(true)
                                                                   .setSmallIcon(R.drawable.ic_launcher)
                                                                   .setContentIntent(pIntent);

        if(result.success) {
            Notification resultNotify;
            if (deleteThread) {
                resultNotify = resultNotifyBuilder.setContentTitle("Удален тред /" + boardPrefix + "/" + postIdToDelete).build();
                notifyManager.notify(1, resultNotify);
                Intent broadcastIntent = new Intent(BoardActivityDeprecated.UPDATE_BOARD);
                broadcastIntent.putExtra(BoardActivityDeprecated.UPDATE_BOARD,  boardPrefix);
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            } else {
                resultNotify = resultNotifyBuilder.setContentTitle("Удалено сообщение /" + boardPrefix + "/" + postIdToDelete).build();
                notifyManager.notify(1, resultNotify);
                Intent broadcastIntent = new Intent(ThreadActivity.UPDATE_THREAD);
                broadcastIntent.putExtra(ThreadActivity.UPDATE_THREAD,  boardPrefix + postIdToDelete);
                LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
            }
        } else {
            notifyManager.notify(1, resultNotifyBuilder.setContentTitle(result.errorMessage).build());
        }
    }

    public static void deletePost(Context ctx, String boardPrefix, int postId, boolean deleteThread){
        Intent intent = new Intent(ctx, PostDeleteService.class);
        intent.putExtra(DELETE_POST_ID, postId);
        intent.putExtra(BOARD_PREFIX_ID, boardPrefix);
        intent.putExtra(DELETE_THREAD, deleteThread);
        ctx.startService(intent);
    }
}
