package chan.reader.services;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.util.Log;
import android.widget.RemoteViews;

import java.util.List;

import androidx.core.app.NotificationCompat;
import chan.reader.CirnoidSettings;
import chan.reader.R;
import chan.reader.Utils;
import okhttp3.OkHttpClient;

public class ImageDownloadService extends IntentService {

	private static final String TAG = ImageDownloadService.class.getSimpleName();
	private static final String CHANNEL_ID = "channel_id";
	private String mImagesPath;

	public ImageDownloadService() {
		super("download images");
	}

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
        CirnoidSettings mSettings = new CirnoidSettings(this);
		mImagesPath = mSettings.getSavedImagesPath();
		super.onCreate();
	}

	@Override
	protected void onHandleIntent(Intent intent) {
			List<String> urls = (List<String>) intent
					.getSerializableExtra("images");

			createNotificationChannel();
			NotificationManager notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);

			
			Notification notifyMsgPost = new Notification();
			notifyMsgPost.flags = Notification.FLAG_AUTO_CANCEL;
			notifyMsgPost.icon = android.R.drawable.sym_action_email;
			notifyMsgPost.when = System.currentTimeMillis() + 500;
			notifyMsgPost.tickerText = "Изображения загружаются";

			notifyMsgPost.contentView = new RemoteViews(
					this.getPackageName(), R.layout.notification);
			notifyMsgPost.contentView.setProgressBar(
					R.id.status_progress, 100, 0, true);
			notifyMsgPost.contentView.setTextViewText(R.id.status_text,
					"Изображения загружаются");

			PendingIntent pIntent = PendingIntent.getActivity(this,
					0, new Intent(), PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
			
			notifyMsgPost.contentIntent = pIntent;
			
			notifyManager.notify(0, notifyMsgPost);

			OkHttpClient httpClient = Utils.getOkHTTP(getApplicationContext());
			
			for (int i = 0; i < urls.size(); ++i) {
				String url = urls.get(i);
				
				boolean isSuccess = Utils.downloadFromUrlToURI(getApplicationContext(), httpClient, url, Uri.parse(mImagesPath));
				if(!isSuccess){
					i--;
					Log.w(TAG, "Retry download");
					continue;
				}
				int progress = (int) ((i / (float) urls.size()) * 100);
				notifyMsgPost.contentView.setProgressBar(
						R.id.status_progress, 100, progress, false);
				notifyManager.notify(0, notifyMsgPost);
			}


		Notification notifyMsgPosted = new NotificationCompat.Builder(this, CHANNEL_ID)
				.setAutoCancel(true)
				.setSmallIcon(android.R.drawable.sym_action_email)
				.setWhen(System.currentTimeMillis() + 500)
				.setTicker("Все изображения загружены")
				.setContentTitle("Изображения загружены")
				.setContentText("Все изображения загружены")
				.setContentIntent(pIntent).build();
			
		notifyManager.notify(0, notifyMsgPosted);
	}


	private void createNotificationChannel() {
		// Create the NotificationChannel, but only on API 26+ because
		// the NotificationChannel class is not in the Support Library.
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			CharSequence name = "channel_name";
			String description = "channel_description";
			int importance = NotificationManager.IMPORTANCE_DEFAULT;
			NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
			channel.setDescription(description);
			// Register the channel with the system. You can't change the importance
			// or other notification behaviors after this.
			NotificationManager notificationManager = getSystemService(NotificationManager.class);
			notificationManager.createNotificationChannel(channel);
		}
	}
}
