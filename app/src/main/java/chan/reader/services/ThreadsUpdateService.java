package chan.reader.services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.os.Process;
import android.os.Vibrator;
import androidx.core.app.NotificationCompat;
import android.util.Log;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.atomic.AtomicBoolean;

import chan.reader.CirnoidSettings;
import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.activities.ThreadActivity;
import chan.reader.db.DbHandler;
import chan.reader.objects.Post;
import chan.reader.objects.PostDefinition;
import chan.reader.parsers.ThreadParser;

public class ThreadsUpdateService extends Service {
	private static final String TAG = ThreadsUpdateService.class.getSimpleName();
	private static final Timer timer = new Timer();
	private static List<PostDefinition> subscriptions = new ArrayList<PostDefinition>();
	private DbHandler dbHandler;
	private ThreadParser parser;
	private NotificationManager notifyManager;
	private List<Post> replies;
	private Vibrator vibra;
	private boolean enable_vibration = false;
	private final Random randomGenerator = new Random(System.currentTimeMillis());
	//private final LocalBinder mBinder = new LocalBinder();
	private Thread mLoopThread;
	private AtomicBoolean keepRunning = new AtomicBoolean(true);
	
	@Override
	public void onCreate() {
		super.onCreate();
		dbHandler = new DbHandler(this);
		parser = new ThreadParser();
		notifyManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        CirnoidSettings settings = new CirnoidSettings(this);
		enable_vibration = settings.getVibrationEnabled();
		vibra = (Vibrator) getSystemService(VIBRATOR_SERVICE);

		
		mLoopThread = new Thread() {
			public void run() {
				updateLoop();
			}
		};
		mLoopThread.setPriority(Process.THREAD_PRIORITY_BACKGROUND);
		mLoopThread.start();
	}

	@Override
	public void onDestroy() {
		Log.d("THREADUPDATER", "close");
		stopLoop();
		super.onDestroy();
	}
	
	
	private class LocalBinder extends Binder {
		ThreadsUpdateService getService(){
			return ThreadsUpdateService.this;
		}
	}

	@Override
	public IBinder onBind(Intent intent) {
		//return mBinder;
		return null;
	}
	
	/*
	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);
	}*/
	private void stopLoop(){
		keepRunning = new AtomicBoolean(false);
		timer.cancel();
		mLoopThread.interrupt();
	}

	private void updateLoop() {
		TimerTask task = new TimerTask() {

			@Override
			public void run() {
				//Log.d("UPDATE THREADS", "begin");
				keepRunning = new AtomicBoolean(true);

				subscriptions = dbHandler.getSubscriptions();
				for (int i = 0;i < subscriptions.size();++i) {
					if(!keepRunning.get()){
						break;
					}
					final PostDefinition sub = subscriptions.get(i);
					synchronized (Registry.dbMonitor) {
						//Log.d("UPDATE THREADS", "Begin thread " + sub.board
						//		+ " " + sub.threadId);
						try {
							replies = new ArrayList<Post>();
							String threadUrl = Registry.SITE_URL + "/"
									+ sub.board + "/res/" + sub.threadId
									+ ".html";
							HttpResponse response = Utils
									.urlToConnection(getBaseContext(), threadUrl);
							final String hash = response.getLastHeader("ETag").getValue();

							if (!dbHandler.threadChanged(sub.board,
									sub.threadId, hash)) {
								//Log.d("Board",
								//		"Page not changed, loading from cache");
								replies = dbHandler.getThread(sub.board,
										sub.threadId);
							} else {
								boolean isGzip = Utils.isGzip(response.getHeaders("Content-Encoding"));
								String threadHtml = Utils.streamToString(response.getEntity().getContent(), isGzip);
								parser.parseUrl(sub.board,
										threadHtml, hash);
								replies = parser.getComments();
								final List<Post> repliesToThread = new ArrayList<Post>(
										replies);
								int prevPostsCount = dbHandler.getThread(sub.board, sub.threadId).size();
								int newPostsCount = repliesToThread.size() - prevPostsCount;
								dbHandler.upsertThread(sub.board,
										repliesToThread, hash);


								NotificationCompat.Builder notifyMsgPostedBuilder = new NotificationCompat.Builder(ThreadsUpdateService.this)
										.setAutoCancel(true)
										.setSmallIcon(android.R.drawable.sym_action_chat)
										.setWhen(System.currentTimeMillis() + 1000)
										.setTicker("Тред обновлен");

								Intent notificationIntent = new Intent(
										ThreadsUpdateService.this,
										ThreadActivity.class);
								notificationIntent.putExtra(
										ThreadActivity.BOARD_PREFIX_ID,
										sub.board);
								notificationIntent.putExtra(
										ThreadActivity.THREAD_URL_ID,
										Registry.SITE_URL + "/" + sub.board
										+ "/res/" + sub.threadId
										+ ".html");
								PendingIntent pIntent = PendingIntent
										.getActivity(ThreadsUpdateService.this,
												sub.threadId,
												notificationIntent,
												PendingIntent.FLAG_ONE_SHOT | PendingIntent.FLAG_IMMUTABLE);
								
								StringBuilder notifyMessage = new StringBuilder();
								notifyMessage.append(sub.board);
								notifyMessage.append("#");
								notifyMessage.append(sub.threadId);
								notifyMessage.append(" ");
								notifyMessage.append(newPostsCount);
								notifyMessage.append(" ");
								
								int remainder = newPostsCount % 10;
								if(remainder == 1){
									notifyMessage.append("новый ответ");
								} else if(remainder > 2 && remainder < 5){
									notifyMessage.append("новых ответа");
								} else {
									notifyMessage.append("новых ответов");
								}

								Notification notifyMsgPosted = notifyMsgPostedBuilder.setContentTitle("Тред обновлен")
										.setContentText(notifyMessage.toString())
										.setContentIntent(pIntent).build();
								notifyManager.notify(sub.threadId,
										notifyMsgPosted);

								if (enable_vibration){
									vibra.vibrate(300);
								}
							}
						}  catch (HttpException e) {
							//Помечаем тред как протухший
							
							dbHandler.markNonExistent(sub.board, sub.threadId);	
							Log.i(TAG, "Thread #" + sub.board + "/" + sub.threadId + " not exists");
						}  catch (IOException e) {
							Log.e(TAG, "IOError:" + e.getMessage());
						} catch (SAXException e) {
							Log.e(TAG, "Parsing exception");
						}
					}
					
					try {
						Thread.sleep(5000 * (randomGenerator.nextInt(5) + 1));
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		};
		
		timer.schedule(task, 0, 1000 * 60 * 5);
	}
}
