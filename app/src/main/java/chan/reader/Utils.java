package chan.reader;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.text.Html;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.Window;
import android.webkit.MimeTypeMap;
import android.widget.EditText;

import org.apache.http.Header;
import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.InputStreamBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.client.DefaultRedirectHandler;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.jetbrains.annotations.NotNull;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.nio.charset.Charset;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.GZIPInputStream;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.KeyManager;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.documentfile.provider.DocumentFile;
import chan.reader.CustomMultiPartEntity.ProgressListener;
import chan.reader.activities.PictureActivity;
import chan.reader.objects.Post;
import kotlin.Unit;
import kotlin.jvm.functions.Function1;
import okhttp3.CacheControl;
import okhttp3.ConnectionSpec;
import okhttp3.Cookie;
import okhttp3.CookieJar;
import okhttp3.HttpUrl;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class Utils {
	private static final String CAPTCHA_PARAMETER = "captcha";
	private static final String ATTACHED_FILE_PARAMETER = "file";
	private static final String TEXT_PARAMETER = "nya4";
	private static final String TOPIC_PARAMETER = "nya3";
	//private static final String EMAIL_PARAMETER = "nya2";
	private static final String NAME_PARAMETER = "nya1";
	private static final String THREAD_ID_PARAMETER = "parent";
	private static final String TASK_PARAMETER = "task";
	private static final String TASK_VALUE = "post";

	public static final SimpleDateFormat format = new SimpleDateFormat(
			"dd-MM-yyyy HH:mm:ss", Locale.US);
	private static final Pattern errorMsgRegex = Pattern.compile("<h1 style=\"text-align:" +
			" center\">Ошибка:(.+?)<br /><br />.*?Назад</a>");
	
	
	public static int chooseTheme(int themeId) {
		switch (themeId) {
		case 0:
			return R.style.Futaba_default;
		case 1:
			return R.style.Futaba_medium;
		case 2:
			return R.style.Futaba_large;
		default:
			return R.style.Futaba_default;
		}
	}

	public static HttpResponse urlToConnection(Context ctx, String pageUrl) throws HttpException, IOException {
		HttpClient client = Utils.getHttp(ctx);

		HttpGet request = new HttpGet(pageUrl);
		request.setHeader("User-Agent", Registry.USER_AGENT);
		request.setHeader("Connection", "keep-alive");
        request.setHeader("Accept-Encoding", "gzip");
		
		HttpResponse response = client.execute(request);
		if(response.getStatusLine().getStatusCode() == 404)
			throw new HttpException("404 Error");
		return response;
	}

	public static String streamToString(InputStream stream, boolean isGzip) {
        if(isGzip){
            try {
                stream = new GZIPInputStream(stream);
            } catch (IOException e) {
                e.printStackTrace();
                //If can't read gzip - read as plain stream
            }
        }
		InputStreamReader reader = new InputStreamReader(stream);

		BufferedReader buff = new BufferedReader(reader);
		StringBuilder strBuff = new StringBuilder();

		String s;
		try {
			while ((s = buff.readLine()) != null) {
				strBuff.append(s);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return strBuff.toString();
	}

	public static Bitmap urlToBitmap(Context context, String url) throws IOException {
		OkHttpClient http = getOkHTTP(context);
		Request request = new Request.Builder()
				.url(url)
				.cacheControl(CacheControl.FORCE_NETWORK)
				.header("User-Agent", getUserAgent(context))
				.header("Connection", "keep-alive")
				.build();
		Response response = http.newCall(request).execute();

		InputStream stream = response.body().byteStream();
		return BitmapFactory.decodeStream(stream);
	}

	public static WResponse deletePost(Context context, String BASE_URL, String prefix, int postId, String password){
		WResponse result = new WResponse();
		try {

			HttpClient client = Utils.getHttp(context);

			HttpPost request = new HttpPost(BASE_URL + "/cgi-bin/wakaba.pl/" + prefix + "/");

			List<NameValuePair> params = new LinkedList<NameValuePair>();
			params.add(new BasicNameValuePair("delete", Integer.toString(postId)));
			params.add(new BasicNameValuePair("task", "delete"));
			params.add(new BasicNameValuePair("password", password));
			UrlEncodedFormEntity entity = null;

			entity = new UrlEncodedFormEntity(params);
			request.setEntity(entity);
			request.setHeader("Accept-Encoding", "gzip");


			//HttpGet request = new HttpGet(BASE_URL + "/cgi-bin/wakaba.pl/" + prefix + "/?task=delete&delete=" + postId + "&password=" + password);

			HttpResponse response = client.execute(request);


			Header[] headers = response.getHeaders("Content-Encoding");
			boolean isGzip = isGzip(headers);;

			String data = streamToString(response.getEntity().getContent(), isGzip);
			result = buildResponse(data);
		} catch (IOException e) {
			e.printStackTrace();
			result.success = false;
			result.errorMessage = "Не удалось удалить пост";
		}
		return result;
	}

	public static WResponse newReply(Context context, String BASE_URL,
			String prefix, String threadId, String name, String topic,
			String text, String fileUri, String captcha, String password,
			ProgressListener listener, Length requestLength)
			throws IOException {
		/*
		 * nya1 - имя, nya2 - email, nya3 - тема, nya4 - text, file - файл, captcha - капча
		 */
		WResponse result = new WResponse();
		HttpClient client = Utils.getHttp(context);

		HttpPost request = new HttpPost(BASE_URL + "/cgi-bin/wakaba.pl/"
				+ prefix + "/");

		Charset charset = Charset.forName("UTF-8");

		CustomMultiPartEntity multipart = new CustomMultiPartEntity(HttpMultipartMode.STRICT, listener);

		multipart.addPart(TASK_PARAMETER, new StringBody(TASK_VALUE, charset));

		multipart.addPart(THREAD_ID_PARAMETER,
				new StringBody(threadId, charset));

		multipart.addPart(NAME_PARAMETER, new StringBody(name, charset));

		multipart.addPart(TOPIC_PARAMETER, new StringBody(topic, charset));

		multipart.addPart(TEXT_PARAMETER, new StringBody(text, charset));

		multipart.addPart("password", new StringBody(password, charset));

		if (!fileUri.trim().equals("")) {
			Uri imageUri = Uri.parse(fileUri);
			String fileName = getPath(context, imageUri);
			multipart.addPart(ATTACHED_FILE_PARAMETER, new FileBody(new File(fileName)));
		}

		if (!captcha.trim().equals("")) {
			multipart.addPart(CAPTCHA_PARAMETER, new StringBody(captcha));
		} else {
			result.success = false;
			result.errorMessage = "Введён неверный код подтверждения.";
			return result;
		}

		requestLength.totalSize = multipart.getContentLength();
		request.setEntity(multipart);
		request.setHeader("Accept-Encoding", "gzip");

		HttpResponse response = client.execute(request);

		Header[] headers = response.getHeaders("Content-Encoding");
        boolean isGzip = isGzip(headers);
		//TODO: fix encoding

		String data = streamToString(response.getEntity().getContent(), isGzip);
		result = buildResponse(data);

		return result;
	}

	public static WResponse newThread(Context context, String BASE_URL,
			String prefix, String name, String topic, String text,
			String fileUri, String captcha, String password, ProgressListener listener, Length requestLength) throws IOException {
		/*
		 * nya2 - имя, nya3 - тема, nya4 - text, file - файл, captcha - капча
		 */
		WResponse result = new WResponse();

		HttpClient client = Utils.getHttp(context);

		HttpPost request = new HttpPost(BASE_URL + "/cgi-bin/wakaba.pl/"
				+ prefix + "/");

		Charset charset = Charset.forName("UTF-8");

		CustomMultiPartEntity multipart = new CustomMultiPartEntity(HttpMultipartMode.STRICT, listener);

		multipart.addPart(TASK_PARAMETER, new StringBody(TASK_VALUE, charset));

		multipart.addPart(NAME_PARAMETER, new StringBody(name, charset));

		multipart.addPart(TOPIC_PARAMETER, new StringBody(topic, charset));

		multipart.addPart(TEXT_PARAMETER, new StringBody(text, charset));
		multipart.addPart("password", new StringBody(password, charset));

		if (!fileUri.trim().equals("")) {
			Uri imageUri = Uri.parse(fileUri);
			String mimeType = context.getContentResolver().getType(imageUri);
			InputStream fileStream = context.getContentResolver()
					.openInputStream(imageUri);
			multipart.addPart(ATTACHED_FILE_PARAMETER, new InputStreamBody(
					fileStream, "image." + mimeType.split("/")[1]));
		} else {
			result.success = false;
			result.errorMessage = "Сообщения без изображений запрещены.";
			return result;
		}

		if (!captcha.trim().equals("")) {
			multipart.addPart(CAPTCHA_PARAMETER, new StringBody(captcha));
		} else {
			result.success = false;
			result.errorMessage = "Введён неверный код подтверждения.";
			//Toast.makeText(context, "Не введена капча!", Toast.LENGTH_SHORT)
			//		.show();
			return result;
		}

		requestLength.totalSize = multipart.getContentLength();
		
		
		request.setEntity(multipart);
		request.setHeader("Accept-Encoding", "gzip");

		HttpResponse response = client.execute(request);
		Header[] headers = response.getHeaders("Content-Encoding");
        boolean isGzip =  isGzip(headers);

		String data = streamToString(response.getEntity().getContent(), isGzip);

		result = buildResponse(data);

		return result;

	}


	public static boolean downloadFromUrlToURI(Context context, OkHttpClient httpClient, String urlString, Uri uri) {
		String filename = urlString.split("/src/")[1];

		DocumentFile docFile = null;
		OutputStream output;
		try {

			String docId = DocumentsContract.getTreeDocumentId(uri);
			Uri dirUri = DocumentsContract.buildDocumentUriUsingTree(uri, docId);

			Uri destUri = DocumentsContract.createDocument(context.getContentResolver(), dirUri, "*/*", filename);

			output = context.getContentResolver().openOutputStream(destUri, "w");

			Request request = new Request.Builder()
					.url(urlString)
					.header("User-Agent", Registry.USER_AGENT)
					.header("Connection", "keep-alive")
					.build();


			Response response = httpClient.newCall(request).execute();

			if(response.code() != 200){
				output.close();
				return false;
			}

			InputStream input = response.body().byteStream();

			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
				output.write(buffer, 0, bytesRead);
			}
			output.close();
			return true;

		} catch (IOException e) {
			//Удаляем файл, если скачивание прервалось
			docFile.delete();
			e.printStackTrace();
			return false;
		}

	}

	public static String getMimeType(String url) {
		String type = null;
		String extension = MimeTypeMap.getFileExtensionFromUrl(url);
		if (extension != null) {
			type = MimeTypeMap.getSingleton().getMimeTypeFromExtension(extension);
		}
		return type;
	}
	public static boolean downloadFromUrl(OkHttpClient httpClient, String urlString, String path) {
		File file = new File(path, urlString.split("/src/")[1]);
		if (!file.getParentFile().exists())
			file.getParentFile().mkdir();
		if (!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		OutputStream output;
		try {
			output = new FileOutputStream(file);

			Request request = new Request.Builder()
					.url(urlString)
					.header("User-Agent", Registry.USER_AGENT)
					.header("Connection", "keep-alive")
					.build();

			Response response = httpClient.newCall(request).execute();

			if(response.code() != 200){
				output.close();
				return false;
			}


			InputStream input = response.body().byteStream();

			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
				output.write(buffer, 0, bytesRead);
			}
			output.close();
			return true;

		} catch (IOException e) {
			//Удаляем файл, если скачивание прервалось
			file.delete();
			e.printStackTrace();
			return false;
		}

	}

	public static boolean downloadFromUrl(HttpClient http, String urlString, String path){
		
		File file = new File(path, urlString.split("/src/")[1]);
		if (!file.getParentFile().exists())
			file.getParentFile().mkdir();
		if (!file.exists()){
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		OutputStream output;
		try {
			output = new FileOutputStream(file);

			
			HttpGet request = new HttpGet(urlString);
			request.setHeader("User-Agent", Registry.USER_AGENT);
			request.setHeader("Connection", "keep-alive");
			
			
			HttpResponse response = http.execute(request);
			
			if(response.getStatusLine().getStatusCode() != 200){
				output.close();
				return false;
			}
			
			
			InputStream input = response.getEntity().getContent();

			byte[] buffer = new byte[4096];
			int bytesRead = 0;
			while ((bytesRead = input.read(buffer, 0, buffer.length)) >= 0) {
				output.write(buffer, 0, bytesRead);
			}
			output.close();
			return true;
			
		} catch (IOException e) {
			//Удаляем файл, если скачивание прервалось
			file.delete();
			e.printStackTrace();
			return false;
		}
	}

	public static Date iichanTStoDate(String timestamp) {
		//Распарсить дату с ычана в Date
		String[] chunks = timestamp.trim().split("\\s+");
		int start = 0;
		if(chunks[0].length() > 2)
			start = 1;
		String day = chunks[1+start];
		String monthCyr = chunks[2+start];
		String year = chunks[3+start];
		String hms = chunks[4+start];

		if (monthCyr.equals("января")) {
			monthCyr = "01";
		} else if (monthCyr.equals("февраля")) {
			monthCyr = "02";
		} else if (monthCyr.equals("марта")) {
			monthCyr = "03";
		} else if (monthCyr.equals("апреля")) {
			monthCyr = "04";
		} else if (monthCyr.equals("мая")) {
			monthCyr = "05";
		} else if (monthCyr.equals("июня")) {
			monthCyr = "06";
		} else if (monthCyr.equals("июля")) {
			monthCyr = "07";
		} else if (monthCyr.equals("августа")) {
			monthCyr = "08";
		} else if (monthCyr.equals("сентября")) {
			monthCyr = "09";
		} else if (monthCyr.equals("октября")) {
			monthCyr = "10";
		} else if (monthCyr.equals("ноября")) {
			monthCyr = "11";
		} else if (monthCyr.equals("декабря")) {
			monthCyr = "12";
		}
		StringBuilder formatted = new StringBuilder(day);

		formatted.append("-");
		formatted.append(monthCyr);
		formatted.append("-");
		formatted.append(year);
		formatted.append(" ");
		formatted.append(hms);
		Date date;
		try {
			date = format.parse(formatted.toString());
		} catch (ParseException e) {
			date = new Date();
			e.printStackTrace();
		}
		return date;
	}

	
	private static WResponse buildResponse(String html){
		WResponse result = new WResponse();

		Matcher matcher = errorMsgRegex.matcher(html);

		if(html.contains("Ошибка: ")){
			if(!html.contains("<blockquote")){
				if(matcher.find()){
					result.success = false;
					result.errorMessage = matcher.group(1);
				}
			}
		}

		return result;
	}

	
	public static void showCopyFromPostDialog(Context context, Post post){
		Dialog copyDialog = new Dialog(context);
		copyDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
		copyDialog.setContentView(R.layout.copy_dialog);
		EditText copyText = (EditText) copyDialog.findViewById(R.id.copyText);
		copyText.setText(Html.fromHtml(post.text));		
		copyDialog.show();
	}
	
	 public static SpannableStringBuilder noTrailingwhiteLines(SpannableStringBuilder text) {
		 	
		 	//Log.i("TEXTLENGTH", text.length() + "");
		 	if(text.length() == 0){
		 		return text;
		 	}
		 	while (text.charAt(text.length() - 1) == '\n') {
		 		text = (SpannableStringBuilder) text.subSequence(0, text.length() - 1);
		 		if(text.length() == 0){
		 			break;
		 		}
		 	}
		 	return text;
	 }

	@SuppressLint("NewApi")
	 public static String getPath(Context context, Uri uri) {
		String path = "";
		if(uri == null){
			return path;
		}
		String[] projection = { MediaStore.Images.Media.DATA };

		Cursor cursor;
		if(Build.VERSION.SDK_INT >= 19){
			String wholeID = DocumentsContract.getDocumentId(uri);
			String id = wholeID.split(":")[1];
			String sel = MediaStore.Images.Media._ID + "=?";

			cursor = context.getContentResolver().query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
														projection,	sel, new String[]{id}, null);

		} else {
			cursor = context.getContentResolver().query(uri, projection, null, null, null);
		}

		try {

			int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
			cursor.moveToFirst();
			path = cursor.getString(column_index);
			cursor.close();
		} catch (NullPointerException e){
			Log.e("Utils", e.toString());
		}
		return path;
	 }


	 public static String getUserAgent(Context ctx){
		 StringBuilder userAgent = new StringBuilder("Cirnoid ImageBoard Client (9) ");
		 PackageInfo pInfo;
		 try {
			 pInfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 0);
			 userAgent.append(pInfo.versionName);
		 } catch (NameNotFoundException e) {
			 e.printStackTrace();
		 }
		 
		 userAgent.append(" ");
		 userAgent.append(randomString());
		 
		 return userAgent.toString();
	 }

	 private static String randomString(){
		 Random rnd = new Random(System.currentTimeMillis());
		 StringBuilder str = new StringBuilder();
		 for (int i = 0; i < 10; ++i) {
			 str.append(rnd.nextInt(9));
		 }
		 return str.toString();
	 }
	 
	 
	 public static String generatePassword(){
		 Random rnd = new Random(System.currentTimeMillis());
		 final String base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJLMNOPQRSTUVWXYZ1234567890";
		 String output = "";

		 for(int i = 0; i < 16; i++)
		 {
			 output += base.charAt(rnd.nextInt(base.length()));
		 }

		 return output;
	 }
	 
	 public static void showImage(String url, Context context) {
		 Intent imageViewerIntent = new Intent(context, PictureActivity.class);
		 imageViewerIntent.putExtra(PictureActivity.IMAGE_URL, Registry.SITE_URL
				 + url);
		 context.startActivity(imageViewerIntent);
	 }
	 

	 @Deprecated
	 public static HttpClient getHttp(Context ctx){
		 if(CirnoidApplication.http == null){
			 HttpParams params = new BasicHttpParams();
			 params.setParameter("Connection", "keep-alive");
			 HttpProtocolParams.setUserAgent(params, Utils.getUserAgent(ctx));
			 SchemeRegistry schemeRegistry = new SchemeRegistry();
			 schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
			 schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

			 ClientConnectionManager manager = new ThreadSafeClientConnManager(params, schemeRegistry);

			 DefaultHttpClient client = new DefaultHttpClient(manager, params);

			 SSLContext sslContext = null;
			 try {
				 sslContext = SSLContext.getInstance("TLS");
				 sslContext.init(new KeyManager[0], new TrustManager[] {new X509TrustManager() {
					 @Override
					 public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
						 return;
					 }

					 @Override
					 public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
						 return;
					 }

					 @Override
					 public X509Certificate[] getAcceptedIssuers() {
						 return new X509Certificate[0];
					 }
				 }}, new SecureRandom());
				 SSLContext.setDefault(sslContext);
			 } catch (NoSuchAlgorithmException e) {
				 throw new RuntimeException(e);
			 } catch (KeyManagementException e) {
				 throw new RuntimeException(e);
			 }
			 client.setRedirectHandler(new DefaultRedirectHandler());
			 client.setCookieStore(new BasicCookieStore());
			 CirnoidApplication.http = client;
		 }
		 return CirnoidApplication.http;
	 }


    public static boolean isSDPresent(){
      return Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED);
    }

    public static boolean isGzip(Header[] headers) {

        final boolean defaultValue = false;
		if (headers.length == 0) {
			return defaultValue;
		}

		for (int i = 0; i < headers.length; i++) {
			Header header = headers[i];
			Log.e("Gzip", "isGzip: " + header.getName() + " / " + header.getValue());

            if (header.getValue().equals("gzip")) {
                return true;
            }
		}
		return defaultValue;
	}


	public static Response urlToOkHTTPConnection(Context context, String boardUrl) throws IOException {
		OkHttpClient http = getOkHTTP(context);
		Request request = new Request.Builder().url(boardUrl).addHeader("User-Agent", getUserAgent(context)).build();
		return http.newCall(request).execute();
	}

	public static OkHttpClient getOkHTTP(Context context) {
		TrustManager[] trustAllCerts = new TrustManager[]{
				new X509TrustManager() {
					@Override
					public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
					}

					@Override
					public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
					}

					@Override
					public java.security.cert.X509Certificate[] getAcceptedIssuers() {
						return new java.security.cert.X509Certificate[]{};
					}
				}
		};


		final SSLContext sslContext;
		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
			// Create an ssl socket factory with our all-trusting manager
			final javax.net.ssl.SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

			ConnectionSpec spec = new ConnectionSpec.Builder(ConnectionSpec.MODERN_TLS)
					.allEnabledTlsVersions()
					.allEnabledCipherSuites()
					.build();


			CirnoidSettings settings = new CirnoidSettings(context);


			OkHttpClient client = new OkHttpClient.Builder()
					.connectionSpecs(Collections.singletonList(spec))
					.sslSocketFactory(sslSocketFactory, (X509TrustManager)trustAllCerts[0])
					.hostnameVerifier(new HostnameVerifier() {
						@Override
						public boolean verify(String hostname, SSLSession session) {
							return true;
						}
					})
					.addInterceptor(new AntiDdosInterceptor2(context, new AntiDDOSCookiesStorage() {
						@Override
						public void saveCookie(@NonNull String cookie) {
							settings.setAntiDdosCookie(cookie);
						}

						@Nullable
						@Override
						public String loadCookie() {
							return settings.getAntiDdosCookie();
						}
					}))
					.cookieJar(cookieJar)
					.build();

			return client;
		} catch (NoSuchAlgorithmException e) {
			throw new RuntimeException(e);
		} catch (KeyManagementException e) {
			throw new RuntimeException(e);
		}


	}

	private static CookieJar cookieJar = new SimpleCookieJar();

	private static class SimpleCookieJar implements CookieJar {

		private List<Cookie> cookies;

		@Override
		public void saveFromResponse(@NotNull HttpUrl httpUrl, @NotNull List<Cookie> cookies) {
			this.cookies = cookies;
		}

		@Override
		public List<Cookie> loadForRequest(HttpUrl url) {
			if (cookies != null)
				return cookies;
			return Collections.emptyList();
		}
	}
}
