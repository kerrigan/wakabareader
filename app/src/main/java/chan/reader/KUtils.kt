package chan.reader

import android.annotation.SuppressLint
import android.content.Context
import coil.ImageLoader
import coil.disk.DiskCache
import okhttp3.OkHttpClient
import java.security.cert.X509Certificate
import javax.net.ssl.SSLContext
import javax.net.ssl.TrustManager
import javax.net.ssl.X509TrustManager

object KUtils {
    @SuppressLint("CustomX509TrustManager")
    @JvmStatic
    fun initUntrustImageLoader(context: Context): ImageLoader {
        var settings = CirnoidSettings(context)
        // Create a trust manager that does not validate certificate chains
        val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
            @SuppressLint("TrustAllX509TrustManager")
            override fun checkClientTrusted(chain: Array<X509Certificate>, authType: String) {}

            @SuppressLint("TrustAllX509TrustManager")
            override fun checkServerTrusted(chain: Array<X509Certificate>, authType: String) {}

            override fun getAcceptedIssuers(): Array<X509Certificate> {
                return arrayOf()
            }
        })

        // Install the all-trusting trust manager
        val sslContext = SSLContext.getInstance("SSL")
        sslContext.init(null, trustAllCerts, java.security.SecureRandom())

        // Create an ssl socket factory with our all-trusting manager
        val sslSocketFactory = sslContext.socketFactory

        val client = OkHttpClient.Builder()
            .sslSocketFactory(sslSocketFactory, trustAllCerts[0] as X509TrustManager)
            .addInterceptor(AntiDdosInterceptor2(context, object : AntiDDOSCookiesStorage {
                override fun saveCookie(cookie: String) {
                    settings.antiDdosCookie = cookie
                }

                override fun loadCookie(): String? {
                    return settings.antiDdosCookie
                }
            }))
            .hostnameVerifier { _, _ -> true }.build()


        return ImageLoader.Builder(context)
            .diskCache(
                DiskCache.Builder()
                    .directory(context.cacheDir)
                    .maximumMaxSizeBytes(settings.imagesCacheSize * 1024 * 1024L)
                    .build()
            )
            .allowRgb565(true)
            .allowHardware(true)
            .respectCacheHeaders(true)
            .okHttpClient(client)
            .build()
    }
}