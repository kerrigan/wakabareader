package chan.reader.parsers;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpException;
import org.apache.http.HttpResponse;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import chan.reader.Utils;
import chan.reader.objects.Post;

public class ThreadParser {
	private final static String TAG = ThreadParser.class.getSimpleName();
	Integer currentThreadNumber;
	private final List<Post> comments = new LinkedList<Post>();

	public ThreadParser() {
		// TODO: заменить весь бывший здесь ад на получение данных из
		// нормального парсера
	}

	public List<Post> getComments() {
		return comments;
	}

	public void parseUrl(Context ctx, String boardPrefix, String pageUrl)
			throws IOException, SAXException, HttpException {
		HttpResponse response = Utils.urlToConnection(ctx, pageUrl);
	

		// InputStream stream = connection.getInputStream();
		boolean isGzip = response.getHeaders("Content-Encoding").length > 0;
		String threadHtml = Utils.streamToString(response.getEntity().getContent(), isGzip);
		parseUrl(boardPrefix, threadHtml, response.getLastHeader("ETag").getValue());
	}

	public void parseUrl(String boardPrefix, String html, String hash)
			throws IOException, SAXException {
		InputStream input = new ByteArrayInputStream(html.getBytes("UTF-8"));
		InputSource source = new InputSource(input);
		Parser parser = new Parser();
		// TODO: переделать, чтобы номер треда и префикс находился сам
		comments.clear();

        final ThreadHandler handler = new ThreadHandler(boardPrefix, comments);
        parser.setContentHandler(handler);
		parser.setFeature(Parser.namespacesFeature, true);
		parser.setFeature(Parser.CDATAElementsFeature, true);
		parser.setFeature(Parser.ignorableWhitespaceFeature, false);
		parser.setFeature(Parser.namespacePrefixesFeature, false);
		long parseStart = System.currentTimeMillis();
		parser.parse(source);
		
		
		long parseStop = System.currentTimeMillis() - parseStart;
		Log.i(TAG, "THREAD PARSE STOP" + parseStop);
	}
}
