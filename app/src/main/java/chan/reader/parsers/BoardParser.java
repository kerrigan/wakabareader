package chan.reader.parsers;

import android.content.Context;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import chan.reader.Utils;
import chan.reader.objects.Post;


public class BoardParser {
	private static final String TAG = BoardParser.class.getSimpleName();
	private final List<Post> topics = new ArrayList<Post>();

	public BoardParser(){
	}
	
	
	public void parseUrl(String boardPrefix, String html, String hash) throws Exception {
		topics.clear();
		InputStream input = new ByteArrayInputStream(html.getBytes("UTF-8"));
		InputSource source = new InputSource(input);


		Parser parser = new Parser();
		parser.setContentHandler(new BoardHandler(boardPrefix, topics));
        parser.setFeature(Parser.namespacesFeature, true);
        parser.setFeature(Parser.CDATAElementsFeature, true);
        parser.setFeature(Parser.ignorableWhitespaceFeature, false);
        parser.setFeature(Parser.namespacePrefixesFeature, false);


		long parseStart = System.currentTimeMillis();
        parser.parse(source);
        long parseStop = System.currentTimeMillis() - parseStart;
		Log.i(TAG, "BOARD PARSE STOP " + parseStop);
	}
	
	public void parseUrl(Context ctx, String boardPrefix, String pageUrl) throws Exception{
		//Log.d("HTML", "Start parse");
        HttpResponse response = Utils.urlToConnection(ctx, pageUrl);
		boolean isGzip = Utils.isGzip(response.getHeaders("Content-Encoding"));
		String boardHtml = Utils.streamToString(response.getEntity().getContent(), isGzip);
		parseUrl(boardPrefix, boardHtml, response.getLastHeader("ETag").getValue());
	}
	
	
	public List<Post> getOpPosts() {

		return topics;
	}
}
