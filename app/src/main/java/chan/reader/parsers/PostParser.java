package chan.reader.parsers;

import android.text.SpannableStringBuilder;
import android.util.Log;

import org.ccil.cowan.tagsoup.Parser;
import org.xml.sax.InputSource;

import java.io.ByteArrayInputStream;
import java.util.List;

import chan.reader.Utils;
import chan.reader.objects.Post;
import coil.ImageLoader;

public class PostParser {
	
	private final List<Post> mPosts;
	private final ImageLoader mImgLoader;
	private final Integer mPostId;

	public PostParser(List<Post> posts, ImageLoader imgLoader, Integer postId) {
		mPosts = posts;
		mPostId = postId;
		mImgLoader = imgLoader;
	}

	public SpannableStringBuilder parsePost(String html) {
		Parser parser = new Parser();
		PostHandler handler = new PostHandler(mPosts, mPostId, mImgLoader);
		try {
			InputSource source = new InputSource(new ByteArrayInputStream(
					html.getBytes("UTF-8")));

			parser.setContentHandler(handler);
			parser.setFeature(Parser.namespacesFeature, true);
			parser.setFeature(Parser.CDATAElementsFeature, true);
			parser.setFeature(Parser.ignorableWhitespaceFeature, false);
			parser.setFeature(Parser.namespacePrefixesFeature, false);

			parser.parse(source);
		} catch (Exception e) {
			Log.e("POST PARSER", "PARSE FAILED" + e.getMessage());
		}
		return Utils.noTrailingwhiteLines(handler.currentText);
	}
}
