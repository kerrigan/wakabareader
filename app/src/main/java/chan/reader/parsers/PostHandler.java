package chan.reader.parsers;

import android.graphics.Typeface;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

import chan.reader.SpanRecord;
import chan.reader.objects.Post;
import chan.reader.spannables.CodeSpan;
import chan.reader.spannables.PostReferenceSpan;
import chan.reader.spannables.QuoteSpan;
import chan.reader.spannables.SpoilerSpan;
import coil.ImageLoader;

public class PostHandler extends DefaultHandler {
	private boolean IN_SPOILER = false;
	private boolean IN_URL = false;
	private boolean IN_QUOTE = false;
	private boolean IN_DEL = false;
	private boolean IN_BOLD = false;
	private boolean IN_ITALIC = false;
	private boolean IN_CODE = false;
	private boolean IN_REPLY = false;
	private boolean IN_NUM_LIST = false;
	private boolean IN_NONUM_LIST = false;
	private boolean IN_LIST_ITEM = false;
	
	private int leftSpoiler = 0;
	private int rightSpoiler = 0;
	private int leftQuote = 0;
	private int rightQuote = 0;
	private int leftUrl = 0;
	private int rightUrl = 0;
	private int leftDel = 0;
	private int rightDel = 0;
	private int leftBold = 0;
	private int rightBold = 0;
	private int leftItalic = 0;
	private int rightItalic = 0;
	private int leftCode = 0;
	private int rightCode = 0;
	private int leftReply = 0;
	private int rightReply = 0;

	private final List<Post> mPosts;
	private final List<SpanRecord> spanRecords = new ArrayList<SpanRecord>();
	
	public final SpannableStringBuilder currentText = new SpannableStringBuilder();
	private final Integer mPostId;
//	private final ImageLoader mImLoader;

	private final ImageLoader mImLoader;
	private int listCounter;


	
	
	public PostHandler(List<Post> posts, Integer postId, ImageLoader imageLoader) {
		mPosts = posts;
		mPostId = postId;
		mImLoader = imageLoader;
	}
	
	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		
		if(IN_CODE){
			if(qName.equals("br")){
				currentText.append("\n");
				return;
			}
			
			StringBuilder attrString = new StringBuilder();
			for (int i = 0; i < attributes.getLength(); ++i) {
				attrString.append(" ").append(attributes.getQName(i)).append("=\"").append(attributes.getValue(i)).append("\"");
			}
			String tagBegin = "<" + qName;
			if(attrString.length() != 0){
				tagBegin += attrString.toString();
			}
			currentText.append(tagBegin).append(">");
		}
		
		if(qName.equals("span") && attributes.getIndex("class") != -1 &&
				attributes.getValue("class").equals("spoiler")){
			IN_SPOILER = true;
			leftSpoiler = currentText.length();
		}
		
		if(qName.equals("blockquote") && attributes.getIndex("class") != -1 &&
				attributes.getValue("class").equals("unkfunc")){
			IN_QUOTE = true;
			leftQuote = currentText.length();
		}
		
		if(qName.equals("del")){
			IN_DEL = true;
			leftDel = currentText.length();
		}
		
		if(qName.equals("strong")){
			IN_BOLD = true;
			leftBold = currentText.length();
		}
		
		if(qName.equals("em")){
			IN_ITALIC = true;
			leftItalic = currentText.length();
		}
		
		if(qName.equals("a") && attributes.getIndex("rel") != -1 &&
				attributes.getValue("rel").equals("nofollow")){
			IN_URL = true;
			leftUrl = currentText.length();
		}
		
		if(qName.equals("code")){
			IN_CODE = true;
			leftCode = currentText.length();
		}
		
		if(qName.equals("a") && attributes.getIndex("onclick") != -1){
			IN_REPLY = true;
			leftReply = currentText.length();
		}
		
		if(qName.equals("ul")){
			IN_NONUM_LIST = true;
			currentText.append("\n");
		}
		
		if(qName.equals("ol")){
			IN_NUM_LIST = true;
			listCounter = 1;
			currentText.append("\n");
		}
		
		if(qName.equals("li")){
			IN_LIST_ITEM = true;
			if(IN_NUM_LIST){
				currentText.append(Integer.toString(listCounter)).append(". ");
				listCounter++;
			}
			if(IN_NONUM_LIST){
				currentText.append("- ");
			}
		}
	}
	
	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		
		
		if(IN_CODE && !qName.equals("code")){
			if(qName.equals("br")){
				return;
			}
			currentText.append("</").append(qName).append(">");
		}
		
		if(qName.equals("p")){
			currentText.append("\n");
		}
		if(qName.equals("br")){
			currentText.append("\n");
		}
		
		if(IN_SPOILER && localName.equals("span")){
			rightSpoiler = currentText.length();
			spanRecords.add(new SpanRecord(new SpoilerSpan(), leftSpoiler, rightSpoiler));
			IN_SPOILER = false;
		}
		
		if(IN_QUOTE && qName.equals("blockquote")){
			rightQuote = currentText.length();
			spanRecords.add(0, new SpanRecord(new QuoteSpan(), leftQuote, rightQuote));
			currentText.append("\n");
			IN_QUOTE = false;
		}
		
		if(IN_DEL && qName.equals("del")){
			rightDel = currentText.length();
			spanRecords.add(new SpanRecord(new StrikethroughSpan(), leftDel, rightDel));
			IN_DEL = false;
		}
		
		if(IN_BOLD && qName.equals("strong")){
			rightBold = currentText.length();
			spanRecords.add(new SpanRecord(new StyleSpan(Typeface.BOLD), leftBold, rightBold));
			IN_BOLD = false;
		}
		
		if(IN_ITALIC && qName.equals("em")){
			rightItalic = currentText.length();
			spanRecords.add(new SpanRecord(new StyleSpan(Typeface.ITALIC), leftItalic, rightItalic));
			IN_ITALIC = false;
		}
		
	
		if(IN_CODE && qName.equals("code")){
			rightCode = currentText.length();
			spanRecords.add(new SpanRecord(new CodeSpan(), leftCode, rightCode));
			IN_CODE = false;
		}
		
		if(IN_URL && qName.equals("a")){
			rightUrl = currentText.length();
			spanRecords.add(new SpanRecord(
					new URLSpan(currentText.subSequence(leftUrl, rightUrl).toString()),
					leftUrl, rightUrl));
			IN_URL = false;
		}
		
		if(IN_REPLY && qName.equals("a")){
			rightReply = currentText.length();
			spanRecords.add(
					new SpanRecord(
							new PostReferenceSpan(
									mPosts,
									Integer.parseInt(currentText.subSequence(leftReply + 2, rightReply).toString()),
									mPostId,
									mImLoader),
					leftReply, rightReply));
			IN_REPLY = false;
		}
		
		if(IN_NONUM_LIST && qName.equals("ol")){
			IN_NUM_LIST = false;
		}
		
		if(IN_NUM_LIST && qName.equals("ul")){
			currentText.append("\n");
			IN_NONUM_LIST = false;
		}
		
		if(IN_LIST_ITEM && qName.equals("li")){
			currentText.append("\n");
			IN_LIST_ITEM = false;
		}
		
	}
	
	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		currentText.append(new String(ch, start, length));//.replace("<", "&lt;").replace(">", "&gt;"));
	}
	
	@Override
	public void endDocument() throws SAXException {		
		for (SpanRecord record : spanRecords) {
			currentText.setSpan(record.span, record.left, record.right, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
		}
		spanRecords.clear();
		super.endDocument();
	}
}
