/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package chan.reader.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.List;

import chan.reader.Utils;
import chan.reader.objects.Post;

public class ThreadHandler extends DefaultHandler {

    private StringBuilder thisElement = new StringBuilder();
    private Integer thisThreadId = 0;
    private String thisText = "";
    private String thisImgURL = "";
    private String thisThumbURL = "";
    private String thisBoardPrefix = "";
    private StringBuilder thisDate = new StringBuilder();
    private StringBuilder thisName = new StringBuilder();
    private StringBuilder thisTitle = new StringBuilder();
    private boolean IN_THREAD = false;
    private boolean IN_TEXT = false;
    private boolean IN_COMMENT = false;
    private boolean IN_NAME = false;
    private boolean IN_TITLE = false;
    private boolean IN_DATE = false;
    
    private int BQLEVEL = 0;
    private int BLOCKQUOTECOUNT = 0;
    private int THUMBCOUNT = 0;
    private int IMGURLCOUNT = 0;
    public final List<Post> comments;


    public ThreadHandler(String boardPrefix, List<Post>comments) {
    	this.comments = comments;
        thisBoardPrefix = boardPrefix.replace("/", "").trim();
    }

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {

        if ((attributes.getIndex("id") != -1)) {
            String threadIdStr = attributes.getValue("id");
            if (threadIdStr.startsWith("thread-")) {
                thisThreadId = Integer.parseInt(attributes.getValue("id").split("-")[1]);
                IN_THREAD = true;
            }
        }

        if (qName.equals("td")) {
            if (attributes.getIndex("class") != -1 && attributes.getIndex("id") != -1
                    && attributes.getValue("class").equals("reply")) {
                String comment_class = attributes.getValue("id");
                if (comment_class.startsWith("reply")) {
                    IN_COMMENT = true;
                    thisThreadId = Integer.parseInt(comment_class.substring("reply".length()));
                }
            }
        }

        if (IN_THREAD) {
            if (IN_TEXT) {
            	StringBuilder attrString = new StringBuilder();
				for (int i = 0; i < attributes.getLength(); ++i) {
					attrString.append(" ").append(attributes.getQName(i)).append("=\"").append(attributes.getValue(i)).append("\"");
				}
				String tagBegin = "<" + qName;
				if(attrString.length() != 0){
					tagBegin += attrString.toString();
				}
				thisElement.append(tagBegin).append(">");
    			//thisElement.append("\n");
    		}
        	
            if (qName.equals("a") && attributes.getIndex("target") != -1 && attributes.getIndex("href") != -1) {
                String target = attributes.getValue("target");
                String href = attributes.getValue("href");

                if (target.equals("_blank") && href.startsWith("/" + thisBoardPrefix + "/src/")) {
                    IMGURLCOUNT += 1;
                    if (IMGURLCOUNT == 1) {
                        thisImgURL = href;
                    }
                }
            }

            if (qName.equals("img")) {
                THUMBCOUNT += 1;
                if (THUMBCOUNT == 1) {
                    thisThumbURL = attributes.getValue("src");
                }
            }
            if (qName.equals("blockquote")) {
                BQLEVEL++;
                if (!IN_TEXT) {
                    BLOCKQUOTECOUNT += 1;
                }
                if (BLOCKQUOTECOUNT == 1) {
                    IN_TEXT = true;
                }
            }

            if (qName.equals("span") && attributes.getIndex("class") != -1) {
                String classname = attributes.getValue("class");
                if (classname.equals("postername")) {
                    IN_NAME = true;
                }
                
                if(classname.equals("filetitle")){
					IN_TITLE  = true;
					//TITLECOUNT++;
				}
            }
        }

        if (IN_COMMENT) {
            if (IN_TEXT) {
				StringBuilder attrString = new StringBuilder();
				for (int i = 0; i < attributes.getLength(); ++i) {
					attrString.append(" ").append(attributes.getQName(i)).append("=\"").append(attributes.getValue(i)).append("\"");
				}
				String tagBegin = "<" + qName;
				if (attrString.length() != 0) {
					tagBegin += attrString.toString();
				}

                thisElement.append(tagBegin).append(">");
				//thisElement.append("\n");
            }
        	
        	
            if (qName.equals("a") && attributes.getIndex("target") != -1
            		&& attributes.getIndex("href") != -1) {
                String target = attributes.getValue("target");
                String href = attributes.getValue("href");

                if (target.equals("_blank") && href.startsWith("/" + thisBoardPrefix + "/src/")) {
                        thisImgURL = href;
                }
            }

            if (qName.equals("img")) {
                THUMBCOUNT += 1;
                if (THUMBCOUNT == 1) {
                    thisThumbURL = attributes.getValue("src");
                }
            }
            if (qName.equals("blockquote")) {
                BQLEVEL++;
                if (!IN_TEXT) {
                    BLOCKQUOTECOUNT += 1;
                }
                if (BLOCKQUOTECOUNT == 1) {
                    IN_TEXT = true;
                }
            }


            if (qName.equals("span") && attributes.getIndex("class") != -1) {
                String classname = attributes.getValue("class");
                if (classname.equals("commentpostername")) {
                    IN_NAME = true;
                }
                
                if(classname.equals("filetitle")){
					IN_TITLE  = true;
					//TITLECOUNT++;
				}
            }

        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (IN_THREAD) {
			
        	
        	
            if (IN_NAME) {
                if (qName.equals("span")) {
                    IN_NAME = false;
                    IN_DATE = true;
                }
            }
            
        	if(IN_TITLE){
				if(qName.equals("span")){
					IN_TITLE = false;
				}
			}
        	
        	if (IN_DATE) {
				if (qName.equals("label")) {
					IN_DATE = false;
				}
			}

            if (qName.equals("blockquote")) {
                BQLEVEL--;
                if (IN_TEXT && BQLEVEL == 0) {
                    IN_TEXT = false;
                    thisText = thisElement.toString();
                    //topics.add(thisElement);
                    thisElement = new StringBuilder();
                }


                if (!IN_TEXT && BQLEVEL == 0) {
                    BLOCKQUOTECOUNT = 0;
                    THUMBCOUNT = 0;
                    IMGURLCOUNT = 0;
                    IN_THREAD = false;
                    Post oppost = new Post();
                    oppost.text = thisText.trim();
                    oppost.image = thisImgURL;
                    oppost.thumb = thisThumbURL;
                    oppost.id = thisThreadId;
                    oppost.board = thisBoardPrefix;
                    oppost.name = thisName.toString();
                    oppost.title = thisTitle.toString();
                    oppost.timestamp = Utils.iichanTStoDate(thisDate.toString());
                    comments.add(oppost);

                    // Очистка
                    thisThreadId = 0;
                    thisText = "";
                    thisName = new StringBuilder();
                    thisDate = new StringBuilder();
                    thisThumbURL = "";
                    thisImgURL = "";
                    thisElement = new StringBuilder();
                    thisTitle = new StringBuilder();
                    BQLEVEL = 0;
                }
            }
            
            if(IN_TEXT){
				thisElement.append("</").append(qName).append(">");
			}
        }

        if (IN_COMMENT) {

            if (qName.equals("blockquote")) {
                BQLEVEL--;
                if (IN_TEXT && BQLEVEL == 0) {
                    IN_TEXT = false;
                    thisText = thisElement.toString();
                    //topics.add(thisElement);
                    thisElement = new StringBuilder();
                }


                if (!IN_TEXT && BQLEVEL == 0) {
                    BLOCKQUOTECOUNT = 0;
                    THUMBCOUNT = 0;
                    IMGURLCOUNT = 0;
                    BQLEVEL = 0;
                }
            }


            if (IN_NAME) {
                if (qName.equals("span")) {
                    IN_NAME = false;
                    IN_DATE = true;
                }
            }
            
        	if(IN_TITLE){
				if(qName.equals("span")){
					IN_TITLE = false;
				}
			}
        	
        	if (IN_DATE) {
				if (qName.equals("label")) {
					IN_DATE = false;
				}
			}
        	

            if (qName.equals("td")) {
                IN_COMMENT = false;
                //TODO: забираем комментарий, чистим данные
                Post comment = new Post();
                comment.name = thisName.toString();
                comment.board = thisBoardPrefix;
                comment.text = thisText.trim();
                comment.id = thisThreadId;
                comment.image = thisImgURL;
                comment.thumb = thisThumbURL;
                comment.title = thisTitle.toString();
                comment.timestamp = Utils.iichanTStoDate(thisDate.toString());
                comments.add(comment);


                //clear
                thisName = new StringBuilder();
                thisText = "";
                thisThumbURL = "";
                thisImgURL = "";
                thisTitle = new StringBuilder();
                thisDate = new StringBuilder();
                thisThreadId = 0;
            }
            
        	
			if(IN_TEXT && qName != "br"){
				thisElement.append("</").append(qName).append(">");
			}
        }


        if (!IN_THREAD) {
        }


    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        super.characters(ch, start, length);
        String text = new String(ch, start, length);

        if (IN_TEXT) {
            thisElement.append(text.replace("<", "&lt;").replace(">", "&gt;"));
        }
        
        if (IN_NAME) {
            thisName.append(text);
        }
        
		if(IN_TITLE){
			thisTitle.append(text);
		}
		
		if (IN_DATE) {
			thisDate.append(text);
		}
    }
}