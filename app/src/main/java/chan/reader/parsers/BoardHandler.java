package chan.reader.parsers;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import chan.reader.Registry;
import chan.reader.Utils;
import chan.reader.objects.Post;

class BoardHandler extends DefaultHandler {

	StringBuilder thisElement = new StringBuilder();
	Integer thisThreadId = 0;
	private StringBuilder thisName = new StringBuilder();
	private String thisText = "";
	private String thisImgURL = "";
	private String thisThumbURL = "";
	private String thisBoardPrefix = "";
	private StringBuilder thisTitle = new StringBuilder();
	Attributes thisElementAttrs = null;
	boolean IN_THREAD = false;
	boolean IN_TEXT = false;
	boolean IN_QUOTE = false;
	boolean IN_NAME = false;
	private boolean IN_TITLE = false;
	int BLOCKQUOTECOUNT = 0;
	int THUMBCOUNT = 0;
	int IMGURLCOUNT = 0;
	final List<Post> topics;
	private int NAMECOUNT = 0;
	//private int TITLECOUNT = 0;
	private int BQLEVEL = 0;
	private boolean IN_DATE = false;
	private StringBuilder thisDate = new StringBuilder();

    //O
    private boolean IN_OMITTED_POSTS = false;
    private StringBuilder thisOmittedPostsText = new StringBuilder();
    boolean omittedFound = false;
    int omittedCount = 0;
    int omittedImagesCount = 0;
    //Replies fields

    private boolean IN_REPLY = false;
    private boolean REPLY_IN_TEXT = false;
    private StringBuilder thisReplyElement = new StringBuilder();
    private int REPLY_IMGURLCOUNT = 0;
    private String thisReplyImgURL;
    private int REPLY_THUMBCOUNT = 0;
    private String thisReplyThumbURL;
    private int REPLY_BQLEVEL = 0;
    private int REPLY_BLOCKQUOTECOUNT = 0;
    private int REPLY_NAMECOUNT = 0;
    private boolean REPLY_IN_NAME = false;
    private boolean REPLY_IN_TITLE = false;
    private boolean REPLY_IN_DATE = false;
    private String thisReplyText;
    private int thisReplyId = 0;
    private StringBuilder thisReplyName = new StringBuilder();
    private StringBuilder thisReplyDate = new StringBuilder();
    private StringBuilder thisReplyTitle = new StringBuilder();


    public BoardHandler(String boardPrefix, List<Post> topics) {
		thisBoardPrefix = boardPrefix.replace("/", "").trim();
		this.topics = topics;
	}

	@Override
	public void startElement(String uri, String localName, String qName,
			Attributes attributes) throws SAXException {
		/*
		 * thisElement = qName; thisElementAttrs = attributes;
		 */

		if ((attributes.getIndex("id") != -1)) {
			String threadIdStr = attributes.getValue("id");

			if (threadIdStr.startsWith("thread-")) {
				thisThreadId = Integer.parseInt(attributes.getValue("id")
						.split("-")[1]);
				IN_THREAD = true;
			}

		}

		if (IN_THREAD) {
			if (IN_TEXT) {
				StringBuilder attrString = new StringBuilder();
				for (int i = 0; i < attributes.getLength(); ++i) {
					attrString.append(" ").append(attributes.getQName(i)).append("=\"").append(attributes.getValue(i)).append("\"");
				}
				String tagBegin = "<" + qName;
				if(attrString.length() != 0){
					tagBegin += attrString.toString();
				}
				thisElement.append(tagBegin).append(">");
				//thisElement.append("\n");
			}
			
			
			if (qName.equals("a") && attributes.getIndex("target") != -1
					&& attributes.getIndex("href") != -1) {
				String target = attributes.getValue("target");
				String href = attributes.getValue("href");

				if (target.equals("_blank") && href.startsWith("/" + thisBoardPrefix + "/src/")) {
					IMGURLCOUNT += 1;
					if (IMGURLCOUNT == 1) {
						thisImgURL = href;
					}
				}
			}

			if (qName.equals("img")) {
				THUMBCOUNT += 1;
				if (THUMBCOUNT == 1) {
					thisThumbURL = attributes.getValue("src");
				}
			}
			if (qName.equals("blockquote")) {
				BQLEVEL++;
				if (!IN_TEXT) {
					BLOCKQUOTECOUNT += 1;
				}
				if (BLOCKQUOTECOUNT == 1) {
					IN_TEXT = true;
				}
			}

			if (qName.equals("span") && attributes.getIndex("class") != -1
					&& NAMECOUNT == 0) {
				String classname = attributes.getValue("class");
				if (classname.equals("postername")) {
					IN_NAME = true;
					NAMECOUNT++;
				}
				
				if(classname.equals("filetitle")){
					IN_TITLE  = true;
				}
			}
		}

        if(qName.equals("span") && attributes.getValue("class") != null && attributes.getValue("class").equals("omittedposts")){
            IN_OMITTED_POSTS = true;
        }

        if(qName.equals("td") && attributes.getValue("id") != null && attributes.getValue("id").startsWith("reply")){
            thisReplyId = Integer.parseInt(attributes.getValue("id").substring(5));
            IN_REPLY = true;
        }

        if(IN_REPLY){
            if (REPLY_IN_TEXT) {
                StringBuilder attrString = new StringBuilder();
                for (int i = 0; i < attributes.getLength(); ++i) {
                    attrString.append(" ").append(attributes.getQName(i)).append("=\"").append(attributes.getValue(i)).append("\"");
                }
                String tagBegin = "<" + qName;
                if(attrString.length() != 0){
                    tagBegin += attrString.toString();
                }
                thisReplyElement.append(tagBegin).append(">");
                //thisElement.append("\n");
            }


            if (qName.equals("a") && attributes.getIndex("target") != -1
                    && attributes.getIndex("href") != -1) {
                String target = attributes.getValue("target");
                String href = attributes.getValue("href");

                if (target.equals("_blank") && href.startsWith("/" + thisBoardPrefix + "/src/")) {
                    REPLY_IMGURLCOUNT += 1;
                    if (REPLY_IMGURLCOUNT == 1) {
                        thisReplyImgURL = href;
                    }
                }
            }

            if (qName.equals("img") && attributes.getIndex("src") != -1) {
                REPLY_THUMBCOUNT += 1;
                if (REPLY_THUMBCOUNT == 1) {
                    thisReplyThumbURL = attributes.getValue("src");
                }
            }
            if (qName.equals("blockquote")) {
                REPLY_BQLEVEL++;
                if (!REPLY_IN_TEXT) {
                    REPLY_BLOCKQUOTECOUNT += 1;
                }
                if (REPLY_BLOCKQUOTECOUNT == 1) {
                    REPLY_IN_TEXT = true;
                }
            }

            if (qName.equals("span") && attributes.getIndex("class") != -1
                    && REPLY_NAMECOUNT == 0) {
                String classname = attributes.getValue("class");
                if (classname.equals("commentpostername")) {
                    REPLY_IN_NAME = true;
                    REPLY_NAMECOUNT++;
                }

                if(classname.equals("replytitle")){
                    REPLY_IN_TITLE  = true;
                }
            }
        }

        super.startElement(uri, localName, qName, attributes);
	}

	@Override
	public void endElement(String uri, String localName, String qName)
			throws SAXException {
		super.endElement(uri, localName, qName);

		if (IN_THREAD) {

			if (IN_NAME) {
				if (qName.equals("span")) {
					IN_NAME = false;
					IN_DATE  = true;
				}
			}
			if(IN_TITLE){
				if(qName.equals("span")){
					IN_TITLE = false;
				}
			}
			
			if (IN_DATE) {
				if (qName.equals("label")) {
					IN_DATE = false;
				}
			}
			
			


			if (qName.equals("blockquote")) {
				BQLEVEL--;
				if (IN_TEXT && BQLEVEL == 0) {
					IN_TEXT = false;
					thisText = thisElement.toString();
					// topics.add(thisElement);
					thisElement = new StringBuilder();
				}

				if (!IN_TEXT && BQLEVEL == 0) {
					BLOCKQUOTECOUNT = 0;
					THUMBCOUNT = 0;
					IMGURLCOUNT = 0;
					IN_THREAD = false;
					NAMECOUNT = 0;
					Post oppost = new Post();
					oppost.text = thisText.trim();
					oppost.image = thisImgURL;
					oppost.thumb = thisThumbURL;
					oppost.id = thisThreadId;
					oppost.board = thisBoardPrefix;
					oppost.name = thisName.toString();
					oppost.title = thisTitle.toString();
					oppost.timestamp = Utils.iichanTStoDate(thisDate.toString());
					topics.add(oppost);

					// Очистка
					thisThreadId = 0;
					thisText = "";
					thisName = new StringBuilder();
					thisThumbURL = "";
					thisImgURL = "";
					thisElement = new StringBuilder();
					thisTitle = new StringBuilder();
					thisDate = new StringBuilder();
					BQLEVEL = 0;
				}
			}
			
			if(IN_TEXT){
				thisElement.append("</").append(qName).append(">");
			}
		}

        if(IN_OMITTED_POSTS && qName.equals("span")){
            final String omittedString = thisOmittedPostsText.toString();
            Matcher matcher = Registry.omittedRegex.matcher(omittedString);
            Matcher matcherWithImages = Registry.omittedWithImagesRegex.matcher(omittedString);

            if(matcherWithImages.find()){
                omittedCount = Integer.parseInt(matcherWithImages.group(1));
                omittedImagesCount = Integer.parseInt(matcherWithImages.group(2));
                omittedFound = true;
            } else if(matcher.find()){
                omittedCount = Integer.parseInt(matcher.group(1));
                omittedImagesCount = 0;
                omittedFound = true;
			}


            if(topics.size() > 0) {
                Post oppost = topics.get(topics.size() - 1);
                oppost.omittedCount = omittedCount;
                oppost.omittedImagesCount = omittedImagesCount;
                oppost.omittedFound = omittedFound;
                omittedFound = false;
                omittedCount = 0;
                omittedImagesCount = 0;
                thisOmittedPostsText = new StringBuilder();
            }

            IN_OMITTED_POSTS = false;

        }

        if(IN_REPLY){
            if (REPLY_IN_NAME) {
                if (qName.equals("span")) {
                    REPLY_IN_NAME = false;
                    REPLY_IN_DATE  = true;
                }
            }
            if(REPLY_IN_TITLE){
                if(qName.equals("span")){
                    REPLY_IN_TITLE = false;
                }
            }

            if (REPLY_IN_DATE) {
                if (qName.equals("label")) {
                    REPLY_IN_DATE = false;
                }
            }




            if (qName.equals("blockquote")) {
                REPLY_BQLEVEL--;
                if (REPLY_IN_TEXT && REPLY_BQLEVEL == 0) {
                    REPLY_IN_TEXT = false;
                    thisReplyText = thisReplyElement.toString();
                    // topics.add(thisElement);
                    thisReplyElement = new StringBuilder();
                }

                if (!REPLY_IN_TEXT && REPLY_BQLEVEL == 0) {
                    REPLY_BLOCKQUOTECOUNT = 0;
                    REPLY_THUMBCOUNT = 0;
                    REPLY_IMGURLCOUNT = 0;
                    REPLY_NAMECOUNT = 0;
                    IN_REPLY = false;
                    Post reply = new Post();
                    reply.text = thisReplyText.trim();
                    reply.image = thisReplyImgURL;
                    reply.thumb = thisReplyThumbURL;
                    reply.id = thisReplyId;
                    reply.board = thisBoardPrefix;
                    reply.name = thisReplyName.toString();
                    reply.title = thisReplyTitle.toString();
                    reply.timestamp = Utils.iichanTStoDate(thisReplyDate.toString());

                    if(topics.size() > 0) {
                        Post post = topics.get(topics.size()-1);
                        if(post.lastReplies == null){
                            post.lastReplies = new ArrayList<>();
                        }
                        post.lastReplies.add(reply);
                    }

                    // Очистка
                    thisReplyId = 0;
                    thisReplyText = "";
                    thisReplyName = new StringBuilder();
                    thisReplyThumbURL = "";
                    thisReplyImgURL = "";
                    thisReplyElement = new StringBuilder();
                    thisReplyTitle = new StringBuilder();
                    thisReplyDate = new StringBuilder();
                    REPLY_BQLEVEL = 0;
                }
            }

            if(REPLY_IN_TEXT){
                thisReplyElement.append("</").append(qName).append(">");
            }
        }
	}

	@Override
	public void characters(char[] ch, int start, int length)
			throws SAXException {
		super.characters(ch, start, length);
        String text = new String(ch, start, length).replace("<", "&lt;").replace(">", "&gt;");

		if (IN_TEXT) {
            thisElement.append(text);
		}

		if (IN_NAME) {
			thisName.append(text);
		}
		
		if(IN_TITLE){
			thisTitle.append(text);
		}
		
		if (IN_DATE) {
			thisDate.append(text);
		}
        
        if (IN_OMITTED_POSTS){
            thisOmittedPostsText.append(text);
        }

        if(REPLY_IN_TEXT){
            thisReplyElement.append(text);
        }

        if(REPLY_IN_NAME) {
            thisReplyName.append(text);
        }


        if(REPLY_IN_DATE){
            thisReplyDate.append(text);
        }

        if(REPLY_IN_TITLE){
            thisReplyTitle.append(text);
        }
		// System.out.println(thisElement + " " + attrs);
	}
}
