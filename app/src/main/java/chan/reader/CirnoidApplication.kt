package chan.reader

import android.app.Application
import org.apache.http.client.HttpClient
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CirnoidApplication : Application() {
    override fun onCreate() {
        super.onCreate()
        //TODO: reconnect ACRA

        startKoin{
            androidLogger()
            androidContext(this@CirnoidApplication)
            modules(appModule)
        }
    }

    override fun onTerminate() {
        if (Registry.db != null) {
            Registry.db.close()
        }
        super.onTerminate()
    }

    companion object {
        @JvmField var boardsInstance: Boards? = null
        @JvmField var http: HttpClient? = null
    }
}
